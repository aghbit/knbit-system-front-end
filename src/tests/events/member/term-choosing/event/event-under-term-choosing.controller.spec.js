/**
 * Created by novy on 18.10.15.
 */

describe('EventUnderTermChoosingController', function () {

  var $controller, $q, $rootScope, enrollmentServiceMock;

  beforeEach(module('knbitFrontend.Events.Members.TermChoosing.Event'));
  beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
    $controller = _$controller_;
    $q = _$q_;
    $rootScope = _$rootScope_;
    enrollmentServiceMock = jasmine.createSpyObj('enrollmentService', ['enrollFor', 'disenrollFrom']);
  }));

  it("should update chosenTerm on successful enrollment", function () {
    // given
    var event = {
      eventId: 'anId',
      chosenTerm: 'someTerm'
    };
    var termId = 'termId';

    var objectUnderTest = createControllerWith('memberId', event);
    enrollmentServiceMock.enrollFor.and.returnValue($q.when({}));

    // when
    objectUnderTest.enrollCallbackFor(termId);
    $rootScope.$apply();

    // then
    expect(event.chosenTerm).toBe(termId);
  });

  it("should clear member preference on disenroll", function () {
    // given
    var termId = 'termId';
    var event = {
      eventId: 'anId',
      chosenTerm: termId
    };

    var objectUnderTest = createControllerWith('memberId', event);
    enrollmentServiceMock.disenrollFrom.and.returnValue($q.when({}));

    // when
    objectUnderTest.disenrollCallbackFor(termId);
    $rootScope.$apply();

    // then
    expect(event.chosenTerm).toBeUndefined();
  });

  function createControllerWith(memberId, event) {
    var dependencies = {enrollmentService: enrollmentServiceMock};
    var directiveParams = {memberId: memberId, event: event};

    return $controller('EventUnderTermChoosingController', dependencies, directiveParams);
  }
});
