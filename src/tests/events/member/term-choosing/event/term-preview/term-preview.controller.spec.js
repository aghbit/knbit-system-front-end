/**
 * Created by novy on 18.10.15.
 */

describe('TermPreviewController', function () {

  var $controller, $q, $rootScope;

  beforeEach(module('knbitFrontend.Events.Members.TermChoosing.Event.TermPreview'));
  beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
    $controller = _$controller_;
    $q = _$q_;
    $rootScope = _$rootScope_;
  }));

  it("should toggle enrolled property on term and increase participant count on successful enrollment", function () {
    // given
    var term = {
      enrolled: false,
      participantsEnrolled: 666
    };
    var enrollmentCallback = function () {
      return $q.when({});
    };

    var objectUnderTest = createControllerWith(term, angular.noop, enrollmentCallback, angular.noop);

    // when
    objectUnderTest.enrollAndUpdate();
    $rootScope.$apply();

    // then
    expect(term).toEqual(jasmine.objectContaining({
      enrolled: true, participantsEnrolled: 667
    }));
  });

  it("should toggle enrolled property on term and decrease participant count on successful disenroll", function () {
    // given
    var term = {
      enrolled: true,
      participantsEnrolled: 666
    };
    var disenrollCallback = function () {
      return $q.when({});
    };

    var objectUnderTest = createControllerWith(term, angular.noop, angular.noop, disenrollCallback);

    // when
    objectUnderTest.disenrollAndUpdate();
    $rootScope.$apply();

    // then
    expect(term).toEqual(jasmine.objectContaining({
      enrolled: false, participantsEnrolled: 665
    }));
  });

  it("should not be able to enroll if told not to do so", function () {
    // when
    var term = {
      enrolled: false,
      participantsEnrolled: 0,
      participantsLimit: 666
    };
    var notAllowedToEnroll = () => false;
    var objectUnderTest = createControllerWith(term, notAllowedToEnroll, angular.noop, angular.noop);

    // then
    expect(objectUnderTest.canEnroll()).toBeFalsy();
  });

  it("should not be able to enroll if participant count exceeds limit", function () {
    // when
    var term = {
      enrolled: false,
      participantsEnrolled: 666,
      participantsLimit: 666
    };
    var allowedToEnroll = () => true;
    var objectUnderTest = createControllerWith(term, allowedToEnroll, angular.noop, angular.noop);

    // then
    expect(objectUnderTest.canEnroll()).toBeFalsy();
  });

  function createControllerWith(term, allowedToEnrolledCallback, enrollCallback, disenrollCallback) {
    var directiveParams = {
      term: term,
      allowedToEnroll: allowedToEnrolledCallback,
      enroll: enrollCallback,
      disenroll: disenrollCallback
    };

    return $controller('TermPreviewUnderTermChoosingCtrl', {}, directiveParams);
  }
});
