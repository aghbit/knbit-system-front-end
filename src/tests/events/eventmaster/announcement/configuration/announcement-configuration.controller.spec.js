/**
 * Created by novy on 20.09.15.
 */

describe('AnnouncementConfigurationController ', function () {

  var $controller;
  var $q;
  var $rootScope;
  var publisherConfigurationServiceMock;
  var modalServiceMock;

  beforeEach(module('knbitFrontend.Events.EventMaster.Announcement.Configuration'));
  beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
    $controller = _$controller_;
    $q = _$q_;
    $rootScope = _$rootScope_;
  }));
  beforeEach(function () {
    publisherConfigurationServiceMock =
      jasmine.createSpyObj('publisherConfigurationService', ['basicConfigurationData']);

    modalServiceMock = jasmine.createSpyObj('configurationModalService', ['confirmConfigurationDeletion']);
  });

  it("should go for basicConfigurationData on init", function () {
    // given
    publisherConfigurationServiceMock.basicConfigurationData.and.returnValue($q.when([]));

    // when
    createController();

    // then
    expect(publisherConfigurationServiceMock.basicConfigurationData).toHaveBeenCalled();
  });

  it("should transform basicConfigurationData by appending callbacks", function () {
    // given
    var soleConfiguration = {foo: 'bar'};
    publisherConfigurationServiceMock.basicConfigurationData.and.returnValue($q.when([soleConfiguration]));
    var objectUnderTest = createController();

    // when
    $rootScope.$apply();

    // then
    var transformedConfiguration = objectUnderTest.configurationsWithCallbacks[0];
    expect(transformedConfiguration.configuration).toBe(soleConfiguration);
    expect(transformedConfiguration.editCallback).toBeDefined();
    expect(transformedConfiguration.removeCallback).toBeDefined();
  });


  function createController() {
    var injectionParams = {
      publisherConfigurationService: publisherConfigurationServiceMock,
      configurationModalService: modalServiceMock
    };

    return $controller('AnnouncementConfigurationController', injectionParams);
  }
});
