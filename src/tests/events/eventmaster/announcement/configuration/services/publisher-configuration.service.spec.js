/**
 * Created by novy on 20.09.15.
 */

describe('PublisherConfigurationService ', function () {

  var objectUnderTest;

  var facebookConfigurationResourceMock;
  var twitterConfigurationResourceMock;
  var googleGroupConfigurationResourceMock;
  var iietBoardConfigurationResourceMock;

  beforeEach(module('knbitFrontend.Events.EventMaster.Announcement.Configuration.Services', function ($provide) {
    var methodsToSpy = ['getConfigurationDetails', 'createConfiguration', 'updateConfiguration', 'deleteConfiguration'];
    facebookConfigurationResourceMock = jasmine.createSpyObj('facebookConfigurationResourceMock', methodsToSpy);
    twitterConfigurationResourceMock = jasmine.createSpyObj('twitterConfigurationResourceMock', methodsToSpy);
    googleGroupConfigurationResourceMock = jasmine.createSpyObj('googleGroupConfigurationResourceMock', methodsToSpy);
    iietBoardConfigurationResourceMock = jasmine.createSpyObj('iietBoardConfigurationResourceMock', methodsToSpy);

    $provide.value('facebookConfigurationResource', facebookConfigurationResourceMock);
    $provide.value('twitterConfigurationResource', twitterConfigurationResourceMock);
    $provide.value('googleGroupConfigurationResource', googleGroupConfigurationResourceMock);
    $provide.value('iietBoardConfigurationResource', iietBoardConfigurationResourceMock);
    $provide.value('basicConfigurationDataResource', {});
  }));
  beforeEach(inject(function (publisherConfigurationService) {
    objectUnderTest = publisherConfigurationService;
  }));


  it("requested to get configuration details, should dispatch request to proper resource", function () {
    // when
    objectUnderTest.getConfigurationDetails(666, 'FACEBOOK');

    // then
    expect(facebookConfigurationResourceMock.getConfigurationDetails).toHaveBeenCalledWith(666);
  });

  it("requested to create new configuration, should dispatch request to proper resource", function () {
    // given
    var newConfiguration = {
      foo: 'bar'
    };

    // when
    objectUnderTest.createConfiguration(newConfiguration, 'TWITTER');

    // then
    expect(twitterConfigurationResourceMock.createConfiguration).toHaveBeenCalledWith(newConfiguration);
  });

  it("requested to update existing configuration, should dispatch request to proper resource", function () {
    // given
    var confId = 666;
    var configurationToUpdate = {
      foo: 'bar'
    };

    // when
    objectUnderTest.editConfiguration(confId, configurationToUpdate, 'GOOGLE_GROUP');

    // then
    expect(googleGroupConfigurationResourceMock.updateConfiguration).toHaveBeenCalledWith(confId, configurationToUpdate);
  });

  it("requested to delete configuration, should dispatch request to proper resource", function () {
    // when
    objectUnderTest.deleteConfiguration(333, 'IIET_BOARD');

    // then
    expect(iietBoardConfigurationResourceMock.deleteConfiguration).toHaveBeenCalledWith(333);
  });

  it("should throw an exception invoked action with unknown vendor", function () {
    // when
    var invalidCallClosure = function () {
      objectUnderTest.deleteConfiguration(333, 'UNKNOWN PROVIDER')
    };

    // then
    expect(invalidCallClosure).toThrow();
  });
});
