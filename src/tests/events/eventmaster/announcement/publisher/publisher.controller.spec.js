/**
 * Created by novy on 13.04.15.
 */

describe('announcementPublishingController should', function () {

  var $rootScope;
  var $q;
  var AnnouncementPublishingServiceMock;
  var publisherModalServiceMock;
  var ToastingServiceMock;
  var objectUnderTest;

  beforeEach(module('knbitFrontend.Events.EventMaster.Announcement.Publisher'));
  beforeEach(inject(function ($controller, _$rootScope_, _$q_) {
    $rootScope = _$rootScope_;
    $q = _$q_;

    AnnouncementPublishingServiceMock = jasmine.createSpyObj('AnnouncementPublishingService', ['publishAnnouncement']);
    publisherModalServiceMock = jasmine.createSpyObj('publisherModalService', ['openPublishingAnnouncementModal']);
    ToastingServiceMock = jasmine.createSpyObj('ToastingService', ['showSuccessToast', 'showErrorToast']);

    objectUnderTest = $controller('AnnouncementPublishingController', {
      AnnouncementPublishingService: AnnouncementPublishingServiceMock,
      publisherModalService: publisherModalServiceMock,
      ToastingService: ToastingServiceMock,
      Messages: {ANNOUNCEMENT_PUBLISHER: {}}
    });

  }));

  it("take result from modal and try to publish it", function () {
    // given
    var announcement = {
      title: 'title',
      content: 'content',
      imageUrl: '',
      publishers: []
    };
    publisherModalServiceMock.openPublishingAnnouncementModal.and.returnValue($q.when(announcement));
    AnnouncementPublishingServiceMock.publishAnnouncement.and.returnValue($q.when({}));

    // when
    objectUnderTest.publishAnnouncement();
    $rootScope.$apply();

    // then
    expect(AnnouncementPublishingServiceMock.publishAnnouncement).toHaveBeenCalledWith(announcement);
  });

  it('display success toast if service call succeeded', function () {

    // given
    publisherModalServiceMock.openPublishingAnnouncementModal.and.returnValue($q.when({}));
    AnnouncementPublishingServiceMock.publishAnnouncement.and.returnValue($q.when({}));

    // when
    objectUnderTest.publishAnnouncement();
    $rootScope.$apply();

    // then
    expect(ToastingServiceMock.showSuccessToast).toHaveBeenCalled();
  });

  it("display error toast if service call failed", function () {

    // given:
    publisherModalServiceMock.openPublishingAnnouncementModal.and.returnValue($q.when({}));
    AnnouncementPublishingServiceMock.publishAnnouncement.and.returnValue($q.reject({}));


    // when
    objectUnderTest.publishAnnouncement();
    $rootScope.$apply();

    // then
    expect(ToastingServiceMock.showErrorToast).toHaveBeenCalled();
  });

  it("should not display any toast if modal was dismissed", function () {
    // given:
    publisherModalServiceMock.openPublishingAnnouncementModal.and.returnValue($q.reject({}));
    AnnouncementPublishingServiceMock.publishAnnouncement.and.returnValue($q.when({}));


    // when
    objectUnderTest.publishAnnouncement();
    $rootScope.$apply();

    // then
    expect(ToastingServiceMock.showSuccessToast).not.toHaveBeenCalled();
    expect(ToastingServiceMock.showErrorToast).not.toHaveBeenCalled();
  });
});
