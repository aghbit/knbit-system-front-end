/**
 * Created by novy on 29.08.15.
 */

describe("SurveyQuestionnaireController", function () {

  var moduleName =
    'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.Questionnaire';

  var $controller;

  beforeEach(module(moduleName));
  beforeEach(inject(function (_$controller_) {
    $controller = _$controller_;
  }));

  it("should add question with 2 empty answers and SINGLE_CHOICE type asked for new question", function () {
    //given
    var questions = [];
    var objectUnderTest = controllerWithQuestion(questions);

    //when
    objectUnderTest.addNewQuestion();

    //then
    var newQuestion = {
      title: '',
      description: '',
      type: 'SINGLE_CHOICE',
      answers: ['', '']
    };
    expect(questions).toEqual([newQuestion]);
  });

  it("should remove nth question if asked", function () {
    //given
    var questions = ["q1", "q2", "q3"];
    var objectUnderTest = controllerWithQuestion(questions);

    //when
    objectUnderTest.removeQuestion(1);

    //then
    expect(questions).toEqual(["q1", "q3"]);
  });

  function controllerWithQuestion(questions) {
    var directiveParams = {questions: questions};
    return $controller('SurveyQuestionnaireController', {}, directiveParams);
  }
});

