/**
 * Created by novy on 29.08.15.
 */

describe("SurveyPoliciesDirective", function () {

  var moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.SurveyPolicies';

  var scope;
  var objectUnderTest;
  var viewModel;

  var htmlTemplate =
    '<knbit-events-survey-policies end-of-surveying="endOfSurveying" ' +
    'minimal-interest-threshold="minimalInterestThreshold">' +
    '</knbit-events-survey-policies>';

  beforeEach(module('templates'));
  beforeEach(module(moduleName));
  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.endOfSurveying = {};
    scope.minimalInterestThreshold = {};

    objectUnderTest = $compile(htmlTemplate)(scope);
    scope.$digest();
    viewModel = objectUnderTest.isolateScope().vm;
  }));

  it("if memberVotesNotificationPolicy checkbox becomes unchecked, it should set minimalInterestThreshold to null", function () {
    //given
    memberVotesNotificationPolicyCheckboxClicked();
    setMinimalInterestThresholdTo(66);

    // when
    memberVotesNotificationPolicyCheckboxClicked();

    //then
    expect(viewModel.minimalInterestThreshold).toBeNull();
  });

  it("should update minimalInterestThreshold on user input if memberVotesNotificationPolicy checked", function () {
    //given
    memberVotesNotificationPolicyCheckboxClicked();

    //when
    setMinimalInterestThresholdTo(66);

    //then
    expect(viewModel.minimalInterestThreshold).toEqual('66');
  });

  it("should set minimalInterestThreshold to last selected value if memberVotesNotificationPolicy checkbox becomes checked", function () {
    //given
    memberVotesNotificationPolicyCheckboxClicked();
    setMinimalInterestThresholdTo(66);
    memberVotesNotificationPolicyCheckboxClicked();

    //when
    memberVotesNotificationPolicyCheckboxClicked();

    //then
    expect(viewModel.minimalInterestThreshold).toEqual('66');
  });

  it("should ignore new minimalInterestThreshold value if memberVotesNotification checkbox is unchecked", function () {
    //given
    memberVotesNotificationPolicyCheckboxClicked();
    memberVotesNotificationPolicyCheckboxClicked();

    //when
    setMinimalInterestThresholdTo(33);

    //then
    expect(viewModel.minimalInterestThreshold).toBeNull();
  });

  function memberVotesNotificationPolicyCheckboxClicked() {
    var memberVotesNotificationPolicyCheckbox =
      angular.element(objectUnderTest.find('[name="memberVotesNotificationPolicy"]')[0]);
    memberVotesNotificationPolicyCheckbox.trigger('click');
    scope.$apply();
  }

  function setMinimalInterestThresholdTo(newInterestThreshold) {
    var minimalInterestThresholdInput =
      angular.element(objectUnderTest.find('[name="minimalInterestThreshold"]')[0]);
    minimalInterestThresholdInput.val(newInterestThreshold).trigger('input');
    scope.$apply();
  }

});
