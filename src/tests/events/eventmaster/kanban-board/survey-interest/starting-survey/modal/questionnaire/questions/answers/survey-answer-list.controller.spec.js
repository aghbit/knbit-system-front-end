/**
 * Created by novy on 29.08.15.
 */

describe("SurveyAnswerListController", function () {

  var moduleName =
    'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.Questionnaire';

  var $controller;

  beforeEach(module(moduleName));
  beforeEach(inject(function (_$controller_) {
    $controller = _$controller_;
  }));

  it("should add empty answers if requested", function () {
    //given
    var answers = [];
    var objectUnderTest = controllerWithAnswers(answers);

    //when
    objectUnderTest.addEmptyAnswer();

    //then
    expect(answers).toEqual(['']);
  });

  it("should remove nth answer if asked", function () {
    //given
    var answers = ["a1", "a2", "a3"];
    var objectUnderTest = controllerWithAnswers(answers);

    //when
    objectUnderTest.removeAnswer(0);

    //then
    expect(answers).toEqual(["a2", "a3"]);
  });

  function controllerWithAnswers(answers) {
    var directiveParams = {answers: answers};
    return $controller('SurveyAnswerListController', {}, directiveParams);
  }
});

