/**
 * Created by novy on 29.08.15.
 */

describe("SurveyPoliciesDirective", function () {

  var moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.SurveyPolicies';

  var scope;
  var objectUnderTest;
  var viewModel;

  var htmlTemplate =
    '<knbit-events-survey-policies end-of-surveying="endOfSurveying" ' +
    'minimal-interest-threshold="minimalInterestThreshold">' +
    '</knbit-events-survey-policies>';

  beforeEach(module('templates'));
  beforeEach(module(moduleName));
  beforeEach(module('knbitFrontend.Components.DatePicker'));
  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.endOfSurveying = {};
    scope.minimalInterestThreshold = {};

    objectUnderTest = $compile(htmlTemplate)(scope);
    scope.$digest();
    viewModel = objectUnderTest.isolateScope().vm;
  }));

  it("if timeBasedEndingPolicy checkbox becomes unchecked, it should set endOfSurveying to null", function () {
    //given
    timeBasedEndingPolicyCheckboxClicked();
    setEndOfSurveyingTo(moment('2014-04-25T01:32:21.196Z'));

    // when
    timeBasedEndingPolicyCheckboxClicked();

    //then
    expect(viewModel.endOfSurveying).toBeNull();
  });

  it("should update endOfSurveying on user input if timeBasedEndingPolicy checked", function () {
    //given
    var expectedDate = moment('2014-04-25T01:32:21.196Z');
    timeBasedEndingPolicyCheckboxClicked();

    //when
    setEndOfSurveyingTo(expectedDate);

    //then
    expect(viewModel.endOfSurveying).toEqual(expectedDate);
  });

  it("should set endOfSurveying to last selected value if timeBasedEndingPolicy checkbox becomes checked", function () {
    //given
    var expectedDate = moment('2014-04-25T01:32:21.196Z');
    timeBasedEndingPolicyCheckboxClicked();
    setEndOfSurveyingTo(expectedDate);
    timeBasedEndingPolicyCheckboxClicked();

    //when
    timeBasedEndingPolicyCheckboxClicked();

    //then
    expect(viewModel.endOfSurveying).toEqual(expectedDate);
  });

  it("should ignore new endOfSurveying value if timeBasedEndingPolicy checkbox is unchecked", function () {
    //given
    timeBasedEndingPolicyCheckboxClicked();
    timeBasedEndingPolicyCheckboxClicked();

    //when
    setEndOfSurveyingTo(moment('2014-04-25T01:32:21.196Z'));

    //then
    expect(viewModel.endOfSurveying).toBeNull();
  });

  function timeBasedEndingPolicyCheckboxClicked() {
    var timeBasedEndingPolicyCheckbox = angular.element(objectUnderTest.find('[name="timeBasedEndingPolicy"]')[0]);
    timeBasedEndingPolicyCheckbox.trigger('click');
    scope.$apply();
  }

  function setEndOfSurveyingTo(newSurveyingDate) {
    var endOfSurveyingInput = angular.element(objectUnderTest.find('[name="endOfSurveying"]')[0]);
    endOfSurveyingInput.trigger('change', newSurveyingDate);
    scope.$apply();
  }

});
