describe("SurveyPoliciesDirective", function () {

  var moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.SurveyPolicies';

  var initialDirectiveParams = {
    endOfSurveying: "initial end of surveying",
    minimalInterestThreshold: "initial threshold"
  };

  var scope;
  var objectUnderTest;
  var viewModel;

  var htmlTemplate =
    '<knbit-events-survey-policies ' +
    'end-of-surveying="initialDirectiveParams.endOfSurveying" ' +
    'minimal-interest-threshold="initialDirectiveParams.minimalInterestThreshold"> ' +
    '</knbit-events-survey-policies>';


  beforeEach(module('templates'));
  beforeEach(module(moduleName));
  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.initialDirectiveParams = initialDirectiveParams;

    objectUnderTest = $compile(htmlTemplate)(scope);
    scope.$digest();
    viewModel = objectUnderTest.isolateScope().vm;
  }));

  it("should initialize with both policies unchecked", function () {
    expect(viewModel.timeBasedEndingPolicy).toBeFalsy();
    expect(viewModel.memberVotesNotificationPolicy).toBeFalsy();
  });

  it("should initialize with default endOfSurveying value", function () {
    expect(viewModel.endOfSurveying).toBe(initialDirectiveParams.endOfSurveying);
  });

  it("should initialize with default minimalInterestThreshold value", function () {
    expect(viewModel.minimalInterestThreshold).toBe(initialDirectiveParams.minimalInterestThreshold);
  });
});
