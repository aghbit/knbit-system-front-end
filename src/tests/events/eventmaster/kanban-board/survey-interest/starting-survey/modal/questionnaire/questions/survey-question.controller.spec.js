/**
 * Created by novy on 29.08.15.
 */

describe("SurveyQuestionController", function () {

  var moduleName =
    'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.Questionnaire';

  var $controller;

  beforeEach(module(moduleName));
  beforeEach(inject(function (_$controller_) {
    $controller = _$controller_;
  }));

  it("should delete all answers if question type changed to TEXT", function () {
    // given
    var question = {
      type: 'MULTIPLE_CHOICE',
      answers: [
        'ans1', 'ans2', 'ans3'
      ]
    };
    var objectUnderTest = controllerFor(question);

    //when
    question.type = 'TEXT';
    objectUnderTest.onQuestionTypeChanged();

    // then
    expect(question.answers).toEqual([])
  });

  it("should preserve answers if changing to non-TEXT type and had more than 1 answers", function () {
    // given
    var question = {
      type: 'MULTIPLE_CHOICE',
      answers: [
        'ans1', 'ans2', 'ans3'
      ]
    };
    var objectUnderTest = controllerFor(question);

    //when
    question.type = 'SINGLE_CHOICE';
    objectUnderTest.onQuestionTypeChanged();

    // then
    expect(question.answers).toEqual(['ans1', 'ans2', 'ans3'])
  });

  it("should set two empty answers if changing to non-TEXT type and had no answers before", function () {
    // given
    var question = {
      type: 'TEXT',
      answers: []
    };
    var objectUnderTest = controllerFor(question);

    //when
    question.type = 'SINGLE_CHOICE';
    objectUnderTest.onQuestionTypeChanged();

    // then
    expect(question.answers).toEqual(['', ''])
  });

  it("should return true asked if SINGLE_CHOICE question is choosable", function () {
    // when
    var objectUnderTest = controllerFor({type: 'SINGLE_CHOICE'});

    // then
    expect(objectUnderTest.isQuestionChoosable()).toBeTruthy();
  });

  it("should return true asked if MULTIPLE_CHOICE choice question is choosable", function () {
    // when
    var objectUnderTest = controllerFor({type: 'MULTIPLE_CHOICE'});

    // then
    expect(objectUnderTest.isQuestionChoosable()).toBeTruthy();
  });

  it("should return false asked if TEXT choice question is choosable", function () {
    // when
    var objectUnderTest = controllerFor({type: 'TEXT'});

    // then
    expect(objectUnderTest.isQuestionChoosable()).toBeFalsy();
  });

  function controllerFor(question) {
    var directiveParams = {
      question: question
    };

    return $controller('SurveyQuestionController', {}, directiveParams);
  }
});
