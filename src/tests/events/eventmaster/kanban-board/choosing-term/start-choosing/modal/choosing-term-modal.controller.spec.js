/**
 * Created by novy on 06.09.15.
 */

describe('ChoosingTermModalController', function () {

  var $controller;
  var modalInstanceMock;

  beforeEach(module('knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.StartingChoosingPopup'));
  beforeEach(inject(function (_$controller_) {
    $controller = _$controller_;
    modalInstanceMock = jasmine.createSpyObj('$modalInstance', ['close']);
  }));

  it("should initialize with empty terms", function () {
    // when
    var objectUnderTest = createController();

    // then
    expect(objectUnderTest.terms).toEqual([]);
  });


  it("should split data between terms and term proposals", function () {
    // given
    var objectUnderTest = createController();
    objectUnderTest.terms = [
      {id: 1, roomBooking: false},
      {id: 2, roomBooking: true},
      {id: 3, roomBooking: true},
      {id: 4, roomBooking: false}
    ];

    // when
    objectUnderTest.submit();

    // then
    expect(modalInstanceMock.close).toHaveBeenCalledWith({
      terms: [{id: 1, roomBooking: false}, {id: 4, roomBooking: false}],
      termProposals: [{id: 2, roomBooking: true}, {id: 3, roomBooking: true}]
    });
  });

  function createController() {
    return $controller('ChoosingTermModalController', {$modalInstance: modalInstanceMock});
  }
});
