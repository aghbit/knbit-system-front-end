/**
 * Created by novy on 05.09.15.
 */

describe('ChoosingTermPreviewTermsController', function () {

  var moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.TermPreview.Terms';

  var termsModalServiceMock;
  var rootScope;
  var $controller;
  var $q;

  beforeEach(module(moduleName));
  beforeEach(inject(function ($rootScope, _$controller_, _$q_) {
    rootScope = $rootScope;
    $controller = _$controller_;
    $q = _$q_;

    termsModalServiceMock = jasmine.createSpyObj('termsModalService', [
      'confirmTermRemoval',
      'confirmReservationCancellation',
      'openNewTermModal'
    ]);

    termsModalServiceMock.confirmTermRemoval.and.returnValue($q.when({}));
    termsModalServiceMock.confirmReservationCancellation.and.returnValue($q.when({}));
    termsModalServiceMock.openNewTermModal.and.returnValue($q.when({}))
  }));

  it("should open confirmation dialog on term removal request", function () {
    // given
    var soleTerm = {capacity: 666};
    var objectUnderTest = createController({terms: [soleTerm]});

    // when
    objectUnderTest.removeTerm(soleTerm);

    // expect
    expect(termsModalServiceMock.confirmTermRemoval).toHaveBeenCalled();
  });

  it("should call removeTermCallback if modal confirmed on removal", function () {
    // given
    var soleTerm = {capacity: 666};
    var removeTermCallback = jasmine.createSpy();
    var objectUnderTest = createController({
      terms: [soleTerm],
      removeTermCallback: removeTermCallback
    });

    // when
    objectUnderTest.removeTerm(soleTerm);
    rootScope.$apply();

    // expect
    expect(removeTermCallback).toHaveBeenCalledWith({term: soleTerm});
  });

  it("should open confirmation dialog on reservation cancel request", function () {
    // given
    var soleReservation = {id: 666};
    var objectUnderTest = createController({reservations: [soleReservation]});

    // when
    objectUnderTest.cancelReservation(soleReservation);

    // expect
    expect(termsModalServiceMock.confirmReservationCancellation).toHaveBeenCalled();
  });

  it("should call cancelReservationCallback if modal confirmed on cancellation request", function () {
    // given
    var soleReservation = {id: 666};
    var cancelReservationCallback = jasmine.createSpy();
    var objectUnderTest = createController({
      reservations: [soleReservation],
      cancelReservationCallback: cancelReservationCallback
    });

    // when
    objectUnderTest.cancelReservation(soleReservation);
    rootScope.$apply();

    // expect
    expect(cancelReservationCallback).toHaveBeenCalledWith({reservation: soleReservation});
  });

  it("should open new term modal on request", function () {
    // given
    var objectUnderTest = createController({});

    // when
    objectUnderTest.addNewTerm();

    // expect
    expect(termsModalServiceMock.openNewTermModal).toHaveBeenCalled();
  });

  it("should call newTermAdded callback if modal confirmed on addition", function () {
    // given
    var newTerm = 'newTerm';
    termsModalServiceMock.openNewTermModal.and.returnValue(
      $q.when(newTerm)
    );

    var newTermCallback = jasmine.createSpy();
    var objectUnderTest = createController({
      newTermCallback: newTermCallback
    });

    // when
    objectUnderTest.addNewTerm();
    rootScope.$apply();

    // expect
    expect(newTermCallback).toHaveBeenCalledWith({newTerm: newTerm});
  });

  function createController(directiveParams) {
    return $controller('ChoosingTermPreviewTermsController', {termsModalService: termsModalServiceMock}, directiveParams);
  }
});
