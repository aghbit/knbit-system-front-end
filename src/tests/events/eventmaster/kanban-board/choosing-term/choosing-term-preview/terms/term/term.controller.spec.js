/**
 * Created by novy on 05.09.15.
 */

describe('ChoosingTermPreviewTermController', function () {

  let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.TermPreview.Terms';

  var $controller;
  var $filter;

  var millenniumEpochInMillis = new Date(2000, 0, 1, 0, 0, 0, 0).valueOf();

  beforeEach(module(moduleName));
  beforeEach(inject(function (_$controller_, _$filter_) {
    $controller = _$controller_;
    $filter = _$filter_;
  }));

  it("should create property with properly formatted date", function () {
    // when
    var objectUnderTest = createControllerWithTerm({date: millenniumEpochInMillis});

    // then
    expect(objectUnderTest.formattedDate()).toBe('Jan 1, 2000');
  });

  it("should properly parse start time", function () {
    // when
    var objectUnderTest = createControllerWithTerm({date: millenniumEpochInMillis});

    // then
    expect(objectUnderTest.startTime()).toBe('12:00 AM');
  });

  it("should properly parse end time", function () {
    // when
    var durationInMinutes = 90;
    var objectUnderTest = createControllerWithTerm({
      date: millenniumEpochInMillis,
      duration: durationInMinutes
    });

    // then
    expect(objectUnderTest.endTime()).toBe('01:30 AM');
  });

  it("given term with location, should instruct view to display it", function () {
    var objectUnderTest = createControllerWithTerm({location: '3.27A'});

    // then
    expect(objectUnderTest.shouldDisplayLocation()).toBeTruthy();
  });

  it("given term without location, should instruct view not to display it", function () {
    var objectUnderTest = createControllerWithTerm({date: millenniumEpochInMillis});

    // then
    expect(objectUnderTest.shouldDisplayLocation()).toBeFalsy();
  });

  function createControllerWithTerm(term) {
    var injectionParams = {$filter: $filter};
    var directiveParams = {term: term};

    return $controller('ChoosingTermPreviewTermController', injectionParams, directiveParams);
  }
});
