/**
 * Created by novy on 12.05.15.
 */

describe('EventProposalController', function () {

  var rootScope;
  var $q;
  var modalMock;
  var EventProposalResourceMock;
  var ToastingServiceMock;

  beforeEach(module('knbitFrontend.Events.EventMaster.Proposals'));
  beforeEach(inject(function ($controller, $rootScope, _$q_) {

    rootScope = $rootScope;
    $q = _$q_;
    modalMock = jasmine.createSpyObj('$modal', ['open']);
    EventProposalResourceMock = jasmine.createSpyObj('EventProposalResource', ['proposalBy', 'changeState']);
    EventProposalResourceMock.proposalBy.and.returnValue($q.when({}));
    EventProposalResourceMock.changeState.and.returnValue($q.when({}));
    ToastingServiceMock = jasmine.createSpyObj('ToastingService', ['showSuccessToast', 'showErrorToast']);

    $controller('EventProposalController', {
      $rootScope: $rootScope,
      $modal: modalMock,
      EventProposalResource: EventProposalResourceMock,
      ToastingService: ToastingServiceMock
    });

  }));

  it('given proper event on root scope, it should change proposal state based on modal return value', function () {

    //given
    var expectedProposalId = 'domainId';
    var expectedStateChangeAction = 'ACCEPTED';
    modalMock.open.and.returnValue({
      result: $q.when(expectedStateChangeAction)
    });

    //when
    rootScope.$emit('notification-bar:event-proposal-selected', expectedProposalId);
    rootScope.$apply();

    //then
    expect(EventProposalResourceMock.changeState).toHaveBeenCalledWith(
      expectedProposalId, expectedStateChangeAction
    );

  });

  it('on successful state change via EventProposalResource, it should display success toast', function () {

    //given
    modalMock.open.and.returnValue({
      result: $q.when('ACCEPTED')
    });

    EventProposalResourceMock.changeState.and.returnValue(
      $q.when('ACCEPTED')
    );

    //when
    rootScope.$emit('notification-bar:event-proposal-selected', 'domainId');
    rootScope.$apply();

    //then
    expect(ToastingServiceMock.showSuccessToast).toHaveBeenCalled();

  });

  it('if EventProposalResource fails to change proposal state, it should display error toast', function () {

    //given
    modalMock.open.and.returnValue({
      result: $q.when('ACCEPTED')
    });

    EventProposalResourceMock.changeState.and.returnValue(
      $q.reject({})
    );

    //when
    rootScope.$emit('notification-bar:event-proposal-selected', 'domainId');
    rootScope.$apply();

    //then
    expect(ToastingServiceMock.showErrorToast).toHaveBeenCalled();

  });

});
