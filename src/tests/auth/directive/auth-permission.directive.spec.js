/**
 * Created by novy on 29.11.15.
 */

describe('auth permission directive', function () {

  var $rootScope;
  var $q;
  var $compile;
  var AuthServiceMock;

  beforeEach(module('ui.router'));
  beforeEach(module('AuthModule', function ($provide) {
    AuthServiceMock = jasmine.createSpyObj('AuthService', ['hasPermission']);
    $provide.value('AuthService', AuthServiceMock);
  }));
  beforeEach(inject(function (_$rootScope_, _$q_, _$compile_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $compile = _$compile_;
  }));

  describe('given markup with secured content', function () {
    var markup;

    beforeEach(function () {
      markup = '<div><span has-permission="PERM">should be secured</span></div>';
    });

    it('should include markup in DOM if user has permission', function () {
      // given
      pretendHasPermission(true);
      var compiledElement = compiledMarkup(markup);

      // when
      var securedSpan = compiledElement.find('span');

      // then
      expect(securedSpan.length).toBe(1);
    });

    it('should not include markup in DOM if no permission for given user', function () {
      // given
      pretendHasPermission(false);
      var compiledElement = compiledMarkup(markup);

      // when
      var securedSpan = compiledElement.find('span');

      // then
      expect(securedSpan.length).toBe(0);
    });
  });

  function pretendHasPermission(hasPermission) {
    AuthServiceMock.hasPermission.and.returnValue($q.when(hasPermission));
  }

  function compiledMarkup(markup) {
    var compiledElement = $compile(markup)($rootScope);
    $rootScope.$digest();
    return compiledElement;
  }
});
