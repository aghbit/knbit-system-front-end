
describe('NotificationBarCtrl should', function() {

  var scope;
  var NotificationBarCtrl;

  beforeEach(module('knbitFrontend.Components.NotificationBar'));
  beforeEach(inject(function($controller, $rootScope, $q) {
    scope = $rootScope.$new();

    var NotificationBarServiceMock = jasmine.createSpyObj('NotificationBarService', ['receiveCurrentMessages', 'receiveBatchMessages']);
    NotificationBarServiceMock.receiveCurrentMessages.and.returnValue($q.when({}));
    NotificationBarServiceMock.receiveBatchMessages.and.returnValue($q.when({}));

    NotificationBarCtrl = $controller('NotificationBarCtrl', {
      $scope: scope,
      $state: {},
      NotificationBarService: NotificationBarServiceMock
    });
  }));

  it('properly synchronize messages', function() {
    // given
    var messageMap = new Map();
    messageMap.set('1', {id: '1', read: false, payload: {id: '1'}});
    messageMap.set('2', {id: '2', read: false, payload: {id: '2'}});

    // when
    NotificationBarCtrl.synchronizeCurrentView(messageMap);

    // then
    expect(scope.messages).toEqual([
      {id: '2', read: false, payload: {id: '2'}},
      {id: '1', read: false, payload: {id: '1'}}
    ]);
  });

  it('properly synchronize unread messages count', function() {
    // given
    var messageMap = new Map();
    messageMap.set('1', {id: '1', read: false, payload: {}});
    messageMap.set('2', {id: '2', read: true, payload: {}});

    // when
    NotificationBarCtrl.synchronizeCurrentView(messageMap);

    // then
    expect(scope.unreadMessagesCount).toBe(1);
  });

  it('push messages to front of empty map', function() {
    // given
    var mapToBeModified = new Map();
    var messagesToBePushedFront = [
      {id: '1', read: false, payload: "{\"id\": \"1\"}"},
      {id: '2', read: false, payload: "{\"id\": \"2\"}"}
    ];

    // when
    var concatenatedMap = NotificationBarCtrl.pushMessagesToFrontOfMap(messagesToBePushedFront, mapToBeModified);

    // then
    expect(flatMap(concatenatedMap)).toEqual([
      {id: '1', read: false, payload: {id: '1'}},
      {id: '2', read: false, payload: {id: '2'}}
    ]);
  });

  it('push messages to front of nonempty map', function() {
    // given
    var mapToBeModified = new Map();
    mapToBeModified.set('3', {id: '3', read: false, payload: {id: '3'}});
    var messagesToBePushedFront = [
      {id: '1', read: false, payload: "{\"id\": \"1\"}"},
      {id: '2', read: false, payload: "{\"id\": \"2\"}"}
    ];

    // when
    var concatenatedMap = NotificationBarCtrl.pushMessagesToFrontOfMap(messagesToBePushedFront, mapToBeModified);

    // then
    expect(flatMap(concatenatedMap)).toEqual([
      {id: '1', read: false, payload: {id: '1'}},
      {id: '2', read: false, payload: {id: '2'}},
      {id: '3', read: false, payload: {id: '3'}}
    ]);
  });

  // util functions
  function flatMap(map) {
    var values = [];
    map.forEach( val => values.push(val) );
    return values;
  }

});
