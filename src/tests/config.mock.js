/**
 * Created by novy on 15.05.15.
 */

angular.module('knbitFrontend.Components.Config', []);
angular.module('knbitFrontend.Events.Config', []);
angular.module('knbitFrontend.Home.Config', []);
angular.module('knbitFrontend.Projects.Config', []);
angular.module('knbitFrontend.Auth.Config', [])
  .constant('AuthEvents', {LOGGED_IN: 'loggedIn', LOGGED_OUT: 'loggedOut'})
  .constant('AuthUrls', {});
