'use strict';

var CreateSectionFormFactory = function () {
  return {
    getFields: function () {
      return [
        {
          key: 'name',
          type: 'input',
          templateOptions: {
            type: 'text',
            label: 'Section name',
            placeholder: 'Enter section name',
            required: true
          }
        },
        {
          key: 'description',
          type: 'textarea',
          templateOptions: {
            rows: 3,
            label: 'Description',
            placeholder: 'Enter description',
            required: true
          }
        },
        {
          key: 'logoResource',
          type: 'input',
          templateOptions: {
            type: 'text',
            label: 'Logo URL',
            placeholder: 'Enter logo URL',
            required: true
          }
        },
        {
          key: 'hexColor',
          type: 'input',
          templateOptions: {
            type: 'text',
            label: 'Color',
            placeholder: 'Enter HEX color, i.e. #ED088F',
            required: true
          }
        }
      ];
    }
  };
};

export default CreateSectionFormFactory;
