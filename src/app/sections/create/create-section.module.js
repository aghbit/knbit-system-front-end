'use strict';

import CreateSectionCtrl from './controller/create-section.controller.js';
import CreateSectionFormFactory from './factory/create-section-form.factory.js';

var CreateSectionModule = angular.module('CreateSectionModule', [
  'ui.bootstrap',
  'formly',
  'formlyBootstrap'
])
  .factory('CreateSectionFormFactory', CreateSectionFormFactory)
  .controller('CreateSectionCtrl', CreateSectionCtrl);

export default CreateSectionModule;
