'use strict';

class CreateSectionCtrl {
  constructor($scope, AuthService, CreateSectionFormFactory, $resource, sectionsUrl) {
    this.fields = CreateSectionFormFactory.getFields();
    this.scope = $scope;
    this.createSectionResource = $resource(sectionsUrl, {}, {
      create: {
        method: 'POST',
        headers: {
          'knbit-aa-auth': AuthService.getToken()
        }
      }
    });

    this.form = {};
  }

  cancel() {
    this.scope.$close(false);
  }

  create() {
    this.createSectionResource.create(this.form).$promise.then(() => {

    },
    () => {
      // todo alert handling
    }
    );
    this.scope.$close(true);
  }
}

CreateSectionCtrl.$inject = ['$scope', 'AuthService', 'CreateSectionFormFactory', '$resource', 'sectionsUrl'];

export default CreateSectionCtrl;
