'use strict';

class SectionDetailsCtrl {
  constructor($resource, sectionsUrl, $stateParams) {
    var sectionDetailsUrl = sectionsUrl + '/' + $stateParams.sectionId;
    $resource(sectionDetailsUrl).get().$promise.then((section) => {
      this.section = section;
    }, () => {
      // todo error alert
    });
  }
}

SectionDetailsCtrl.$inject = ['$resource', 'sectionsUrl', '$stateParams'];

export default SectionDetailsCtrl;
