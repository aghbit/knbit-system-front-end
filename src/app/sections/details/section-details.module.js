'use strict';

import SectionDetailsCtrl from './controller/section-details-controller.js';

var SectionDetailsModule = angular.module('SectionDetailsModule', [
  'knbitFrontend.Sections.Config'
])
  .controller('SectionDetailsCtrl', SectionDetailsCtrl);

export default SectionDetailsModule;
