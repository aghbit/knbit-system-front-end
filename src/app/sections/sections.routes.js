'use strict';

var Routes = function ($stateProvider) {
  $stateProvider
  .state('sections.overview', {
    url: '',
    views: {
      'content-page': {
        templateUrl: 'app/sections/overview/view/sections-overview.html',
        controller: 'SectionsOverviewCtrl',
        controllerAs: 'sectionsOverview'
      }
    }
  })
  .state('sections.create', {
    url: '/create',
    parent: 'sections.overview',
    onEnter: ['$modal', '$state', function ($modal, $state) {
      $modal.open({
        templateUrl: 'app/sections/create/view/create-section.view.html',
        controller: 'CreateSectionCtrl',
        controllerAs: 'createSection'
      }).result.finally(function () {
        $state.go('^');
      });
    }],
    data: {
      permission: 'MANAGE_ALL_SECTIONS'
    }
  })
  .state('sections.details', {
    url: '/{sectionId}',
    views: {
      'content-page': {
        templateUrl: 'app/sections/details/view/section-details.view.html',
        controller: 'SectionDetailsCtrl',
        controllerAs: 'sectionDetails'
      }
    }
  })
  .state('sections.management', {
    url: '/{sectionId}/management',
    views: {
      'content-page': {
        templateUrl: 'app/sections/management/view/section-management.html',
        controller: 'SectionManagementCtrl',
        controllerAs: 'sectionManagement'
      }
    },
    data: {
      permission: 'MANAGE_SECTION'
    }
  })
  .state('sections.compose-email', {
    url: '/compose-email',
    parent: 'sections.management',
    params: {userIdsList: {}},
    onEnter: ['$modal', '$state', function ($modal, $state) {
      $modal.open({
        templateUrl: 'app/notifications/compose-email.view.html',
        controller: 'ComposeEmailCtrl',
        controllerAs: 'composeEmail',
        size: 'lg'
      }).result.finally(function () {
        $state.go('^');
      });
    }],
    data: {
      permission: 'SEND_MAIL'
    }
  });
};


Routes.$inject = ['$stateProvider'];

export default Routes;
