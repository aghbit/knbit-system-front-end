'use strict';

class SectionManagementCtrl {
  constructor($resource, sectionsUrl, $stateParams, $state, usersListUrl, AuthService) {
    this.state = $state;
    this.resource = $resource;
    this.sectionsUrl = sectionsUrl;
    this.authHeader = {
      'knbit-aa-auth': AuthService.getToken()
    };
    this.commandFailed = false;

    var sectionDetailsUrl = sectionsUrl + '/' + $stateParams.sectionId;
    $resource(sectionDetailsUrl).get().$promise.then((section) => {
      this.section = section;
    }, () => {
      // todo error alert
    });
    this.candidatesResource = $resource(sectionDetailsUrl + '/candidates');
    //this.membersResource = $resource(sectionDetailsUrl + '/members');
    this.sectionManagersResource = $resource(sectionDetailsUrl + '/sectionManagers', {}, {
      promote: {
        method: 'POST',
        headers: this.authHeader
      }
    });
    this.usersResource = $resource(usersListUrl);
    this.membersResource = $resource(sectionDetailsUrl + '/members', {}, {
      addUser: {
        method: 'PUT',
        headers: this.authHeader
      }
    });
    this.removeUserResource = $resource(sectionDetailsUrl + '/members/:userId', {
      userId: '@id'
    }, {
      delete: {
        method: 'DELETE',
        headers: this.authHeader
      }
    });
    this.usersResource.get().$promise.then((usersList) => {
      this.bitUsers = usersList.values;
      this.fetchUsersInSection();
      this.otherBitUsers = this.bitUsers.filter((user) => {
        return user.state === 'accepted'
      });
    }, (error) => {
      this.commandHasFailed(error.data.reason)
    });
  }

  fetchUsersInSection() {
    this.candidatesResource.query().$promise.then((candidatesList) => {
      this.candidates = candidatesList;
      this.bitUsers.forEach((user) => {
        this.candidates.forEach((candidate) => {
          if (candidate.userId === user.userId) {
            candidate.firstName = user.firstName;
            candidate.lastName = user.lastName;
            candidate.email = user.email;
            candidate.startOfStudiesYear = user.startOfStudiesYear;
          }
        });
      });
    }, (error) => {
      this.commandHasFailed(error.data.reason)
    });

    this.membersResource.query().$promise.then((membersList) => {
      this.members = membersList;
      this.bitUsers.forEach((user) => {
        this.members.forEach((member) => {
          if (member.userId === user.userId) {
            member.firstName = user.firstName;
            member.lastName = user.lastName;
            member.email = user.email;
            member.startOfStudiesYear = user.startOfStudiesYear;
          }
        });
      });
    }, (error) => {
      this.commandHasFailed(error.data.reason)
    });
  }

  composeEmailToSelected(collection) {
    var userIdsList = this.getSelected(collection).map((user) => {
      return {'userId': user.userId}
    });
    this.state.go('sections.compose-email', {userIdsList: userIdsList});
  }

  addSelectedCandidatesToSection() {
    this.performActionForSelected(this.candidates, (userId) => this.addToSection(userId));
  }

  removeSelectedCandidatesFromSection() {
    this.performActionForSelected(this.candidates, (userId) => this.removeFromSection(userId));
  }

  promoteSelectedMembersToSectionManager() {
    this.performActionForSelected(this.members, (userId) => this.promoteToSectionManager(userId));
  }

  removeSelectedMembersFromSection() {
    this.performActionForSelected(this.members, (userId) => this.removeFromSection(userId));
  }

  addOtherBitMembersToSection() {
    this.performActionForSelected(this.otherBitUsers, (userId) => this.addToSection(userId));
  }


  performActionForSelected(collection, actionFun) {
    this.getSelected(collection).map(user => {
      actionFun({'userId': user.userId})
    });
  }

  getSelected(collection) {
    return collection.filter((user) => {
      return user.isSelected;
    });
  }

  addToSection(userId) {
    this.membersResource.addUser(userId).$promise.then(()=> {
      this.commandHasSucceeded();
      this.fetchUsersInSection();
    }, (error) => {
      this.commandHasFailed(error.data.reason);
    });
  }

  removeFromSection(userId) {
    this.removeUserResource.delete({userId: userId.userId}).$promise.then(()=> {
      this.commandHasSucceeded();
      this.fetchUsersInSection();
    }, (error) => {
      this.commandHasFailed(error.data.reason);
    });
  }

  promoteToSectionManager(userId) {
    this.sectionManagersResource.promote(userId).$promise.then(()=> {
      this.commandHasSucceeded();
      this.fetchUsersInSection();
    }, (error) => {
      this.commandHasFailed(error.data.reason);
    });
  }

  commandHasFailed(message) {
    this.commandFailed = true;
    this.commandFailedMessage = 'Command failed: ' + message;
  }

  commandHasSucceeded() {
    this.commandFailed = false;
  }
}

SectionManagementCtrl.$inject = ['$resource', 'sectionsUrl', '$stateParams', '$state', 'usersListUrl', 'AuthService'];

export default SectionManagementCtrl;