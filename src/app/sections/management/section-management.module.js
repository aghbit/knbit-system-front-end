'use strict';

import SectionManagementCtrl from './controller/section-management.controller.js';
import NotificationsModule from '../../notifications/notifications.module.js';

var SectionManagementModule = angular.module('SectionManagementModule', [
  'smart-table',
  'knbitFrontend.Members.Config',
  NotificationsModule.name
])
  .controller('SectionManagementCtrl', SectionManagementCtrl);

export default SectionManagementModule;
