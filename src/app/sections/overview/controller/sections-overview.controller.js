'use strict';

class SectionsOverviewCtrl {
  constructor($resource, sectionsUrl, AuthService) {
    this.sectionsOverviewResource = $resource(sectionsUrl);
    this.resource = $resource;
    this.authService = AuthService;
    this.sectionsUrl = sectionsUrl;
    this.fetchSections();
  }

  fetchSections() {
    var paramJson = {
      userId: this.authService.getUserId()
    };
    this.sectionsOverviewResource.query(paramJson).$promise.then((sectionsList) => {
      this.sectionsList = sectionsList;
      this.noSectionsYet = (sectionsList.length === 0);
    }, () => {
      // TODO display some error alert
    });
  }

  sendRequestToJoin(section) {
    var requestToJoinSectionUrl = this.sectionsUrl + '/' + section.id + '/candidates';
    var requestJson = {
      'userId': this.authService.getUserId()
    };
    this.resource(requestToJoinSectionUrl).save(requestJson).$promise.then(()=> {
      this.fetchSections();
    }, () => {
    });
  }

}

SectionsOverviewCtrl.$inject = ['$resource', 'sectionsUrl', 'AuthService'];

export default SectionsOverviewCtrl;
