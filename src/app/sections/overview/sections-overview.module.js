'use strict';

import SectionsOverviewCtrl from './controller/sections-overview.controller.js';

var SectionsOverviewModule = angular.module('SectionsOverviewModule', [
  'knbitFrontend.Sections.Config'
])
  .controller('SectionsOverviewCtrl', SectionsOverviewCtrl);

export default SectionsOverviewModule;
