'use strict';

import Routes from './sections.routes.js';
import AuthModule from '../auth/auth.module.js';
import SectionsOverviewModule from './overview/sections-overview.module.js';
import CreateSectionModule from './create/create-section.module.js';
import SectionDetailsModule from './details/section-details.module.js';
import SectionManagementModule from './management/section-management.module.js';

var knbitFrontendSectionsModule = angular.module('knbitFrontend.Sections', [
  'ui.bootstrap',
  AuthModule.name,
  SectionsOverviewModule.name,
  CreateSectionModule.name,
  SectionDetailsModule.name,
  SectionManagementModule.name
]).
config(Routes);

export default knbitFrontendSectionsModule;
