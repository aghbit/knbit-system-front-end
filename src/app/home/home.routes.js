/**
 * Created by novy on 22.05.15.
 */

'use strict';

var Routes = function ($stateProvider) {

  $stateProvider
    .state('home.index', {
      url: '',
      controller: 'HomeIndexCtrl',
      controllerAs: 'home',
      templateUrl: 'app/home/templates/index.html'
    })
    .state('home.about', {
      url: '/about',
      controller: 'HomeAboutCtrl',
      controllerAs: 'about',
      templateUrl: 'app/home/templates/about.html'
    });

};

Routes.$inject = ['$stateProvider'];


export default Routes;
