'use strict';

class IndexCtrl {
  constructor() {
    this.counter = 0;
  }

  click() {
    this.counter++;
  }
}

export default IndexCtrl;

