'use strict';

import Routes from './home.routes.js';

import IndexCtrl from './controllers/index.controller.js';
import AboutCtrl from './controllers/about.controller.js';


var knbitFrontendHomeModule = angular.module('knbitFrontend.Home', [
  'ui.router',
  'knbitFrontend.Home.Config'
])
  .config(Routes)
  .controller('HomeIndexCtrl', IndexCtrl)
  .controller('HomeAboutCtrl', AboutCtrl);


export default knbitFrontendHomeModule;
