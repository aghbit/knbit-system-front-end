'use strict';

import Routes from './routes.js';
import HttpInterceptor from './http-interceptor.js';

import knbitFrontendComponentsModule from './components/components.module.js';
import knbitFrontendHomeModule from './home/home.module.js';
import knbitFrontendProjectsModule from './projects/projects.module.js';
import knbitFrontendEventsModule from './events/events.module.js';
import knbitFrontendTimelineModule from './timeline/timeline.module.js';
import knbitFrontendMembersModule from './members/members.module.js';
import knbitFrontendSectionsModule from './sections/sections.module.js';

angular.module('knbitFrontend', [
  'ui.router',
  knbitFrontendComponentsModule.name,
  knbitFrontendHomeModule.name,
  knbitFrontendProjectsModule.name,
  knbitFrontendEventsModule.name,
  knbitFrontendTimelineModule.name,
  knbitFrontendMembersModule.name,
  knbitFrontendSectionsModule.name
])
  .factory('httpInterceptor', HttpInterceptor)
  .config(['$httpProvider', ($httpProvider) => {
    $httpProvider.interceptors.push('httpInterceptor');
  }])
  .config(Routes);

// enable bootstrap-material-js
$.material.init();
