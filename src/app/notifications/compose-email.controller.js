'use strict';

class ComposeEmailCtrl {
  constructor($scope, $stateParams, ComposeEmailFormFactory, NotificationService) {
    this.scope = $scope;
    this.userIdsList = $stateParams.userIdsList;
    this.receiversCount = this.userIdsList ? this.userIdsList.length : 0;
    this.notificationService = NotificationService;
    this.fields = ComposeEmailFormFactory.getFields();
    this.form = {};
  }
  cancel() {
    this.scope.$close(false);
  }
  sendEmail(){
    this.notificationService.sendEmail(this.form.subject, this.form.message, this.userIdsList).$promise.then(() => {
    }, (error) => {
      console.log(error);
    });
    this.scope.$close(true);
  }
}

ComposeEmailCtrl.$inject = ['$scope', '$stateParams', 'ComposeEmailFormFactory', 'NotificationService'];
export default ComposeEmailCtrl;