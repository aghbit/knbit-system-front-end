'use strict';

import NotificationService from './notifications.service.js';
import ComposeEmailCtrl from './compose-email.controller.js';
import ComposeEmailFormFactory from './compose-email-form.factory.js';

var NotificationsModule = angular.module('NotificationsModule', [
  'knbitFrontend.Notifications.Config'
])
.service('NotificationService', NotificationService)
.factory('ComposeEmailFormFactory', ComposeEmailFormFactory)
.controller('ComposeEmailCtrl', ComposeEmailCtrl);

export default NotificationsModule;