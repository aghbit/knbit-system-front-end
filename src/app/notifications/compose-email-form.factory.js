'use strict';

var ComposeEmailFormFactory = function () {
  return {
    getFields: function () {
      return [
        {
          key: 'subject',
          type: 'input',
          templateOptions: {
            type: 'text',
            label: 'Subject',
            placeholder: 'Enter email subject',
            required: true
          }
        },
        {
          key: 'message',
          type: 'textarea',
          templateOptions: {
            rows: 20,
            label: 'Message',
            placeholder: 'Enter plain-text / HTML message',
            required: true
          }
        }
      ];
    }
  };
};

export default ComposeEmailFormFactory;
