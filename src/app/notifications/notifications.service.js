'use strict';

class NotificationService {
  constructor($resource, $q, AuthService, sendEmailUrl) {
    this.promise = $q;
    this.sendEmailResource = $resource(sendEmailUrl, {}, {
      send: {
        method: 'POST',
        headers: {
          'knbit-aa-auth': AuthService.getToken()
        }
      }
    });
  }

  /**
   * Sends email to list of users with notifications-bc.
   * @param subject
   * @param message, can be either plaintext or HTML.
   * @param userIdsList, array of ids, i.e. [{"userId": id1}, {"userId": id2}].
   */
  sendEmail(subject, message, userIdsList) {
    var mailRequestJson = {
      "subject": subject,
      "message": message,
      "receivers": userIdsList
    };
    return this.sendEmailResource.send(mailRequestJson);
  }
}

NotificationService.$inject = ['$resource', '$q', 'AuthService', 'sendEmailUrl'];
export default NotificationService