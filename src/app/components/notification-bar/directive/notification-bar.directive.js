'use strict';

var ShowNotifications = function () {
  return {
    restrict: 'A',
    link: function (scope, elem) {

      $(elem).click(function () {
        $('#notification-container').fadeToggle(300);
        return false;
      });

      $(document).click(function () {
        $('#notification-container').hide();
      });

      // do not close notification window on click
      $('#notification-container').click(function () {
        return false;
      });

    }
  };
};

export default ShowNotifications;
