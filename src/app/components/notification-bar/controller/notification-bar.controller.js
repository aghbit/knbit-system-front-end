'use strict';

var NotificationBarCtrl = function ($scope, $rootScope, NotificationBarService) {
  $scope.unreadMessagesCount = 0;
  $scope.messages = [];
  var internalMessageMap = new Map();

  function toJSObject(candidate) {
    return JSON.parse(candidate);
  }

  this.synchronizeCurrentView = function (messageMap) {
    $scope.messages = [];
    $scope.unreadMessagesCount = 0;
    messageMap.forEach(message => {
      $scope.messages.unshift(message);
      if (message.read === false) {
        $scope.unreadMessagesCount += 1;
      }
    });
  };

  this.pushMessagesToFrontOfMap = function (messages, messageMap) {
    var refreshedInternalMessageMap = new Map();
    messages.forEach(message => {
      message.payload = toJSObject(message.payload);
      refreshedInternalMessageMap.set(message.id, message);
    });
    messageMap.forEach(message => {
      refreshedInternalMessageMap.set(message.id, message);
    });
    return refreshedInternalMessageMap;
  };

  NotificationBarService.receiveCurrentMessages().then(null, null, message => {
    message.payload = toJSObject(message.payload);
    internalMessageMap.set(message.id, message);
    this.synchronizeCurrentView(internalMessageMap);
  });

  NotificationBarService.receiveBatchMessages().then(null, null, messages => {
    messages.reverse();
    internalMessageMap = this.pushMessagesToFrontOfMap(messages, internalMessageMap);
    this.synchronizeCurrentView(internalMessageMap);
  });

  $scope.requestMoreMessages = function () {
    NotificationBarService.requestBatchMessages(
      internalMessageMap.size / 10, 10
    );
  };

  $scope.ensureIsMarkedAsRead = function (id) {
    var message = internalMessageMap.get(id);
    if (message.read === false) {
      NotificationBarService.markMessageAsRead(id);
    }
  };

  $scope.showEventProposalPopup = function (_proposalId) {
    $rootScope.$emit('notification-bar:event-proposal-selected', _proposalId);
  };

  $scope.showAnswerQuestionPopup = function (payload) {
    $rootScope.$emit('notification-bar:answer-question-selected', payload);
  };

};

NotificationBarCtrl.$inject = ['$scope', '$rootScope', 'NotificationBarService'];

export default NotificationBarCtrl;
