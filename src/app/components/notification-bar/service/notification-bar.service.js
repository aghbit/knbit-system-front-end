/* global SockJS, Stomp */
'use strict';

var NotificationService = function ($q, $timeout, $cookieStore, notificationBarConfig) {

  var service = {},
    currentMessagesListener = $q.defer(),
    batchMessagesListener = $q.defer(),
    socket = {
      client: null,
      stomp: null
    };

  service.RECONNECT_TIMEOUT = notificationBarConfig.reconnectTimeout;
  service.SOCKET_URL = notificationBarConfig.socketUrl;
  service.CURRENT_MESSAGE_TOPIC = notificationBarConfig.currentMessageTopic;
  service.INITIAL_MESSAGE_TOPIC = notificationBarConfig.initialMessageTopic;
  service.BATCH_MESSAGE_TOPIC = notificationBarConfig.batchMessageTopic;

  service.INTIAL_MESSAGE_BROKER = notificationBarConfig.initialMessageBroker;
  service.BATCH_MESSAGE_BROKER = notificationBarConfig.batchMessageBroker;
  service.MESSAGE_STATE_BROKER = notificationBarConfig.messageStateBroker;

  service.receiveCurrentMessages = function () {
    return currentMessagesListener.promise;
  };

  service.receiveBatchMessages = function () {
    return batchMessagesListener.promise;
  };

  service.requestBatchMessages = function (_page, _size) {
    socket.stomp.send(service.BATCH_MESSAGE_BROKER, {}, JSON.stringify({page: _page, size: _size}));
  };

  service.markMessageAsRead = function (_id) {
    socket.stomp.send(service.MESSAGE_STATE_BROKER, {}, JSON.stringify({id: _id}));
  };

  var requestInitialMessages = function () {
    socket.stomp.send(service.INTIAL_MESSAGE_BROKER, {}, {});
  };

  var startListener = function () {
    socket.stomp.subscribe(service.CURRENT_MESSAGE_TOPIC, data => {
      currentMessagesListener.notify(JSON.parse(data.body));
    });
    socket.stomp.subscribe(service.INITIAL_MESSAGE_TOPIC, data => {
      var messages = JSON.parse(data.body);
      messages.reverse();
      messages.forEach(
          message => currentMessagesListener.notify(message)
      );
    });
    socket.stomp.subscribe(service.BATCH_MESSAGE_TOPIC, data => {
      batchMessagesListener.notify(JSON.parse(data.body));
    });
    requestInitialMessages();
  };

  var initialize, reconnect;

  var socketUrl = () => {
    return service.SOCKET_URL + '?knbit-aa-auth=' + $cookieStore.get('token');
  };

  initialize = function () {
    socket.client = new SockJS(socketUrl());
    socket.stomp = Stomp.over(socket.client);
    socket.stomp.debug = null;
    socket.stomp.connect({}, startListener);
    socket.stomp.onclose = reconnect;
  };

  reconnect = function () {
    $timeout(function () {
      initialize();
    }, this.RECONNECT_TIMEOUT);
  };

  initialize();
  return service;
};

NotificationService.$inject = ['$q', '$timeout', '$cookieStore', 'notificationBarConfig'];

export default NotificationService;
