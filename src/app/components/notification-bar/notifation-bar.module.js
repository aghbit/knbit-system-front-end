/**
 * Created by novy on 22.05.15.
 */

'use strict';

import NotificationBarCtrl from './controller/notification-bar.controller.js';
import NotificationService from './service/notification-bar.service.js';
import ShowNotifications from './directive/notification-bar.directive.js';

var NotificationBarModule = angular.module('knbitFrontend.Components.NotificationBar', [
  'knbitFrontend.Components.Config'
])
  .service('NotificationBarService', NotificationService)
  .controller('NotificationBarCtrl', NotificationBarCtrl)
  .directive('showNotifications', ShowNotifications);


export default NotificationBarModule;

