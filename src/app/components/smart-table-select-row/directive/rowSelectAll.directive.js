'use strict';

var RowSelectAll = function rowSelectAll() {

  return {
    restrict: 'E',
    template: '<input type="checkbox" ng-model="isAllSelected" />',
    scope: {
      all: '='
    },
    link: function (scope) {

      scope.$watch('isAllSelected', function () {
        if (angular.isDefined(scope.all)) {
          scope.all.forEach(function (val) {
            val.isSelected = scope.isAllSelected;
          })
        }
      });

      scope.$watch('all', function (newVal, oldVal) {
        if (oldVal) {
          oldVal.forEach(function (val) {
            val.isSelected = false;
          });
        }

        scope.isAllSelected = false;
      });
    }
  }
};

export default RowSelectAll;