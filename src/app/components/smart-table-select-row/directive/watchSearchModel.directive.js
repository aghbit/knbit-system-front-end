'use strict';

var WatchSearchModel = function () {
  return {
    require: '^stTable',
    scope: {
      stWatchSearchModel: '='
    },
    link: function (scope, ele, attr, ctrl) {
      scope.$watch('stWatchSearchModel', function (val) {
        ctrl.search(val);
      });

    }
  };
};

export default WatchSearchModel
