'use strict';

var RowSelectSingle = function rowSelect() {
  return {
    restrict: 'E',
    template: '<input type="checkbox"/>',
    replace: true,
    transclude: true,
    scope: {
      ngModel: '='
    },
    link: function (scope, element) {
      element.bind('click', function () {
        scope.$apply(function () {
          scope.ngModel = !scope.ngModel
        })
      });
    }
  }
};

export default RowSelectSingle;