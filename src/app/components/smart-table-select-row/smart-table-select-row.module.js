'use strict';

import RowSelectSingle from './directive/rowSelectSingle.directive.js';
import RowSelectAll from './directive/rowSelectAll.directive.js';
import WatchSearchModel from './directive/watchSearchModel.directive.js';

var SmartTableSelectRowModule = angular.module('SmartTableSelectRowModule', [
  'smart-table'
])
.directive('stRowSelectAll', RowSelectAll)
.directive('stRowSelectSingle', RowSelectSingle)
.directive('stWatchSearchModel', WatchSearchModel);

export default SmartTableSelectRowModule
