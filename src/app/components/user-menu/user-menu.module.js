'use strict';

import AuthModule from '../../auth/auth.module.js';
import LoginStateCtrl from './controller/login-state.controller.js';

var UserMenuModule = angular.module('knbitFrontend.Components.UserMenu', [
  AuthModule.name
]).controller('LoginStateCtrl', LoginStateCtrl);

export default UserMenuModule;
