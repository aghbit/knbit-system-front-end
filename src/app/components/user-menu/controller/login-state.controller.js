'use strict';

var LoginStateController = function ($scope, AuthService) {
  var ADMIN_PERMISSION = 'ADMIN';

  AuthService.isLoggedIn().then(function () {
    $scope.loggedIn = true;
  }, function () {
    $scope.loggedIn = false;
  });

  AuthService.onLogIn($scope, function () {
    $scope.loggedIn = true;
    AuthService.hasPermission(ADMIN_PERMISSION).then(function () {
      $scope.hasAdminPermission = true;
    }, function () {
      $scope.hasAdminPermission = false;
    });
  });

  $scope.logOut = function () {
    AuthService.logOut();
    $scope.loggedIn = false;
    $scope.hasAdminPermission = false;
  };

};

LoginStateController.$inject = ['$scope', 'AuthService'];

export default LoginStateController;
