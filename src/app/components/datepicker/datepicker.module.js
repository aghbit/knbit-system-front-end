'use strict';

import DatePicker from './directive/datepicker.directive.js';

var DatePickerModule = angular.module('knbitFrontend.Components.DatePicker', [])
  .directive('datePicker', DatePicker);

export default DatePickerModule;
