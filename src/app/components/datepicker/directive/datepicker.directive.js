'use strict';

var DatePicker = function ($window) {

  return {
    scope: {
      onChange: '&',
      date: '=',
      minDate: '@',
      maxDate: '@',
      format: '@'
    },
    link: function (scope, element, attrs) {

      var options = {
        format: scope.format || 'YYYY-MM-DD hh:mm a',
        time: attrs.time || true
      };

      if (scope.minDate) {
        options.minDate = (scope.minDate === 'today') ? $window.moment() : scope.minDate;
      }

      if (scope.date) {
        options.currentDate = scope.date;
      }

      $(element).bootstrapMaterialDatePicker(options);

      element.on('change', function (event, date) {
        scope.$apply(function () {
          scope.date = date ? new Date(date) : date;
          scope.onChange({
            date: date
          });
        });
      });

      scope.$watch('minDate', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $(element).bootstrapMaterialDatePicker('setMinDate', newVal);
        }
      });

      scope.$watch('maxDate', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $(element).bootstrapMaterialDatePicker('setMaxDate', newVal);
        }
      });

      scope.$watch('date', function (newVal, oldVal) {
        if (newVal !== oldVal) {
          $(element).bootstrapMaterialDatePicker('setDate', newVal);
        }
      });
    }
  };

};

DatePicker.$inject = ['$window'];
export default DatePicker;
