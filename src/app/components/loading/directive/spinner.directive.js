'use strict';

let spinner = function () {
  return {
    restrict: 'E',
    templateUrl: 'app/components/loading/directive/spinner.tpl.html'
  };
};

spinner.$inject = ['$http'];

export default spinner;



