'use strict';

let ajax = function ($http) {
  return {
    restrict: 'A',
    scope: {
      'promise': '@'
    },
    link: function (scope, elem, attrs)
    {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, function (newVal)
      {
        if(newVal){
          elem.show();
        }else{
          elem.hide();
        }
      });
    }
  };
};

ajax.$inject = ['$http'];

export default ajax;



