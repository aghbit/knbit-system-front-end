'use strict';

import ajax from './directive/ajax.directive.js';
import spinner from './directive/spinner.directive.js';

var LoadingModule = angular.module('knbitFrontend.Components.Loading', [])
  .directive('loadingAjax', ajax)
  .directive('loadingSpinner', spinner);

export default LoadingModule;
