'use strict';

import NavbarModule from './navbar/navbar.module.js';
import NotificationBarModule from './notification-bar/notifation-bar.module.js';
import DatePickerModule from './datepicker/datepicker.module.js';
import LoadingModule from './loading/loading.module.js';
import SmartTableSelectRowModule from './smart-table-select-row/smart-table-select-row.module.js';

var knbitFrontendComponentsModule = angular.module('knbitFrontend.Components', [
  NavbarModule.name,
  NotificationBarModule.name,
  DatePickerModule.name,
  LoadingModule.name,
  SmartTableSelectRowModule.name
]);


export default knbitFrontendComponentsModule;
