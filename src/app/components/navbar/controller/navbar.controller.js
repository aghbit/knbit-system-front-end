'use strict';

var NavbarCtrl = function ($scope, $rootScope, AuthEvents, AuthService) {
  $scope.collapsed = true;
  $scope.toggle = toggle;

  listenForAuthEvents();
  checkUserPermission();

  function toggle() {
    $scope.collapsed = !$scope.collapsed;
  }

  function checkUserPermission() {
    AuthService
      .hasPermission('EVENTS_MANAGEMENT')
      .then(
      () => $scope.isEventMaster = true,
      () => $scope.isEventMaster = false
    );
  }

  function listenForAuthEvents() {
    let loggedInDestroyer = $rootScope.$on(AuthEvents.LOGGED_IN, () => checkUserPermission());
    let loggedOutDestroyer = $rootScope.$on(AuthEvents.LOGGED_OUT, () => checkUserPermission());

    $scope.$on('$destroy', () => {
      loggedInDestroyer();
      loggedOutDestroyer();
    });
  }

};

NavbarCtrl.$inject = ['$scope', '$rootScope', 'AuthEvents', 'AuthService'];

export default NavbarCtrl;
