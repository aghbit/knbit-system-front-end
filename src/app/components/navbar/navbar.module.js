/**
 * Created by novy on 22.05.15.
 */

'use strict';

import UserMenuModule from '../user-menu/user-menu.module.js';
import NavbarCtrl from './controller/navbar.controller.js';

var NavbarModule = angular.module('knbitFrontend.Components.Navbar', [
  UserMenuModule.name
])
  .controller('NavbarCtrl', NavbarCtrl);

export default NavbarModule;

