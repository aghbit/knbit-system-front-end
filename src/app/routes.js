/**
 * Created by novy on 22.05.15.
 */

'use strict';

var Routes = function ($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('home', {
      url: '/home',
      abstract: true,
      templateUrl: 'app/home/templates/layout.html'
    })
    .state('projects', {
      url: '/projects',
      abstract: true,
      templateUrl: 'app/projects/templates/layout.html'
    })
    .state('events', {
      url: '/events',
      abstract: true,
      templateUrl: 'app/events/common/partials/layout.html'
    })
    .state('timeline', {
      url: '/timeline',
      abstract: true,
      templateUrl: 'app/timeline/timeline.tpl.html'
    })
    .state('members', {
      url: '/members',
      abstract: true,
      templateUrl: 'app/members/common/partials/layout.html'
    })
    .state('sections', {
      url: '/sections',
      abstract: true,
      templateUrl: 'app/sections/common/partials/layout.html'
    });

  $urlRouterProvider.otherwise(function ($injector) {
    var $state = $injector.get('$state');
    $state.go('sections.overview');
  });

};

Routes.$inject = ['$stateProvider', '$urlRouterProvider'];

export default Routes;
