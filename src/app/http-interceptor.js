'use strict';

/**
 * Remarks:
 *  1. Cannot use AuthService, circular dependency with AuthResource.
 *  2. Auth token added only to events-bc API. Reason: other teams
 *     have to allow knbit-aa-auth header in CORS filter.
 *  3. Handling of corner cases needed, e.g. redirect to special page
 *     on unauthorized request.
 */

var HttpInterceptor = ($cookieStore) => {
  var HEADER = 'knbit-aa-auth';
  var eventsBcPattern = new RegExp('.+/eventsbc/api/.+');
  var timelineBcPattern = new RegExp('.+/api/v3/.+'); // TODO: /timelinebc/api when enabled on backend

  return {
    request: (config) => {
      let token = $cookieStore.get('token');

      if (angular.isDefined(token)) {
        if (eventsBcPattern.test(config.url) || timelineBcPattern.test(config.url)) {
          config.headers[HEADER] = token;
        }
      }

      return config;
    },
    response: (response) => {
      let token = response.config.headers[HEADER];

      //  TODO: when header is empty "undefined" is saved which is not a valid JSON value
      //  TODO: this makes $cookieStore.get throw error - use $cookies across application instead
      //  TODO: http://stackoverflow.com/questions/18679094/angularjs-unexpected-token-in-fromjson
      if (angular.isDefined(token)) {
        if (eventsBcPattern.test(response.config.url)) {
          $cookieStore.put('token', token);
        }
      }

      return response;
    }
  };
};

HttpInterceptor.$inject = ['$cookieStore'];

export default HttpInterceptor;
