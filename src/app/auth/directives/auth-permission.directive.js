'use strict';

function authPermissionFactory(authPermissionRegisteredName) {

  const authPermission = function(ngIfDirective, AuthService, AuthEvents) {
    const ngIf = ngIfDirective[0];

    return {
      transclude: ngIf.transclude,
      priority: ngIf.priority,
      terminal: ngIf.terminal,
      restrict: ngIf.restrict,
      $$tlb: ngIf.$$tlb,
      link: linkFunction
    };

    function linkFunction($scope, $element, $attr) {
      const requiredPermission = $attr[authPermissionRegisteredName];

      $scope.$on(AuthEvents.LOGGED_IN, () => checkUserPermission(requiredPermission));
      $scope.$on(AuthEvents.LOGGED_OUT, () => checkUserPermission(requiredPermission));
      checkUserPermission(requiredPermission);

      $attr.ngIf = () => $scope.hasPermission;
      ngIf.link.apply(ngIf, arguments);

      function checkUserPermission(permission) {
        AuthService
          .hasPermission(permission)
          .then(hasPermission => $scope.hasPermission = hasPermission);
      }
    }
  };

  authPermission.$inject = ['ngIfDirective', 'AuthService', 'AuthEvents'];

  return authPermission;
}

export default authPermissionFactory;
