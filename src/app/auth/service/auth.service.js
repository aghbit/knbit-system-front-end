'use strict';

class AuthService {

  constructor($rootScope, $q, $cookieStore, AuthResource, AuthEvents) {
    this.rootScope = $rootScope;
    this.promise = $q;
    this.cookieStore = $cookieStore;
    this.authResource = AuthResource;
    this.authEvents = AuthEvents;
  }

  getToken() {
    return this.cookieStore.get('token');
  }

  getUserId() {
    return this.cookieStore.get('userId');
  }

  isLoggedIn() {
    return this.promise((resolve, reject) => {
      var token = this.getToken();
      if (angular.isDefined(token)) {
        this.authResource.authenticate(token).$promise.then((response) => {
          this.cookieStore.put('userId', response.userId);
          resolve();
        }, () => {
          this.cookieStore.remove('token');
          reject();
        });
      }
      else {
        reject();
      }
    });
  }

  hasPermission(permission) {
    return this.promise((resolve, reject) => {
      var token = this.getToken();
      if (angular.isDefined(token)) {
        this.authResource.authorize(token, permission).$promise.then((response) => {
          this.cookieStore.put('userId', response.userId);
          resolve(true);
        }, () => {
          reject();
        });
      }
      else {
        reject();
      }
    });
  }

  onLogIn($scope, handler) {
    $scope.$on(this.authEvents.LOGGED_IN, function () {
      handler();
    });
  }

  logIn(credentials) {
    return this.promise((resolve, reject) => {
      this.authResource.logIn(credentials).$promise.then(response => {
        this.cookieStore.put('token', response.token);
        this.cookieStore.put('userId', response.userId);
        this.rootScope.$broadcast(this.authEvents.LOGGED_IN);
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }

  logOut() {
    this.authResource.logOut(this.getToken());
    this.cookieStore.remove('token');
    this.cookieStore.remove('userId');
    this.rootScope.$broadcast(this.authEvents.LOGGED_OUT);
  }
}

AuthService.$inject = ['$rootScope', '$q', '$cookieStore', 'AuthResource', 'AuthEvents'];

export default AuthService;
