'use strict';

const LOGIN_STATE = 'members.login';

let authEventsHandlers = ($rootScope, $state, AuthEvents) => {
  $rootScope.$on(AuthEvents.LOGIN_REQUIRED, (event, data) => {
    $state.go(LOGIN_STATE, {next: data.next});
  });

  $rootScope.$on(AuthEvents.LOGGED_OUT, () => {
    $state.reload();
  });

  $rootScope.$on(AuthEvents.NOT_AUTHORIZED, () => {
    // TODO: transition to 'not authorized' state
    console.log('not authorized');
  });
};

authEventsHandlers.$inject = ['$rootScope', '$state', 'AuthEvents'];

export default authEventsHandlers;
