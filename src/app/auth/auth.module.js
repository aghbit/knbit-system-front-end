'use strict';

import AuthResource from './resource/auth.resource.js';
import AuthService from './service/auth.service.js';
import authPermissionDirectiveFactory from './directives/auth-permission.directive.js';

import authEventsHandlers from './auth-events-handlers.js';
import authStateInterceptor from './auth-state-interceptor.js';

var AuthModule = angular.module('AuthModule', [
  'ngResource',
  'ngCookies',
  'knbitFrontend.Auth.Config'
])
  .service('AuthResource', AuthResource)
  .service('AuthService', AuthService)
  .directive('hasPermission', authPermissionDirectiveFactory('hasPermission'))
  .run(authEventsHandlers)
  .run(authStateInterceptor);

export default AuthModule;
