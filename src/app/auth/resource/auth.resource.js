'use strict';

class AuthResource {
  constructor($resource, AuthUrls) {
    this.authUrls = AuthUrls;
    this.resource = $resource;
  }

  logIn(credentials) {
    return this.resource(this.authUrls.login).save(credentials);
  }

  logOut(token) {
    this.resource(this.authUrls.logout, {}, {
      logout: {
        method: 'POST',
        headers: {'knbit-aa-auth': token}
      }
    }).logout();
  }

  authenticate(token) {
    var tokenJson = {
      token: token
    };
    return this.resource(this.authUrls.authenticate).save(tokenJson);
  }

  authorize(token, permission) {
    var permissionJson = {
      permission: permission
    };
    return this.resource(this.authUrls.authorize, {}, {
      authorize: {
        method: 'POST',
        headers: {'knbit-aa-auth': token}
      }
    }).authorize(permissionJson);
  }

}

AuthResource.$inject = ['$resource', 'AuthUrls'];

export default AuthResource;
