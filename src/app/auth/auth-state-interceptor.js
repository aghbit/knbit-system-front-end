'use strict';

let authStateInterceptor = ($rootScope, $state, $q, AuthService, AuthEvents) => {

  $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {

    function resume() {
      // transition to next state without broadcasting stateChangeStart event
      $state.go(toState.name, toParams, {notify: false}).then(function () {
        $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
      });
    }

    function authenticate() {
      function notAuthorized() {
        $rootScope.$broadcast(AuthEvents.NOT_AUTHORIZED);
        return $q.reject('not authorized');
      }

      function loginRequired() {
        $rootScope.$broadcast(AuthEvents.LOGIN_REQUIRED, {next: toState.name});
        return $q.reject('not authenticated');
      }

      return AuthService.isLoggedIn()
        .then(notAuthorized)
        .catch(loginRequired);
    }

    var requireSignedIn = toState.data && toState.data.requireSignedIn;
    if (angular.isDefined(requireSignedIn)) {
      AuthService.isLoggedIn()
        .then(resume)
        .catch(authenticate);
    }

    var permission = toState.data && toState.data.permission;
    if (!angular.isDefined(permission)) {
      return;
    }

    event.preventDefault();

    AuthService.hasPermission(permission)
      .then(resume)
      .catch(authenticate);

  });
};

authStateInterceptor.$inject = ['$rootScope', '$state', '$q', 'AuthService', 'AuthEvents'];

export default authStateInterceptor;
