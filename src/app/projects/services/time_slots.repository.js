'use strict';

class TimeSlotsRepository {

  constructor($http, projectsConfig) {
    this.$http = $http;
    this.projectsConfig = projectsConfig;
  }

  getAll() {
    return this.$http.get(this.projectsConfig.apiUrl + '/time_slots');
  }

  delete(time_slot) {
    return this.$http.delete(this.projectsConfig.apiUrl + '/time_slots/' + time_slot.id);
  }

  get(id) {
    return this.$http.get(this.projectsConfig.apiUrl + '/projects/' + id);
  }

  create(time_slot) {
    return this.$http.post(this.projectsConfig.apiUrl + '/time_slots', {time_slot: time_slot});
  }

  createDay(time_slot) {
    return this.$http.post(this.projectsConfig.apiUrl + '/time_slots/create_day', {time_slots: time_slot});
  }

  update(project) {
    return this.$http.patch(this.projectsConfig.apiUrl + '/projects/' + project.id, project);
  }

}

TimeSlotsRepository.$inject = ['$http', 'projectsConfig'];

export default TimeSlotsRepository;
