'use strict';

class Notice {

  constructor($rootScope, $timeout) {
    this.$rootScope = $rootScope;
    this.$timeout = $timeout;
  }

  notice(msg) {
    this.notify(msg, 'success');
  }

  error(msg) {
    this.notify(msg, 'danger');
  }

  clear() {
    this.$rootScope.notice = null;
  }

  notify(msg, type) {
    this.$rootScope.notice = {
      message: msg,
      type: type,
    }

    $("body").animate({ scrollTop: 0 }, 400);
  }

}

Notice.$inject = ['$rootScope', '$timeout'];

export default Notice;
