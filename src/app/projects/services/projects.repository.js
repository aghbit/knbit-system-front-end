'use strict';

class ProjectsRepository {

  constructor($http, projectsConfig) {
    this.$http = $http;
    this.projectsConfig = projectsConfig;
  }

  getAll() {
    return this.$http.get(this.projectsConfig.apiUrl + '/projects');
  }

  get(id) {
    return this.$http.get(this.projectsConfig.apiUrl + '/projects/' + id);
  }

  create(project) {
    return this.$http.post(this.projectsConfig.apiUrl + '/projects', {project: project});
  }

  update(project) {
    return this.$http.patch(this.projectsConfig.apiUrl + '/projects/' + project.id, project);
  }

}

ProjectsRepository.$inject = ['$http', 'projectsConfig'];

export default ProjectsRepository;
