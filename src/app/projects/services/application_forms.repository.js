'use strict';

class ApplicationFormsRepository {

  constructor($http, $q, projectsConfig) {
    this.$http = $http;
    this.$q = $q;
    this.projectsConfig = projectsConfig;
  }

  getAll() {
    return this.$http.get(this.projectsConfig.apiUrl + '/application_forms');
  }

  get(id) {
    return this.$http.get(this.projectsConfig.apiUrl + '/application_forms/' + id);
  }

  new() {
    return this.$http.get(this.projectsConfig.apiUrl + '/application_forms/new');
  }

  current() {
    return this.$http.get(this.projectsConfig.apiUrl + '/application_forms/current');
  }

  create(application_form) {
    return this.$http.post(this.projectsConfig.apiUrl + '/application_forms', {application_form: application_form});
  }

  update(application_form) {
    return this.$http.patch(this.projectsConfig.apiUrl + '/application_forms/' + application_form.id, {application_form: application_form});
  }

  setMeeting(application_form) {
    return this.$http.patch(this.projectsConfig.apiUrl + '/application_forms/' + application_form.id + '/set_meeting', { time_slot_id: application_form.meeting_time_id });
  }

}

ApplicationFormsRepository.$inject = ['$http', '$q', 'projectsConfig'];

export default ApplicationFormsRepository;
