'use strict';

let AuthRequestInterceptor = function (projectsConfig, $cookieStore) {
  return {
    request: function (config) {
      if (config.url.indexOf(projectsConfig.apiUrl) === 0) {
        let header = $cookieStore.get('token');

        if (header) {
          config.headers['X-Authentication'] = header;
        }
      }

      return config;
    }
  }
};

AuthRequestInterceptor.$inject = ['projectsConfig', '$cookieStore'];

export default AuthRequestInterceptor;
