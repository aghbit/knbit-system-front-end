'use strict';

import Routes from './projects.routes.js';

import ProjectsRepository from './services/projects.repository.js';
import ApplicationFormsRepository from './services/application_forms.repository.js';
import TimeSlotsRepository from './services/time_slots.repository.js';
import Notice from './services/notice.js';

import ProjectsIndexCtrl from './controllers/projects/index.controller.js';
import ProjectsShowCtrl from './controllers/projects/show.controller.js';
import ProjectsNewCtrl from './controllers/projects/new.controller.js';
import ProjectsEditCtrl from './controllers/projects/edit.controller.js';

import EnrollmentsIndexCtrl from './controllers/enrollments/index.controller.js';
import EnrollmentsTimeSlotsCtrl from './controllers/enrollments/time_slots.controller.js';
import EnrollmentsEditCtrl from './controllers/enrollments/edit.controller.js';

import ApplicationFormsCurrentCtrl from './controllers/application_forms/current.controller.js';
import ApplicationFormsNewCtrl from './controllers/application_forms/new.controller.js';
import ApplicationFormsSetupMeetingCtrl from './controllers/application_forms/setup_meeting.controller.js';

import AuthRequestInterceptor from './services/auth_request_interceptor.js';

var knbitFrontendProjectsModule = angular.module('knbitFrontend.Projects', [
  'ui.router',
  'knbitFrontend.Projects.Config'
])
  .config(Routes)

  .service('ProjectsRepository', ProjectsRepository)
  .service('ApplicationFormsRepository', ApplicationFormsRepository)
  .service('TimeSlotsRepository', TimeSlotsRepository)
  .service('Notice', Notice)
  .service('AuthRequestInterceptor', AuthRequestInterceptor)

  .controller('ProjectsIndexCtrl', ProjectsIndexCtrl)
  .controller('ProjectsShowCtrl', ProjectsShowCtrl)
  .controller('ProjectsNewCtrl', ProjectsNewCtrl)
  .controller('ProjectsEditCtrl', ProjectsEditCtrl)

  .controller('EnrollmentsIndexCtrl', EnrollmentsIndexCtrl)
  .controller('EnrollmentsTimeSlotsCtrl', EnrollmentsTimeSlotsCtrl)
  .controller('EnrollmentsEditCtrl', EnrollmentsEditCtrl)

  .controller('ApplicationFormsCurrentCtrl', ApplicationFormsCurrentCtrl)
  .controller('ApplicationFormsNewCtrl', ApplicationFormsNewCtrl)
  .controller('ApplicationFormsSetupMeetingCtrl', ApplicationFormsSetupMeetingCtrl)

  .config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('AuthRequestInterceptor');
  }]);

export default knbitFrontendProjectsModule;
