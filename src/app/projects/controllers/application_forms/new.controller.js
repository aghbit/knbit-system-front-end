'use strict';

class NewCtrl {
  constructor(ProjectsRepository, Notice, ApplicationFormsRepository, $stateParams, $state) {
    this.ProjectsRepository = ProjectsRepository;
    this.ApplicationFormsRepository = ApplicationFormsRepository;
    this.Notice = Notice;
    this.$state = $state;

    ApplicationFormsRepository.new().then(response =>
            this.application_form = response.data
    );

    ProjectsRepository.getAll().then(response =>
            this.projects = response.data.projects
    );
  }

  selectedProjects() {
    return _.filter(this.projects, { selected: true })
  }

  save() {
    this.application_form.enrollments_attributes = _.map(this.selectedProjects(), (p) => ({ project_id: p.id, team_leader: p.team_leader }));

    this.ApplicationFormsRepository.create(this.application_form).then(response => {
      this.$state.go('projects.application_forms.setup_meeting');
    }, response => {
      this.Notice.error('Error: ' + JSON.stringify(response.data.errors));
    })
  }
}

NewCtrl.$inject = ['ProjectsRepository', 'Notice', 'ApplicationFormsRepository', '$stateParams', '$state'];

export default NewCtrl;