'use strict';

class ApplicationFormsCurrentCtrl {
  constructor(ApplicationFormsRepository, $state) {
    ApplicationFormsRepository.current().then(function (response) {
      $state.go('projects.application_forms.setup_meeting')
    }, function (response) {
      $state.go('projects.application_forms.new')
    });
  }
}

ApplicationFormsCurrentCtrl.$inject = ['ApplicationFormsRepository', '$state'];

export default ApplicationFormsCurrentCtrl;
