'use strict';

const CAN_SELECT_TIME_SLOTS = true;

class SetupCtrl {
  constructor(Notice, ApplicationFormsRepository, ProjectsRepository, TimeSlotsRepository, $stateParams, $state) {
    this.ApplicationFormsRepository = ApplicationFormsRepository;
    this.Notice = Notice;
    this.canSelectTimeSlots = CAN_SELECT_TIME_SLOTS;

    ApplicationFormsRepository.current().then(response => {
      this.application_form = response.data;

      if(this.application_form.meeting_time) {
        this.application_form.time_slot_id = this.application_form.meeting_time.id;
      }

      ProjectsRepository.getAll().then(response => {
        this.projects = response.data.projects;

        _.each(this.projects, (project) => {
          let enrollment = _.find(this.application_form.enrollments, (e) => {
            return e.project.id == project.id;
          })

          project.selected = !!enrollment;

          if(enrollment) {
            project.enrollment_id = enrollment.id;
            project.team_leader = enrollment.team_leader;
          }
        });
      });

      this.setNotifications();
    }, function (response) {
      $state.go('projects.application_forms.new')
    });

    TimeSlotsRepository.getAll().then(response => {
      this.all_time_slots = response.data.time_slots;

      this.time_slots = _.groupBy(this.all_time_slots, time_slot => {
        return moment(time_slot.start_time).startOf('day').format('DD.MM (dddd)');
      });
    })
  }

  selectedProjects() {
    return _.filter(this.projects, { selected: true })
  }

  editMeetingTime() {
    this.editingMeetingTime = true;
  }

  editProjects() {
    this.editingProjects = true;
  }

  setNotifications() {
    if(!CAN_SELECT_TIME_SLOTS) {
      this.Notice.notice("Ok, you are set up! We will notify you as soon as we review your application!");
    } else {
      if(!this.application_form.meeting_time) {
        this.Notice.notice("Please select your preferred meeting time.");
      }
    }
  }

  save() {
    this.application_form.meeting_time_id = this.selectedTimeSlotId;

    if(this.selectedProjects().length > 0) {
      this.application_form.enrollments_attributes = _.map(this.selectedProjects(), (p) => ({ project_id: p.id, team_leader: p.team_leader, id: p.enrollment_id }));
    }

    this.ApplicationFormsRepository.update(this.application_form).then(response => {
      this.application_form = response.data;

      if(this.application_form.meeting_time) {
        this.application_form.time_slot_id = this.application_form.meeting_time.id;
      }

      this.Notice.notice("See you later!");
    });
  }
}

SetupCtrl.$inject = ['Notice', 'ApplicationFormsRepository', 'ProjectsRepository', 'TimeSlotsRepository', '$stateParams', '$state'];

export default SetupCtrl;
