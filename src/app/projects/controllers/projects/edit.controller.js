'use strict';

class EditCtrl {
  constructor(ProjectsRepository, Notice, $stateParams) {
    this.ProjectsRepository = ProjectsRepository;
    this.Notice = Notice;

    ProjectsRepository.get($stateParams.id).then(response =>
        this.project = response.data
    )
  }

  save() {
    this.ProjectsRepository.update(this.project).then(() => this.Notice.notice('Succesfully edited project!'));
  }
}

EditCtrl.$inject = ['ProjectsRepository', 'Notice', '$stateParams'];

export default EditCtrl;
