'use strict';

class ShowCtrl {
  constructor(ProjectsRepository, $stateParams) {
    ProjectsRepository.get($stateParams.id).then(response =>
        this.project = response.data
    )
  }
}

ShowCtrl.$inject = ['ProjectsRepository', '$stateParams'];

export default ShowCtrl;
