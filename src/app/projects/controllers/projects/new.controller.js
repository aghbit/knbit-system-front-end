'use strict';

class NewCtrl {
  constructor(ProjectsRepository, Notice, $stateParams, $state) {
    this.ProjectsRepository = ProjectsRepository;
    this.Notice = Notice;
    this.$state = $state;
    this.project = {};
  }

  save() {
    this.ProjectsRepository.create(this.project).then(response => {
      this.Notice.notice('Succesfully created project!');
      this.$state.go('projects.edit', {id: response.data.id});
    })
  }
}

NewCtrl.$inject = ['ProjectsRepository', 'Notice', '$stateParams', '$state'];

export default NewCtrl;
