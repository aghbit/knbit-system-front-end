'use strict';

class IndexCtrl {
  constructor(ProjectsRepository) {
    this.statuses = [
      {key: 'not_started', label: 'Not started'},
      {key: 'in_progress', label: 'In progress'},
      {key: 'archived', label: 'archived'},
    ];

    this.status = 'not_started';

    ProjectsRepository.getAll().then(response =>
        this.projects = response.data.projects
    )
  }

  setStatus(status) {
    this.status = status;
  }
}

IndexCtrl.$inject = ['ProjectsRepository'];

export default IndexCtrl;
