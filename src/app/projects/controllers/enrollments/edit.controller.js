'use strict';

class EditCtrl {
  constructor(ProjectsRepository, ApplicationFormsRepository, Notice, $stateParams, $state) {
    this.Notice = Notice;
    this.ApplicationFormsRepository = ApplicationFormsRepository;

    ProjectsRepository.getAll().then(response =>
        this.projects = response.data.projects
    )

    ApplicationFormsRepository.get($stateParams.id).then(response =>
        this.form = response.data
    )
  }

  findProject(project_id) {
    return _.findWhere(this.projects, {id: project_id});
  }

  save() {
    let form = {
      id: this.form.id,
      notes: this.form.notes,
      enrollments_attributes: _.map(this.form.enrollments, (e) => ({ id: e.id, project_id: e.project.id, accepted: e.accepted, team_leader: e.team_leader }))
    }

    this.ApplicationFormsRepository.update(form).then(response =>
        this.Notice.notice('Succesfully edited!')
      // this.$state.go('projects.show', { id: response.data.id })
    )
  }
}

EditCtrl.$inject = ['ProjectsRepository', 'ApplicationFormsRepository', 'Notice', '$stateParams', '$state'];

export default EditCtrl;
