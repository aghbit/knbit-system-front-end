'use strict';

class IndexCtrl {
  constructor(ApplicationFormsRepository, ProjectsRepository) {
    this.statuses = ['pending', 'all'];
    this.status = 'pending';

    ApplicationFormsRepository.getAll().then(response =>
        this.forms = response.data.application_forms
    )

    ProjectsRepository.getAll().then(response =>
        this.projects = response.data.projects
    )
  }

  accepted(form) {
    return _.filter(form.enrollments, (e) => e.accepted).length
  }

  setStatus(status) {
    this.status = status;
  }

}

IndexCtrl.$inject = ['ApplicationFormsRepository', 'ProjectsRepository'];

export default IndexCtrl;
