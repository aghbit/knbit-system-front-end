'use strict';

class TimeSlotsCtrl {
  constructor(TimeSlotsRepository) {
    this.TimeSlotsRepository = TimeSlotsRepository;
    this.dateFormat = "dddd, MMMM Do YYYY";

    TimeSlotsRepository.getAll().then(response => {
      this.all_time_slots = response.data.time_slots;
      this.regenerate_time_slots()
    })
  }

  regenerate_time_slots() {
    this.time_slots = _.groupBy(this.all_time_slots, (slot) => moment(slot.start_time).startOf('day').toDate());
    this.flatten_time_slots = _.map(_.pairs(this.time_slots), function (dayTimeSlots) {
      dayTimeSlots[0] = moment(dayTimeSlots[0]).toDate();
      return dayTimeSlots;
    });

    this.flatten_time_slots = _.sortBy(this.flatten_time_slots, (dayTimeSlots) => dayTimeSlots[0]);
  }

  availableStartTimes(day) {
    let timeSlots = _.map(this.time_slots[day], (ts) => moment(ts.start_time).toDate());
    let startTime = moment(day).local().hours(8);
    let availableStartTimes = []

    while (startTime.hours() <= 21) {
      let exists = _.filter(timeSlots, (ts) => moment(ts).diff(moment(startTime), 'minutes') == 0).length;

      if (exists == 0) {
        availableStartTimes.push(moment(startTime).toDate());
      }

      startTime.add(15, 'minutes');
    }

    return availableStartTimes;
  }

  addTimeSlot(day) {
    let availableStartTimes = this.availableStartTimes(day);
    this.all_time_slots.push({
      forEdit: true,
      start_time: availableStartTimes[0],
      available_start_times: availableStartTimes
    });
    this.regenerate_time_slots()
  }

  saveTimeSlot(timeSlot) {
    timeSlot.end_time = moment(timeSlot.start_time).add(15, 'minutes').toDate();

    this.TimeSlotsRepository.create(timeSlot).then(response => {
      this.all_time_slots.push(response.data);

      this.all_time_slots = _.without(this.all_time_slots, timeSlot);
      this.regenerate_time_slots()
    })
  }

  addDay() {
    this.newDay = {};
    this.newDayAvailableStartTimes = []

    let startTime = moment().local().startOf('day').hours(8);

    while (startTime.hours() <= 21) {
      this.newDayAvailableStartTimes.push(moment(startTime).toDate());
      startTime.add(15, 'minutes');
    }
  }

  saveDay() {
    let time_slot = {};

    time_slot.start_time = moment(this.newDay.start_date).
      hours(this.newDay.start_time.getHours()).
      minutes(this.newDay.start_time.getMinutes()).
      toDate();

    // time_slot.end_time = this.newDay.end_time;

    time_slot.end_time = moment(this.newDay.start_date).
      hours(this.newDay.end_time.getHours()).
      minutes(this.newDay.end_time.getMinutes()).
      toDate();

    this.TimeSlotsRepository.createDay(time_slot).then(response => {
      this.newDay = null;
      this.all_time_slots = this.all_time_slots.concat(response.data.time_slots);
      this.regenerate_time_slots()
    })
  }

  delete(time_slot) {
    this.TimeSlotsRepository.delete(time_slot).then(response => {
      this.all_time_slots = _.without(this.all_time_slots, time_slot);
      this.regenerate_time_slots()
    })
  }
}

TimeSlotsCtrl.$inject = ['TimeSlotsRepository'];

export default TimeSlotsCtrl;
