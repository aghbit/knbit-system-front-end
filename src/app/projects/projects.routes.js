/**
 * Created by novy on 22.05.15.
 */

'use strict';

var Routes = function ($stateProvider) {

  $stateProvider
    // Application forms
    .state('projects.application_forms', {
      url: '/application_forms',
      abstract: true,
      template: '<div ui-view></div>',
    })
    .state('projects.application_forms.current', {
      url: '/current',
      controller: 'ApplicationFormsCurrentCtrl',
      data: {
        requireSignedIn: true
      }
    })
    .state('projects.application_forms.new', {
      url: '/new',
      controller: 'ApplicationFormsNewCtrl',
      controllerAs: 'form',
      templateUrl: 'app/projects/templates/application_forms/new.html',
      data: {
        requireSignedIn: true
      }
    })
    .state('projects.application_forms.setup_meeting', {
      url: '/setup_meeting',
      controller: 'ApplicationFormsSetupMeetingCtrl',
      controllerAs: 'setup',
      templateUrl: 'app/projects/templates/application_forms/setup_meeting.html',
      data: {
        requireSignedIn: true
      }
    })

    // Enrollments
    .state('projects.enrollments', {
      url: '/enrollments',
      abstract: true,
      template: '<div ui-view></div>',
    })
    .state('projects.enrollments.time_slots', {
      url: '/time_slots',
      controller: 'EnrollmentsTimeSlotsCtrl',
      controllerAs: 'time_slots',
      templateUrl: 'app/projects/templates/enrollments/time_slots.html',
      data: {
        permission: 'ADMIN',
      }
    })
    .state('projects.enrollments.index', {
      url: '/',
      controller: 'EnrollmentsIndexCtrl',
      controllerAs: 'index',
      templateUrl: 'app/projects/templates/enrollments/index.html',
      data: {
        permission: 'ADMIN',
      }
    })
    .state('projects.enrollments.edit', {
      url: '/:id',
      controller: 'EnrollmentsEditCtrl',
      controllerAs: 'edit',
      templateUrl: 'app/projects/templates/enrollments/edit.html',
      data: {
        permission: 'ADMIN',
      }
    })

    // Projects
    .state('projects.index', {
      url: '/',
      controller: 'ProjectsIndexCtrl',
      controllerAs: 'index',
      templateUrl: 'app/projects/templates/projects/index.html',
      data: {
        permission: 'ADMIN',
      }
    })
    .state('projects.new', {
      url: '/new',
      controller: 'ProjectsNewCtrl',
      controllerAs: 'new',
      templateUrl: 'app/projects/templates/projects/new.html',
      data: {
        permission: 'ADMIN',
      }
    })
    .state('projects.edit', {
      url: '/:id/edit',
      controller: 'ProjectsEditCtrl',
      controllerAs: 'edit',
      templateUrl: 'app/projects/templates/projects/edit.html',
      data: {
        permission: 'ADMIN'
      }
    })
    .state('projects.show', {
      url: '/:id',
      controller: 'ProjectsShowCtrl',
      controllerAs: 'show',
      templateUrl: 'app/projects/templates/projects/show.html',
      data: {
        permission: 'ADMIN',
      }
    });

};

Routes.$inject = ['$stateProvider'];


export default Routes;
