'use strict';

import SemestersModule from './semesters/semesters.module.js';
import ReportsModule from './reports/reports.module.js';

import Routes from './timeline.routes.js';

var KnbitFrontendTimelineModule = angular.module('knbitFrontend.Timeline', [
  'ui.router',
  'ui.bootstrap',
  'knbitFrontend.Timeline.Config',
  SemestersModule.name,
  ReportsModule.name
])
  .config(Routes);

export default KnbitFrontendTimelineModule;
