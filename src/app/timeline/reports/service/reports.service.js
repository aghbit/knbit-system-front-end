'use strict';

let DATE_FORMAT = 'YYYY-MM-DD';

class ReportsService {
  constructor($resource, TIMELINE_URLS, $q) {
    let url = TIMELINE_URLS.endpoint + TIMELINE_URLS.reports + '/pretty';

    let actions = {
      generate: {
        method: 'GET',
        transformResponse: (data) => {
          return {content: data}
        }
      }
    };
    this.reportsResource = $resource(url, {}, actions);
    this.$q = $q;
  }

  generate(startDate, endDate) {
    startDate = moment(startDate).format(DATE_FORMAT);
    endDate = moment(endDate).format(DATE_FORMAT);

    return this.reportsResource.generate({'start-date': startDate, 'end-date': endDate}).$promise.then(response => response.content);
  }

}

ReportsService.$inject = ['$resource', 'TIMELINE_URLS', '$q'];

export default ReportsService;
