'use strict';

let generator = function (ReportsService, $q) {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/reports/generator/generator.tpl.html',
    link: function (scope) {
      scope.generateReport = function (startDate, endDate) {
        scope.generatingFailed = false;
        scope.generatingSuccessed = false;

        ReportsService.generate(startDate, endDate)
          .catch(err => {
            scope.generatingFailed = true;
            return $q.reject(err);
          })
          .then(report => {
            scope.generatingSuccessed = true;
            scope.report = report
          });
      };

      scope.isFormValid = function (form) {
        return scope.startDate && scope.endDate && form.$valid;
      };
    }
  }
};

generator.$inject = ['ReportsService', '$q'];

export default generator;
