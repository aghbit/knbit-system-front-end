'use strict';

import reportGenerator from './generator/generator.directive.js';
import ReportsService from './service/reports.service.js';

let ReportsModule = angular.module('knbitFrontend.Timeline.Reports', [])
  .directive('reportGenerator', reportGenerator)
  .service('ReportsService', ReportsService);

export default ReportsModule;
