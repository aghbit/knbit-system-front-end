import SemestersService from './service/semesters.service.js';
import MembersService from './service/members.service.js';

let CommonModule = angular.module('knbitFrontend.Timeline.Semesters.Common', [
  'knbitFrontend.Timeline.Config',
  'ngResource'
])
  .service('SemestersService', SemestersService)
  .service('MembersService', MembersService);

export default CommonModule;
