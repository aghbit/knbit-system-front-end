'use strict';

class MembersService {

  constructor($resource, TIMELINE_URLS) {
    let url = TIMELINE_URLS.endpoint + TIMELINE_URLS.members + '/:id';
    this.membersResource = $resource(url, {id: '@id'});
  }

  get(id) {
    return this.membersResource.get({id: id}).$promise.then(resource => resource.data);
  }
}

MembersService.$inject = ['$resource', 'TIMELINE_URLS'];

export default MembersService;
