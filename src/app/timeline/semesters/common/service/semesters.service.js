'use strict';

class SemestersService {

  constructor($resource, TIMELINE_URLS) {
    let semestersUrl = TIMELINE_URLS.endpoint + TIMELINE_URLS.semesters + '/:id';
    let activateUrl = TIMELINE_URLS.endpoint + TIMELINE_URLS.activate + '/:id';
    this.semestersResource = $resource(semestersUrl, {id: '@id'});
    this.activateResource = $resource(activateUrl, {id: '@id'});
  }

  get(id) {
    return this.semestersResource.get({id: id}).$promise.then(resource => resource.data);
  }

  getAll() {
    return this.get();
  }

  activate(id) {
    return this.activateResource.save({id: id}).$promise.then(resource => resource.data);
  }
}

SemestersService.$inject = ['$resource', 'TIMELINE_URLS'];

export default SemestersService;
