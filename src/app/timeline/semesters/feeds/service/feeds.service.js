'use strict';

class FeedsService {

  constructor($resource, TIMELINE_URLS) {
    let feedsUrl = TIMELINE_URLS.endpoint + TIMELINE_URLS.feeds;
    this.feedsResource = $resource(feedsUrl);
  }

  loadPage(page) {
    page = page || 0;
    return this.feedsResource.get({page: page}).$promise.then(resource => resource.data);
  }
}

FeedsService.$inject = ['$resource', 'TIMELINE_URLS'];

export default FeedsService;
