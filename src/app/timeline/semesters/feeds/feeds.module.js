'use strict';

import feedsDirective from './directive/feeds.directive.js';
import feedsService from './service/feeds.service.js';

let FeedsModule = angular.module('knbitFrontend.Timeline.Semesters.Feeds', [
  'ui.bootstrap'
])
  .directive('timelineFeeds', feedsDirective)
  .service('FeedsService', feedsService);

export default FeedsModule;
