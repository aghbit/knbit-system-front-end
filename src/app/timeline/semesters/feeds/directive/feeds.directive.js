'use strict';

const DEFAULT_CHUNK_SIZE = 5;
const FEED_LINE_HEIGHT_IN_REM = 3;
const FEEDS_SELECTOR = '.feeds__list';
const DEFAULT_SCROLL_SPEED = 1000;

let feeds = function (FeedsService, $q) {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/feeds/directive/feeds.tpl.html',
    scope: {
      chunkSize: '@'
    },
    link: function (scope, el) {
      let cache = [];
      let page = 0;
      let allFeedsLoaded = false;
      let chunkSize = scope.chunkSize || DEFAULT_CHUNK_SIZE;
      let scrollSpeed = scope.scrollSpeed || DEFAULT_SCROLL_SPEED;

      el.find(FEEDS_SELECTOR).css({
        'max-height': FEED_LINE_HEIGHT_IN_REM * chunkSize + 'rem',
        'line-height': FEED_LINE_HEIGHT_IN_REM + 'rem'
      });

      scope.feeds = [];
      scope.allFeedsShown = false;

      scope.showMoreFeeds = () => {
        getFeeds(chunkSize).then(feeds => {
          scope.feeds = scope.feeds.concat(feeds);
          scope.allFeedsShown = (scope.feeds.length === cache.length);
        });
      };

      scope.showMoreFeeds();

      scope.$watch('feeds.length', (oldVal, newVal) => {
        if (newVal === oldVal) {
          return;
        }
        let $feeds = el.find(FEEDS_SELECTOR);
        let offset = $feeds[0].scrollHeight;

        $feeds.animate({scrollTop: offset}, scrollSpeed);
      });

      function getFeeds(count) {
        let feeds = cache.slice(scope.feeds.length, scope.feeds.length + count);
        let deferred = $q.defer();

        if (feeds.length === count || allFeedsLoaded) {
          deferred.resolve(feeds);
          return deferred.promise;
        }

        return FeedsService.loadPage(page++)
          .then(feeds => {
            cache = cache.concat(feeds.content);
            allFeedsLoaded = (cache.length === feeds.total);
            return getFeeds(count);
          });
      }
    }
  }
};

feeds.$inject = ['FeedsService', '$q'];

export default feeds;

