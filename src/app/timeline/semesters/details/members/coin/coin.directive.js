'use strict';

const MEMBER_SELECTOR = '.member';

let coin = function ($timeout) {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/members/coin/coin.tpl.html',
    link: function (scope, el, attrs) {
      let memberEl = el.find(MEMBER_SELECTOR);

      memberEl.css({
        'transition': 'transform ' + attrs.transition + 'ms ease',
        'backface-visibility': 'hidden'
      });

      scope.$watch(attrs.member, (member) => {
        memberEl.css({'transform': 'rotateY(180deg)'});

        $timeout(() => {
          scope.member = member;
          memberEl.css({'transform': 'rotateY(360deg)'});
        }, attrs.transition);
      });
    }
  }
};

coin.$inject = ['$timeout'];

export default coin;
