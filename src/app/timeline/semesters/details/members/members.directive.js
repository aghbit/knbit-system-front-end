'use strict';

var members = function (MembersService, $q, $timeout) {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/members/members.tpl.html',
    scope: {
      'members': '=',
      'coinsCount': '=',
      'delay': '='
    },
    link: function (scope, el) {
      scope.coins = [];

      let memberIds = scope.members.map(member => member.memberId);
      let size = Math.min(scope.coinsCount, memberIds.length);

      let timeoutId;

      updateCoins(scope.coins, memberIds, 0, size, scope.delay);

      function updateCoins(coins, ids, start, size, delay) {
        let memberPromises = ids
          .slice(start, start + size)
          .map(id => MembersService.get(id));

        $q.all(memberPromises)
          .then(members => {
            members
              .map((member, idx) => {
                let coin = coins[idx] || {};
                coin.member = member;
                coins[idx] = coin;
              });

            let newStart = (start + size + 1) % ids.length;
            let newSize = Math.min(coins.length, ids.length - newStart);

            timeoutId = $timeout(updateCoins.bind(null, coins, ids, newStart, newSize, delay), delay);
          });
      }

      el.on('$destroy', () => {
        if (timeoutId) {
          $timeout.cancel(timeoutId);
        }
      });
    }
  };
};

members.$inject = ['MembersService', '$q', '$timeout'];

export default members;
