'use strict';

import CommonModule from './../common/common.module.js';

import DetailsController from './details.controller.js';
import detailsDraftBar from './draft-bar/draft-bar.directive.js';
import detailsStats from './stats/stats.directive.js';
import detailsMembers from './members/members.directive.js'; // TODO: extract member to directive
import detailsMembersCoin from './members/coin/coin.directive.js';
import detailsSections from './sections/sections.directive.js';
import detailsEvents from './events/events.directive.js';
import detailsProjects from './projects/projects.directive.js';

let DetailsModule = angular.module('knbitFrontend.Timeline.Semesters.Details', [
  CommonModule.name,
  'ngAnimate',
  'ocNgRepeat'
])
  .controller('SemestersDetailsController', DetailsController)
  .directive('semestersDetailsDraftBar', detailsDraftBar)
  .directive('semestersDetailsStats', detailsStats)
  .directive('semestersDetailsMembers', detailsMembers)
  .directive('semestersDetailsMembersCoin', detailsMembersCoin)
  .directive('semestersDetailsSections', detailsSections)
  .directive('semestersDetailsEvents', detailsEvents)
  .directive('semestersDetailsProjects', detailsProjects);

export default DetailsModule;
