'use strict';

let projects = function () {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/projects/projects.tpl.html',
    scope: {
      projects: '=',
      limit: '='
    }
  }
};

export default projects;
