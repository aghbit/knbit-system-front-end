'use strict';

let stats = function () {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/stats/stats.tpl.html',
    scope: {
      stats: '='
    }
  }
};

export default stats;
