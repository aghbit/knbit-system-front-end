'use strict';

class SemestersDetailsController {

  constructor(MembersService, SemestersService, SECTION_CLASSES, $stateParams, $q) {
    this.membersService = MembersService;
    this.SECTION_CLASSES = SECTION_CLASSES;

    this.stats = {};
    this.members = [];
    this.sections = [];
    this.events = [];
    this.projects = [];

    this.displayLimits = {
      members: 8,
      events: 4,
      projects: 3
    };

    this.semesterLoadingSucceeded = false;
    this.semesterLoadingFailed = false;
    this.semesterLoadingInProgress = true;

    SemestersService.get($stateParams.id)
      .catch(err => {
        this.semesterLoadingFailed = true;
        this.semesterLoadingInProgress = false;
        return $q.reject(err);
      })
      .then(semester => {
        this.semesterLoadingSucceeded = true;
        this.semesterLoadingInProgress = false;

        this.isDraft = semester.isDraft;
        this.semesterId = semester.semesterId;

        this.members = semester.members;

        this.sections = semester.sections.map(section => {
          section.className = this.SECTION_CLASSES[section.name];
          return section;
        });

        this.events = semester.events
          .map(this.addSectionData.bind(this))
          .map(this.loadSpeakersData.bind(this));

        this.projects = semester.projects.map(this.addSectionData.bind(this));

        this.stats = {
          membersCount: this.members.length,
          sectionsCount: this.sections.length,
          projectsCount: this.projects.length,
          eventsCount: this.events.length,
          commitsCount: semester.commitsCount
        };
      });
  }

  loadSpeakersData(event) {
    event.speakers.map(speaker => {
      this.membersService.get(speaker.memberId)
        .then(speakerData => {
          angular.extend(speaker, speakerData);
        });
    });
    return event;
  }

  addSectionData(thing) {
    thing.section = this.sections.find(section => section.sectionId === thing.sectionId);
    return thing;
  }
}

SemestersDetailsController.$inject = ['MembersService', 'SemestersService', 'SECTION_CLASSES', '$stateParams', '$q'];

export default SemestersDetailsController;
