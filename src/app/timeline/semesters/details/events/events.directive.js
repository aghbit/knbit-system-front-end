'use strict';

let events = function () {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/events/events.tpl.html',
    scope: {
      events: '=',
      limit: '='
    }
  }
};

export default events;
