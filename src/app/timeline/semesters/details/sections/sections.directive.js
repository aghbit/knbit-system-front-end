'use strict';

const DEFAULT_OPTIONS = {
  navigation: true,
  items: 3,
  itemsDesktop: 3,
  navigationText: ["<i class='material-icons'>keyboard_arrow_left</i>", "<i class='material-icons'>keyboard_arrow_right</i>"]
};
const CAROUSEL_SELECTOR = '.owl-carousel';

let sections = function () {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/sections/sections.tpl.html',
    scope: {
      sections: '='
    },
    link: function (scope, el) {
      scope.initCarousel = () => {
        let carousel = el.find(CAROUSEL_SELECTOR);
        carousel.owlCarousel(DEFAULT_OPTIONS);
      };
    }
  }
};

export default sections;
