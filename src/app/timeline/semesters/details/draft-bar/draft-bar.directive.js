'use strict';

const FIXED_CLASS = 'fixed';
const TIMEOUT = 2000;

let draftBar = function (SemestersService, $state, $timeout) {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/draft-bar/draft-bar.tpl.html',
    scope: {
      semesterId: '='
    },
    link: function (scope, el) {
      let $window = angular.element(window);
      let $barEl = el.find('.draft-bar');
      let barElTopPos = $barEl.offset().top;

      $window.on('scroll', checkOffset);

      checkOffset();

      el.on('destroy', function () {
        $window.off('scroll', checkOffset);
      });

      scope.semesterActivationInProgress = false;
      scope.semesterActivationFailed = false;
      scope.semesterActivationSucceeded = false;
      scope.semesterAlreadyActive = false;

      scope.activateSemester = activateSemester;

      function checkOffset() {
        if ($window.scrollTop() >= barElTopPos) {
          $barEl.addClass(FIXED_CLASS);
        } else {
          $barEl.removeClass(FIXED_CLASS);
        }
      }

      function activateSemester() {
        scope.semesterActivationInProgress = true;
        return SemestersService.activate(scope.semesterId)
          .catch(err => {
            scope.semesterActivationInProgress = false;
            scope.semesterActivationFailed = true;
            $timeout(() => {
              scope.semesterActivationFailed = false;
            }, TIMEOUT);
          })
          .then(semester => {
            scope.semesterActivationInProgress = false;
            scope.semesterActivationSucceeded = true;
            $timeout(() => {
              $state.reload();
            }, TIMEOUT);
          })
      }
    }
  }

};

draftBar.$inject = ['FeedsService', '$state', '$timeout'];

export default draftBar;
