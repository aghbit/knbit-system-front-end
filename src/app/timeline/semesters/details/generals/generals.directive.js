'use strict';

let generals = function () {
  return {
    restrict: 'E',
    templateUrl: 'app/timeline/semesters/details/generals/generals.directive.js',
    scope: {
      stats: '='
    }
  }
};

export default generals;
