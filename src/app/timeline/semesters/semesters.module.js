'use strict';

import CommonModule from './common/common.module.js';
import DetailsModule from './details/details.module.js';
import NavModule from './nav/nav.module.js';
import FeedsModule from './feeds/feeds.module.js';

let SemestersModule = angular.module('knbitFrontend.Timeline.Semesters', [
  CommonModule.name,
  DetailsModule.name,
  NavModule.name,
  FeedsModule.name
]);

export default SemestersModule;
