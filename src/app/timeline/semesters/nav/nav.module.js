'use strict';

import CommonModule from './../common/common.module.js';

import NavController from './nav.controller.js';

let NavModule = angular.module('knBitFrontend.Timeline.Semesters.Nav', [
  CommonModule.name
])
  .controller('SemestersNavController', NavController);

export default NavModule;
