'use strict';

class SemestersNavController {
  constructor(SemestersService) {
    SemestersService.getAll().then(semesters => {
      this.semesters = semesters;
    });
  }
}

SemestersNavController.$inject = ['SemestersService'];

export default SemestersNavController;
