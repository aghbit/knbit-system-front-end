'use strict';

var Routes = function ($stateProvider) {
  $stateProvider

    // -- SEMESTERS --
    .state('timeline.semesters', {
      abstract: true,
      url: '/semesters',
      views: {
        '': {
          templateUrl: 'app/timeline/semesters/semesters.tpl.html'
        },
        'semesters-nav@timeline.semesters': {
          templateUrl: 'app/timeline/semesters/nav/nav.tpl.html',
          controller: 'SemestersNavController',
          controllerAs: 'vm'
        },
        'semesters-details@timeline.semesters': {
          template: '<div ui-view></div>'
        }
      }
    })
    .state('timeline.semesters.last', {
      url: '/last',
      resolve: {
        semesterRedirect: ['SemestersService', '$stateParams', '$state', function (SemestersService, $stateParams, $state) {
          // TODO: remove when proper endpoint on backend created
          return SemestersService.getAll().then(semesters => {
            $state.go('timeline.semesters.details', {id: semesters[0].semesterId});
          });
        }]
      }
    })
    .state('timeline.semesters.details', {
      url: '/:id',
      templateUrl: 'app/timeline/semesters/details/details.tpl.html',
      controller: 'SemestersDetailsController',
      controllerAs: 'vm',
      resolve: {
        lastSemester: ['$stateParams', '$state', function ($stateParams, $state) {
          if ($stateParams.id === '') {
            // TODO: empty param should be handled by ui router, but non-empty id regex doesn't work
            $state.go('timeline.semesters.last');
          }
        }]
      }
    })

    //  -- REPORTS --
    .state('timeline.reports', {
      url: '/reports',
      templateUrl: 'app/timeline/reports/reports.tpl.html',
      data: {
        permission: 'ADMIN'
      }
    });
};

Routes.$inject = ['$stateProvider'];

export default Routes;
