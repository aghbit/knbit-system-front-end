'use strict';

import EventsCommonModule from './common/common.module.js';
import EventMasterModule from './eventmaster/eventmaster.module.js';
import MembersModule from './member/member.module.js';
import EventsOutsiderModule from './outsider/outsider.module.js';

var knbitFrontendEventsModule = angular.module('knbitFrontend.Events', [
  EventsCommonModule.name,
  EventMasterModule.name,
  MembersModule.name,
  EventsOutsiderModule.name,
  'ngMessages'
]);

export default knbitFrontendEventsModule;
