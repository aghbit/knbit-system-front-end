'use strict';

var OutsiderRoutes = function ($stateProvider) {

  $stateProvider
    .state('events.home', {
      url: '/home',
      resolve: {
        redirect: ['AuthService', '$state', function (AuthService, $state) {
          return AuthService
            .isLoggedIn()
            .then(
            () => $state.go('events.members.content.dashboard'),
            () => $state.go('events.outsider')
          );
        }]
      }
    })
    .state('events.outsider', {
      url: '/outsider',
      views: {
        'events-header': {
          templateUrl: 'app/events/outsider/header/header.tpl.html'
        },
        'events-content': {
          templateUrl: 'app/events/outsider/event-proposal/event-proposal.tpl.html',
          controller: 'OutsiderEventProposalController',
          controllerAs: 'ep'
        }
      }
    });

};

OutsiderRoutes.$inject = ['$stateProvider'];

export default OutsiderRoutes;
