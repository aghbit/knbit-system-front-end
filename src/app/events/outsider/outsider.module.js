'use strict';

import OutsiderRoutes from './outsider.routes.js';
import OutsiderEventProposalModule from './event-proposal/event-proposal.module.js';

var EventsOutsiderModule = angular.module('knbitFrontend.Events.Outsider', [
  OutsiderEventProposalModule.name
])
  .config(OutsiderRoutes);

export default EventsOutsiderModule;
