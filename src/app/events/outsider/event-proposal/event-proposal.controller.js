'use strict';

function OutsiderEventProposalController(OutsiderEventProposalResource, ToastingService, Messages) {

  var ep = this;

  ep.eventTypes = [
    {name: 'Lecture', value: 'LECTURE'},
    {name: 'Workshop', value: 'WORKSHOP'},
    {name: 'Study Group', value: 'STUDY_GROUP'}
  ];

  ep.event = {
    name: null,
    imageUrl: null,
    description: null,
    eventType: 'LECTURE'
  };

  ep.submit = submit;

  function submit() {
    OutsiderEventProposalResource
      .createEventProposalOf(ep.event)
      .then(success, error);
  }

  function success() {
    ToastingService.showSuccessToast(Messages.EVENT_PROPOSAL.success);
  }

  function error() {
    ToastingService.showErrorToast(Messages.EVENT_PROPOSAL.error);
  }

}

OutsiderEventProposalController.$inject = ['OutsiderEventProposalResource', 'ToastingService', 'Messages'];

export default OutsiderEventProposalController;
