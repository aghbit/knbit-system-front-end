'use strict';

class OutsiderEventProposalResource {

  constructor($http, proposalConfig) {
    this.$http = $http;
    this.proposalConfig = proposalConfig;
  }

  createEventProposalOf(payload) {
    return this.$http({
      method: 'POST',
      url: this.proposalConfig.proposalUrl,
      data: payload
    });
  }

}

OutsiderEventProposalResource.$inject = ['$http', 'proposalConfig'];

export default OutsiderEventProposalResource;
