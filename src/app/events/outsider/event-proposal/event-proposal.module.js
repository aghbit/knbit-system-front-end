'use strict';

import OutsiderEventProposalController from './event-proposal.controller.js';
import OutsiderEventProposalResource from './event-proposal.resource.js';

var OutsiderEventProposalModule = angular.module('knbitFrontend.Events.Outsider.EventProposal', [
])
  .controller('OutsiderEventProposalController', OutsiderEventProposalController)
  .service('OutsiderEventProposalResource', OutsiderEventProposalResource);


export default OutsiderEventProposalModule;
