'use strict';

var MemberRoutes = function ($stateProvider) {

  $stateProvider
    .state('events.members', {
      url: '/member',
      abstract: true,
      views: {
        'events-header': {
          template: '<knbit-events-member-header></knbit-events-member-header>'
        },
        'events-content': {
          templateUrl: 'app/events/member/common/layout/layout.html'
        }
      }
    })
    .state('events.members.content', {
      url: '',
      abstract: true,
      views: {
        'events-members-sidebar@events.members': {
          templateUrl: 'app/events/member/common/sidebar/sidebar.partial.html'
        }
      }
    })
    .state('events.members.content.dashboard', {
      url: '/dashboard',
      views: {
        'events-members-content@events.members': {
          templateUrl: 'app/events/member/dashboard/dashboard.html',
          controller: 'MemberDashboardCtrl',
          controllerAs: 'dashboard'
        }
      },
      data: {
        requireSignedIn: true
      }
    })
    .state('events.members.content.survey', {
      url: '/surveying',
      views: {
        'events-members-content@events.members': {
          templateUrl: 'app/events/member/survey/survey.html',
          controller: 'MemberSurveyCtrl',
          controllerAs: 'survey'
        }
      },
      data: {
        requireSignedIn: true
      }
    })
    .state('events.members.content.term-choosing', {
      url: '/term-choosing',
      views: {
        'events-members-content@events.members': {
          templateUrl: 'app/events/member/term-choosing/term-choosing.html',
          controller: 'MemberTermChoosingCtrl',
          controllerAs: 'termchoosing'
        }
      },
      data: {
        requireSignedIn: true
      }
    });

};

MemberRoutes.$inject = ['$stateProvider'];

export default MemberRoutes;
