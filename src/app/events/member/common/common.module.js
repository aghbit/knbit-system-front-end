'use strict';

import EventsCommonModule from '../../common/common.module.js';

import AskEventMasterModule from './ask-event-master/ask-event-master.module.js';
import CheckmarkModule from './checkmark/checkmark.module.js';
import TermDetailsModule from './term-details/term-details.module.js';
import EmptyEventsListModule from './empty-events-list/empty-events-list.module.js';
import HeaderModule from './header/header.module.js';
import EventImageModule from './event-image/event-image.module.js';

var MemberCommonModule = angular.module('knbitFrontend.Events.Members.Common', [
  EventsCommonModule.name,
  AskEventMasterModule.name,
  CheckmarkModule.name,
  TermDetailsModule.name,
  EmptyEventsListModule.name,
  HeaderModule.name,
  EventImageModule.name
]);

export default MemberCommonModule;
