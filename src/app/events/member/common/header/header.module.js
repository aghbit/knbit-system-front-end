/**
 * Created by novy on 26.10.15.
 */

'use strict';

import HeaderDirective from './header.directive.js';
import MemberHeaderCtrl from './header.controller.js';
import HeaderResource from './header.reasource.js';

const HeaderModule = angular.module('knbitFrontend.Events.Members.Common.Header', [
  'knbitFrontend.Events.Config',
  'AuthModule'
]);

HeaderModule
  .directive('knbitEventsMemberHeader', HeaderDirective)
  .controller('MemberHeaderCtrl', MemberHeaderCtrl)
  .factory('headerResource', HeaderResource);

export default HeaderModule;
