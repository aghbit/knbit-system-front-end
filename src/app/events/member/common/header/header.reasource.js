/**
 * Created by novy on 26.10.15.
 */

'use strict';

HeaderResource.$inject = ['$http', 'membersConfig'];

function HeaderResource($http, membersConfig) {

  return {headerDataFor: headerDataFor};

  function headerDataFor(memberId) {
    const endpointUrl = `${membersConfig.headerUrl}/${memberId}`;

    return $http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }
}

export default HeaderResource;
