/**
 * Created by novy on 26.10.15.
 */

'use strict';

function HeaderDirective() {

  return {
    restrict: 'E',
    scope: {},
    templateUrl: 'app/events/member/common/header/header.tpl.html',
    controller: 'MemberHeaderCtrl',
    controllerAs: 'header'
  }
}

export default HeaderDirective;
