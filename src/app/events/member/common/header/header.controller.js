'use strict';

class MemberHeaderCtrl {

  constructor(headerResource, AuthService) {
    this.headerResource = headerResource;

    const memberId = AuthService.getUserId();
    this.headerDataFor(memberId);
  }

  headerDataFor(memberId) {
    this.headerResource
      .headerDataFor(memberId)
      .then(headerData => this.headerData = headerData);
  }

  shouldDisplayNextEvent() {
    return angular.isDefined(this.headerData) && !_.isEmpty(this.headerData.nextEvent);
  }

  shouldDisplayNoEventsInfo() {
    return angular.isDefined(this.headerData) && _.isEmpty(this.headerData.nextEvent);
  }

  sectionOrDefault() {
    return this.headerData.nextEvent.section ? this.headerData.nextEvent.section.name : 'KNBIT';
  }

}

MemberHeaderCtrl.$inject = ['headerResource', 'AuthService'];

export default MemberHeaderCtrl;
