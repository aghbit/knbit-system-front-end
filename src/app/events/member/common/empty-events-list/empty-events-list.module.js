'use strict';

import EmptyEventsListDirective from './empty-events-list.directive.js';

const EmptyEventsListModule = angular.module('knbitFrontend.Events.Members.Common.EmptyEventsList', [])
  .directive('knbitEventsMemberEmptyEventsList', EmptyEventsListDirective);


export default EmptyEventsListModule;
