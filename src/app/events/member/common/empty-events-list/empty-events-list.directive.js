'use strict';

function EmptyEventsListDirective() {

  return {
    restrict: 'E',
    templateUrl: 'app/events/member/common/empty-events-list/empty-events-list.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };

}

export default EmptyEventsListDirective;
