'use strict';

import TermDetailsDirective from './term-details.directive.js';

const TermDetailsModule = angular.module('knbitFrontend.Events.Members.Common.TermDetails', [])
  .directive('knbitEventsMemberTermDetails', TermDetailsDirective);


export default TermDetailsModule;

