'use strict';

function TermDetailsDirective() {

  return {
    restrict: 'E',
    scope: {
      icon: '@',
      header: '@'
    },
    transclude: true,
    templateUrl: 'app/events/member/common/term-details/term-details.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };

}

export default TermDetailsDirective;
