/**
 * Created by novy on 16.10.15.
 */

'use strict';

AskEventMasterModalService.$inject = ['$modal'];

function AskEventMasterModalService($modal) {

  return {openQuestionModalWith: openQuestionModalWith};

  function openQuestionModalWith(question) {
    const modalOptions = {
      animation: true,
      size: 'md',
      templateUrl: 'app/events/member/common/ask-event-master/modal/ask-event-master.tpl.html',
      controller: 'AskEventMasterModalController',
      controllerAs: 'vm',
      resolve: {
        question: () => question
      }
    };

    return $modal
      .open(modalOptions)
      .result;
  }
}

export default AskEventMasterModalService;
