/**
 * Created by novy on 29.06.15.
 */

'use strict';

AskEventMasterModalController.$inject = ['$modalInstance', 'question'];

function AskEventMasterModalController($modalInstance, question) {
  const vm = this;

  vm.question = question;
  vm.askQuestion = () => $modalInstance.close(vm.question);
}

export default AskEventMasterModalController;
