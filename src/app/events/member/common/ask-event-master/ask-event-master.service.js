/**
 * Created by novy on 29.06.15.
 */

'use strict';

class QuestionService {

  constructor($resource, membersConfig) {
    let allowedMethods = {
      'post': {
        method: 'POST'
      }
    };

    this.questionResource = $resource(membersConfig.questionUrl, {}, allowedMethods);
  }

  ask(question) {
    return this.questionResource
      .post(question)
      .$promise;
  }

}

QuestionService.$inject = ['$resource', 'membersConfig'];

export default QuestionService;
