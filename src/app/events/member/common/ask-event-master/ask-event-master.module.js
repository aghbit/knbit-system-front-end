/**
 * Created by novy on 16.10.15.
 */

'use strict';

import AskEventMasterDirective from './ask-even-master.directive.js';
import AskEventMasterController from './ask-event-master.controller.js';
import askEventMasterService from './ask-event-master.service.js';

import AskEventMasterModalController from './modal/ask-event-master-modal.controller.js';
import askEventMasterModalService from './modal/ask-event-master-modal.service.js';

const AskEventMasterModule = angular.module('knbitFrontend.Events.Members.Common.AskEventMaster', [
  'ui.bootstrap'
]);

AskEventMasterModule
  .controller('AskEventMasterController', AskEventMasterController)
  .directive('knbitEventsAskEventMaster', AskEventMasterDirective)
  .service('askEventMasterService', askEventMasterService)

  .factory('askEventMasterModalService', askEventMasterModalService)
  .controller('AskEventMasterModalController', AskEventMasterModalController);

export default AskEventMasterModule;
