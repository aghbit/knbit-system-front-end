'use strict';

AskEventMasterController.$inject = ['askEventMasterModalService', 'askEventMasterService'];

function AskEventMasterController(askEventMasterModalService, askEventMasterService) {

  const ctrl = this;

  ctrl.askQuestion = askQuestion;

  function askQuestion() {
    openAskQuestionModal()
      .then(handleQuestionAsked);
  }

  function openAskQuestionModal() {
    const emptyQuestionWithEventInfo = {eventId: ctrl.eventId, eventName: ctrl.eventName};

    return askEventMasterModalService
      .openQuestionModalWith(emptyQuestionWithEventInfo);
  }

  function handleQuestionAsked(question) {
    return askEventMasterService.ask(question);
  }
}

export default AskEventMasterController;
