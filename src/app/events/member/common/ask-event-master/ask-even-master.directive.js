'use strict';

function AskEventMasterDirective() {

  const template = '<div style="cursor: pointer" ng-click="ctrl.askQuestion()" ng-transclude></div>';

  return {
    restrict: 'E',
    scope: {
      eventId: '@',
      eventName: '@'
    },
    template: template,
    transclude: true,
    controller: 'AskEventMasterController',
    controllerAs: 'ctrl',
    bindToController: true
  };
}

export default AskEventMasterDirective;
