'use strict';

function MemberEventImageDirective() {

  return {
    restrict: 'E',
    scope: {
      url: '@'
    },
    template: '<a href="{{ vm.url }}" target="_blank">' +
    '<img class="event-image" alt="Event related image" ng-src="{{ vm.url }}"/>' +
    '</a>',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };

}

export default MemberEventImageDirective;
