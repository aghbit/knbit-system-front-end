'use strict';

import MemberEventImageDirective from './event-image.directive.js';

const EventImageModule = angular.module('knbitFrontend.Events.Members.Common.EventImage', [])
  .directive('knbitEventsMemberEventImage', MemberEventImageDirective);


export default EventImageModule;
