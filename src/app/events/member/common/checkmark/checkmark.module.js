'use strict';

import CheckmarkDirective from './checkmark.directive.js';

const CheckmarkModule = angular.module('knbitFrontend.Events.Members.Common.Checkmark', [])
  .directive('knbitEventsMemberCheckmark', CheckmarkDirective);


export default CheckmarkModule;
