'use strict';

function CheckmarkDirective() {

  return {
    restrict: 'E',
    scope: {
      text: '@',
      type: '@',
      size: '@'
    },
    templateUrl: 'app/events/member/common/checkmark/checkmark.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };

}

export default CheckmarkDirective;
