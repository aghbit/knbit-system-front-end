'use strict';

class MemberDashboardCtrl {

  constructor(MemberDashboardResource, AuthService) {
    this.MemberDashboardResource = MemberDashboardResource;
    this.AuthService = AuthService;
    this.emptyListMessage = false;

    this.initialize();
  }

  groupByDate(events) {
    return _.groupBy(
      events,
      (event) => moment(event.start).format('dddd, MMMM DD')
    );
  }

  // TODO: move to common component
  emptyListMessageOrContent(events) {
    this.emptyListMessage = events.length === 0;
    return events;
  }

  initialize() {
    const userId = this.AuthService.getUserId();
    this.MemberDashboardResource
      .getAll(userId)
      .then(events => this.emptyListMessageOrContent(events))
      .then(this.groupByDate)
      .then(events => this.events = events);
  }

}

MemberDashboardCtrl.$inject = ['MemberDashboardResource', 'AuthService'];

export default MemberDashboardCtrl;
