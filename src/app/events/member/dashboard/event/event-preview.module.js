'use strict';

import ReadyEventPreviewDirective from './event-preview.directive.js';
import ReadyMemberEventPreviewCtrl from './event-preview.controller.js';

const ReadyEventModule = angular.module('knbitFrontend.Events.Members.Dashboard.Event', [])
  .directive('knbitEventsMemberReadyEventPreview', ReadyEventPreviewDirective)
  .controller('ReadyMemberEventPreviewCtrl', ReadyMemberEventPreviewCtrl);


export default ReadyEventModule;

