'use strict';

function ReadyEventPreviewDirective() {

  return {
    restrict: 'E',
    scope: {
      event: '='
    },
    controller: 'ReadyMemberEventPreviewCtrl',
    controllerAs: 're',
    bindToController: true,
    templateUrl: 'app/events/member/dashboard/event/event-preview.tpl.html'
  };

}

export default ReadyEventPreviewDirective;
