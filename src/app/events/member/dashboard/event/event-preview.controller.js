'use strict';

function ReadyMemberEventPreviewCtrl() {
  const re = this;

  re.membersEnrolled = membersEnrolled;
  re.spotsLeft = spotsLeft;

  function membersEnrolled() {
    return re.event.attendees.length;
  }

  function spotsLeft() {
    return re.event.participantsLimit - membersEnrolled();
  }

}


export default ReadyMemberEventPreviewCtrl;
