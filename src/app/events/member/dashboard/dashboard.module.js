/**
 * Created by novy on 29.06.15.
 */

'use strict';

import MemberDashboardCtrl from './dashboard.controller.js';
import MemberDashboardResource from './dashboard.resource.js';

import ReadyEventModule from './event/event-preview.module.js';
import EmptyEventListModule from '../common/empty-events-list/empty-events-list.module.js';

let MemberDashboardModule = angular.module('knbitFrontend.Events.Members.Dashboard', [
  ReadyEventModule.name,
  EmptyEventListModule.name
])
  .controller('MemberDashboardCtrl', MemberDashboardCtrl)
  .service('MemberDashboardResource', MemberDashboardResource);

export default MemberDashboardModule;
