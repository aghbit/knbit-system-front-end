'use strict';

class MemberDashboardResource {

  constructor($http, membersConfig) {
    this.$http = $http;
    this.membersConfig = membersConfig;
  }

  getAll(memberId) {
    let endpointUrl = `${this.membersConfig.dashboardUrl}/${memberId}`;

    return this.$http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }

}

MemberDashboardResource.$inject = ['$http', 'membersConfig'];

export default MemberDashboardResource;

