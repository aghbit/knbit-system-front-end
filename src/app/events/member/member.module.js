'use strict';

import MemberRoutes from './member.routes.js';

import MemberCommonModule from './common/common.module.js';
import MemberDashboardModule from './dashboard/dashboard.module.js';
import MemberSurveyModule from './survey/survey.module.js';
import MemberTermChoosingModule from './term-choosing/term-choosing.module.js';

var MemberModule = angular.module('knbitFrontend.Events.Members', [
  MemberCommonModule.name,
  MemberDashboardModule.name,
  MemberSurveyModule.name,
  MemberTermChoosingModule.name
])
  .config(MemberRoutes);

export default MemberModule;
