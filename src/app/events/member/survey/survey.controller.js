'use strict';

class MemberSurveyCtrl {

  constructor(MemberSurveyResource, AuthService) {
    this.MemberSurveyResource = MemberSurveyResource;
    this.AuthService = AuthService;
    this.emptyListMessage = false;

    this.initialize();
  }

  initialize() {
    let userId = this.AuthService.getUserId();
    this.MemberSurveyResource
      .surveyEvents(userId)
      .then(events => this.emptyListMessageOrContent(events))
      .then(events => this.events = events);
  }

  // TODO: move to common component
  emptyListMessageOrContent(events) {
    this.emptyListMessage = events.length === 0;
    return events;
  }

  vote(eventId, vote) {
    return this.MemberSurveyResource.vote(eventId, vote);
  }
}

MemberSurveyCtrl.$inject = ['MemberSurveyResource', 'AuthService'];

export default MemberSurveyCtrl;
