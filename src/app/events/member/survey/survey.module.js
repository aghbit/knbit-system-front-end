'use strict';

import EventUnderSurveyModule from './event/event-under-survey.module.js';
import MemberSurveyResource from './survey.resource.js';
import MemberSurveyCtrl from './survey.controller.js';

import EmptyEventsListModule from '../common/empty-events-list/empty-events-list.module.js';

const MemberSurveyModule = angular.module('knbitFrontend.Events.Members.Survey', [
  EventUnderSurveyModule.name,
  EmptyEventsListModule.name,
  'knbitFrontend.Events.Config',
  'AuthModule'
])
  .service('MemberSurveyResource', MemberSurveyResource)
  .controller('MemberSurveyCtrl', MemberSurveyCtrl);

export default MemberSurveyModule;
