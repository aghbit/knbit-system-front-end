/**
 * Created by novy on 17.10.15.
 */

'use strict';

EventUnderSurveyController.$inject = ['questionnaireModalService', 'AuthService', 'ToastingService', 'Messages'];

function EventUnderSurveyController(questionnaireModalService, authService, ToastingService, Messages) {
  const vm = this;
  vm.voteUp = voteUp;
  vm.voteDown = voteDown;

  function voteUp(event) {
    if (event.questions) {
      questionnaireModalService
        .showQuestionnaire(event)
        .then(answers => voteUpWithAnswers(event, answers));
    } else {
      voteUpWithAnswers(event, null);
    }
  }

  function voteUpWithAnswers(event, answers) {
    const memberId = authService.getUserId();
    const positiveVote = {
      type: 'POSITIVE',
      attendee: {memberId},
      answers: answers
    };

    vm.onVoteRequested({eventId: event.eventId, vote: positiveVote})
      .then(() => onVoteSuccess(event, 'POSITIVE'), onVoteFailure);
  }

  function voteDown(event) {
    const memberId = authService.getUserId();
    const negativeVote = {
      type: 'NEGATIVE',
      attendee: {memberId}
    };

    vm.onVoteRequested({eventId: event.eventId, vote: negativeVote})
      .then(() => onVoteSuccess(event, 'NEGATIVE'), onVoteFailure);
  }

  function onVoteSuccess(event, voteType) {
    ToastingService.showSuccessToast(Messages.MEMBERS_VOTE.success);
    event.voted = voteType;
  }

  function onVoteFailure() {
    ToastingService.showErrorToast(Messages.MEMBERS_VOTE.error);
  }
}

export default EventUnderSurveyController;
