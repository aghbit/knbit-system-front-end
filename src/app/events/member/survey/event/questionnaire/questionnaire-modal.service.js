/**
 * Created by novy on 17.10.15.
 */

'use strict';

QuestionnaireModalService.$inject = ['$modal'];

function QuestionnaireModalService($modal) {

  return {
    showQuestionnaire: showQuestionnaire
  };

  function showQuestionnaire(event) {
    var modalOptions = {
      animation: true,
      templateUrl: 'app/events/member/survey/event/questionnaire/questionnaire-modal.html',
      controller: 'MemberSurveyModalCtrl',
      controllerAs: 'modal',
      size: 'lg',
      resolve: {
        event: () => event
      }
    };

    return $modal
      .open(modalOptions)
      .result;
  }

}

export default QuestionnaireModalService;
