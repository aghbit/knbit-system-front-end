'use strict';

class MemberSurveyModalCtrl {

  constructor($modalInstance, event) {
    this.$modalInstance = $modalInstance;
    this.event = event;
    this.answers = new Map();
  }

  markSingleChoiceAnswer(question, answer) {
    this.answers.set(question, [answer]);
  }

  containsQuestion(question) {
    return this.answers.get(question);
  }

  updateQuestionAnswers(question, qanswers) {
    if (qanswers.length > 0) {
      this.answers.set(question, qanswers);
    } else {
      this.answers.delete(question);
    }
  }

  static alreadyContainsAnswer(qanswers, answer) {
    return qanswers.indexOf(answer) > -1;
  }

  // ehh...
  markMultipleChoiceAnswer(question, answer) {
    if (!this.containsQuestion(question)) {
      this.answers.set(question, [answer]);
      return;
    }

    var qanswers = this.answers.get(question);

    if (MemberSurveyModalCtrl.alreadyContainsAnswer(qanswers, answer)) {
      var index = qanswers.indexOf(answer);
      qanswers.splice(index, 1);
    } else {
      qanswers.push(answer);
    }

    this.updateQuestionAnswers(question, qanswers);
  }

  markTextChoiceAnswer(question, answer) {
    this.answers.set(question, [answer]);
  }

  allQuestionsAnswered() {
    return this.answers.size === this.event.questions.length;
  }

  flatMap(answersMap) {
    var answersList = [];
    answersMap.forEach(
      (value, key) => answersList.push({
        question: key,
        answers: value
      })
    );
    return answersList;
  }

  submit() {
    if (this.allQuestionsAnswered()) {
      var qanswers = this.flatMap(this.answers);
      this.$modalInstance.close(
        qanswers
      );
    }
  }

  cancel() {
    this.$modalInstance.dismiss('cancel');
  }

}

MemberSurveyModalCtrl.$inject = ['$modalInstance', 'event'];

export default MemberSurveyModalCtrl;
