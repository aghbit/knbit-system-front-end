/**
 * Created by novy on 17.10.15.
 */

'use strict';

import EventsCommonModule from '../../../eventmaster/common/common.module.js';
import AskEventMasterModule from '../../common/ask-event-master/ask-event-master.module.js';

import EventUnderSurveyDirective from './event-under-survey.directive.js';
import EventUnderSurveyController from './event-under-survey.controller.js';
import SurveyAlreadyVotedDirective from './already-voted-info.js';

import questionnaireModalService from './questionnaire/questionnaire-modal.service.js';
import SurveyModalController from './questionnaire/survey-modal.controller.js';
import AnswerController from './questionnaire/answer.controller.js';


const EventUnderSurveyModule = angular.module('knbitFrontend.Events.Members.Survey.Event', [
  AskEventMasterModule.name,
  // todo: not event master!
  'knbitFrontend.Events.EventMaster.Common',
  'AuthModule',
  'ui.bootstrap'
]);

EventUnderSurveyModule
  .factory('questionnaireModalService', questionnaireModalService)
  .controller('MemberSurveyModalCtrl', SurveyModalController)
  .controller('MemberAnswerCtrl', AnswerController)

  .controller('EventUnderSurveyController', EventUnderSurveyController)
  .directive('knbitEventsMemberEventUnderSurvey', EventUnderSurveyDirective)
  .directive('knbitEventsMemberSurveyAlreadyVoted', SurveyAlreadyVotedDirective);

export default EventUnderSurveyModule;

