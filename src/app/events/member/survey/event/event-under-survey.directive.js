/**
 * Created by novy on 17.10.15.
 */

'use strict';

function EventUnderSurveyDirective() {

  return {
    restrict: 'E',
    scope: {
      event: '=',
      onVoteRequested: '&'
    },
    controller: 'EventUnderSurveyController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/member/survey/event/event-under-survey.tpl.html'
  }
}

export default EventUnderSurveyDirective;
