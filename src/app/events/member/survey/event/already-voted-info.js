/**
 * Created by novy on 17.10.15.
 */

'use strict';

const template = `
  <knbit-events-member-checkmark ng-if="vm.vote == 'POSITIVE'"
                                 text="You want it"
                                 type="POSITIVE">
  </knbit-events-member-checkmark>

  <knbit-events-member-checkmark ng-if="vm.vote == 'NEGATIVE'"
                                 text="Not interested"
                                 type="NEGATIVE">
  </knbit-events-member-checkmark>`;

function AlreadyVotedInfoDirective() {

  return {
    restrict: 'E',
    scope: {
      vote: '@'
    },
    template: template,
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };
}

export default AlreadyVotedInfoDirective;
