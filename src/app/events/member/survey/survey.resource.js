'use strict';

class MemberSurveyResource {

  constructor($http, membersConfig) {
    this.$http = $http;
    this.membersConfig = membersConfig;
  }

  surveyEvents(userId) {
    let endpointUrl = this.membersConfig.surveyEventsUrl + `/${userId}`;

    return this.$http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }

  vote(eventId, payload) {
    let endpointUrl = this.membersConfig.surveyVotesUrlPrefix + `/${eventId}` + this.membersConfig.surveyVotesUrlSuffix;

    return this.$http({
      method: 'POST',
      url: endpointUrl,
      data: payload
    })
      .then(response => response.data);
  }

}

MemberSurveyResource.$inject = ['$http', 'membersConfig'];

export default MemberSurveyResource;
