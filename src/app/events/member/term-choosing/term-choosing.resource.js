'use strict';

class MemberTermChoosingResource {

  constructor($http, membersConfig) {
    this.$http = $http;
    this.membersConfig = membersConfig;
  }

  choosingTermEvents(userId) {
    let endpointUrl = this.membersConfig.choosingTermUrl + `/${userId}`;

    return this.$http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }

}

MemberTermChoosingResource.$inject = ['$http', 'membersConfig'];

export default MemberTermChoosingResource;
