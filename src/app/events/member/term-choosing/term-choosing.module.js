'use strict';

import MemberTermChoosingResource from './term-choosing.resource.js';
import MemberTermChoosingCtrl from './term-choosing.controller.js';

import EventUnderTermChoosingModule from './event/event-under-term-choosing.module.js';
import EmptyEventsListModule from '../common/empty-events-list/empty-events-list.module.js';

const MemberTermChoosingModule = angular.module('knbitFrontend.Events.Members.TermChoosing', [
  EventUnderTermChoosingModule.name,
  EmptyEventsListModule.name,
  'knbitFrontend.Events.Config',
  'AuthModule'
])
  .service('MemberTermChoosingResource', MemberTermChoosingResource)
  .controller('MemberTermChoosingCtrl', MemberTermChoosingCtrl);

export default MemberTermChoosingModule;
