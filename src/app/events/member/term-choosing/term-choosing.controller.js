'use strict';

class MemberTermChoosingCtrl {

  constructor(MemberTermChoosingResource, AuthService) {
    this.resource = MemberTermChoosingResource;
    this.authService = AuthService;
    this.emptyListMessage = false;

    this.initialize();
  }

  // TODO: move to common component
  emptyListMessageOrContent(events) {
    this.emptyListMessage = events.length === 0;
    return events;
  }

  initialize() {
    let userId = this.authService.getUserId();
    this.userId = userId;
    this.resource
      .choosingTermEvents(userId)
      .then(events => this.emptyListMessageOrContent(events))
      .then(data => this.events = data);
  }

}

MemberTermChoosingCtrl.$inject = ['MemberTermChoosingResource', 'AuthService'];

export default MemberTermChoosingCtrl;
