'use strict';

import enrollmentService from './enrollment.service.js';
import EventUnderTermChoosingController from './event-under-term-choosing.controller.js';
import EventUnderTermChoosingDirective from './event-under-term-choosing.directive.js';

import TermPreviewUnderTermChoosingModule from './term-preview/term-preview.module.js';

const EventUnderTermChoosingModule = angular.module('knbitFrontend.Events.Members.TermChoosing.Event', [
  TermPreviewUnderTermChoosingModule.name
])
  .factory('enrollmentService', enrollmentService)
  .controller('EventUnderTermChoosingController', EventUnderTermChoosingController)
  .directive('knbitEventsMemberEventUnderTermChoosing', EventUnderTermChoosingDirective);


export default EventUnderTermChoosingModule;

