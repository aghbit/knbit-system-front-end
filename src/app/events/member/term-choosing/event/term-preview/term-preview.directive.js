'use strict';

function TermPreviewUnderTermChoosingDirective() {

  return {
    restrict: 'E',
    scope: {
      term: '=',
      allowedToEnroll: '&',
      enroll: '&',
      disenroll: '&'
    },
    controller: 'TermPreviewUnderTermChoosingCtrl',
    controllerAs: 'tm',
    bindToController: true,
    templateUrl: 'app/events/member/term-choosing/event/term-preview/term-preview.tpl.html'
  }

}

export default TermPreviewUnderTermChoosingDirective;
