'use strict';

function TermPreviewUnderTermChoosingCtrl() {
  const vm = this;

  vm.alreadyEnrolled = alreadyEnrolled;
  vm.canEnroll = canEnroll;
  vm.enrollAndUpdate = enrollAndUpdate;
  vm.disenrollAndUpdate = disenrollAndUpdate;

  vm.computeEndDate = computeEndDate;

  function computeEndDate() {
    return vm.term.date + vm.term.duration * 60000;
  }

  function alreadyEnrolled() {
    return vm.term.enrolled;
  }

  function participantLimitExceeded() {
    return vm.term.participantsEnrolled >= vm.term.participantsLimit;
  }

  function canEnroll() {
    return vm.allowedToEnroll() && !alreadyEnrolled() && !participantLimitExceeded();
  }

  function enrollAndUpdate() {
    vm.enroll({termId: vm.term.termId})
      .then(toggleEnrolledAndChangeParticipantNumberBy(1));
  }

  function disenrollAndUpdate() {
    vm.disenroll({termId: vm.term.termId})
      .then(toggleEnrolledAndChangeParticipantNumberBy(-1));
  }

  function toggleEnrolledAndChangeParticipantNumberBy(num) {
    return () => {
      vm.term.enrolled = !vm.term.enrolled;
      vm.term.participantsEnrolled += num;
    };
  }
}


export default TermPreviewUnderTermChoosingCtrl;
