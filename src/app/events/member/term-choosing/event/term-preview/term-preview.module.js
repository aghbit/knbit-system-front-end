'use strict';

import TermPreviewUnderTermChoosingCtrl from './term-preview.controller.js';
import TermPreviewUnderTermChoosingDirective from './term-preview.directive.js';

const TermPreviewUnderTermChoosingModule = angular.module('knbitFrontend.Events.Members.TermChoosing.Event.TermPreview', [])
  .controller('TermPreviewUnderTermChoosingCtrl', TermPreviewUnderTermChoosingCtrl)
  .directive('knbitEventsMemberTermPreviewUnderTermChoosing', TermPreviewUnderTermChoosingDirective);


export default TermPreviewUnderTermChoosingModule;

