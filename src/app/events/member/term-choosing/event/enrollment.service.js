/**
 * Created by novy on 18.10.15.
 */

'use strict';

EnrollmentService.$inject = ['$http', 'enrollmentConfig'];

function EnrollmentService($http, enrollmentConfig) {

  return {
    enrollFor: enrollFor,
    disenrollFrom: disenrollFrom
  };

  function enrollFor(eventId, termId, memberId) {
    const url = `${enrollmentConfig.urlPrefix}/${eventId}/terms/${termId}/enroll/${memberId}`;
    const method = 'PUT';

    return $http({method, url})
      .then(response => response.data);
  }

  function disenrollFrom(eventId, termId, memberId) {
    const url = `${enrollmentConfig.urlPrefix}/${eventId}/terms/${termId}/disenroll/${memberId}`;
    const method = 'DELETE';

    return $http({method, url})
      .then(response => response.data);
  }
}

export default EnrollmentService;
