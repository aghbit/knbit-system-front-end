'use strict';

function EventUnderTermChoosingDirective() {

  return {
    restrict: 'E',
    scope: {
      memberId: '@',
      event: '='
    },
    controller: 'EventUnderTermChoosingController',
    controllerAs: 'ed',
    bindToController: true,
    templateUrl: 'app/events/member/term-choosing/event/event-under-term-choosing.tpl.html'
  };
}

export default EventUnderTermChoosingDirective;
