/**
 * Created by novy on 18.10.15.
 */

'use strict';

EventUnderTermChoosingController.$inject = ['enrollmentService'];

function EventUnderTermChoosingController(enrollmentService) {
  const vm = this;

  vm.enrollCallbackFor = enrollCallbackFor;
  vm.disenrollCallbackFor = disenrollCallbackFor;
  vm.isEnrolled = () => angular.isDefined(vm.event.chosenTerm);

  function enrollCallbackFor(termId) {
    return enrollmentService
      .enrollFor(vm.event.eventId, termId, vm.memberId)
      .then(markGivenTermAsCurrentMemberChoice(termId));
  }

  function disenrollCallbackFor(termId) {
    return enrollmentService
      .disenrollFrom(vm.event.eventId, termId, vm.memberId)
      .then(clearMemberChoice);
  }

  function markGivenTermAsCurrentMemberChoice(termId) {
    return resolvedPromiseValue => {
      vm.event.chosenTerm = termId;
      return resolvedPromiseValue;
    };
  }

  function clearMemberChoice(resolvedPromiseValue) {
    delete vm.event.chosenTerm;
    return resolvedPromiseValue;
  }
}

export default EventUnderTermChoosingController;
