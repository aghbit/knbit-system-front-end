'use strict';

var EventMasterRoutes = function ($stateProvider) {

  $stateProvider
    .state('events.eventmaster', {
      url: '/eventmaster',
      views: {
        'events-header': {
          templateUrl: 'app/events/eventmaster/common/header/header.html',
          controller: 'EventsHeaderCtrl',
          controllerAs: 'header'
        },
        'events-content': {
          templateUrl: 'app/events/eventmaster/kanban-board/board/view/kanban-board.html',
          controller: 'KanbanBoardCtrl',
          controllerAs: 'kanban'
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })
    .state('events.eventmaster.backlog', {
      url: '/:eventId/backlog',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/kanban-board/backlog/backlog-preview.html',
          controller: 'BacklogPreviewController',
          controllerAs: "backlogPreview"
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    }).state('events.eventmaster.survey', {
      url: '/:eventId/survey',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/survey-preview/survey-interest-preview.html',
          controller: 'SurveyInterestPreviewController',
          controllerAs: "vm"
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })
    .state('events.eventmaster.choosing-term', {
      url: '/:eventId/choosing-term',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/choosing-term-preview/choosing-term-preview.html',
          controller: 'ChoosingTermPreviewController',
          controllerAs: 'vm'
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })
    .state('events.eventmaster.enrollment', {
      url: '/:eventId/enrollment',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/kanban-board/enrollment/enrollment-preview/enrollment-preview.html',
          controller: 'EnrollmentPreviewController',
          controllerAs: 'vm'
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })
    .state('events.eventmaster.ready', {
      url: '/:eventId/ready-event',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/kanban-board/ready-event/ready-event-preview/ready-event-preview.html',
          controller: 'ReadyEventPreviewController',
          controllerAs: 'rd'
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })
    .state('events.eventmaster.archive', {
      url: '/archive',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/archive-events/partials/archive-events.html'
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })
    .state('events.eventmaster.announcement', {
      url: '/announcement',
      abstract: true
    })
    .state('events.eventmaster.announcement.configuration', {
      url: '/configuration',
      views: {
        'events-content@events': {
          templateUrl: 'app/events/eventmaster/announcement/configuration/announcement-configuration.html',
          controller: 'AnnouncementConfigurationController',
          controllerAs: 'ctrl'
        }
      },
      data: {
        permission: 'EVENTS_MANAGEMENT'
      }
    })

};

EventMasterRoutes.$inject = ['$stateProvider'];

export default EventMasterRoutes;
