'use strict';

import StartingChoosingTermModule from './start-choosing/start-choosing.module.js';
import ChoosingTermPreviewModule from './choosing-term-preview/choosing-term-preview.module.js'

let ChoosingTermModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm', [
  StartingChoosingTermModule.name,
  ChoosingTermPreviewModule.name
]);

export default ChoosingTermModule;
