/**
 * Created by novy on 06.09.15.
 */

'use strict';

function NewTermFormDirective() {

  return {
    restrict: 'E',
    scope: {
      term: '='
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/common/new-term-form.tpl.html'
  };
}

export default NewTermFormDirective;
