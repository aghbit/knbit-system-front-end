/**
 * Created by novy on 06.09.15.
 */

'use strict';

import NewTermFormDirective from './new-term-form.directive.js';

let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.Common';

let ChoosingTermCommonModule = angular.module(moduleName, []);

ChoosingTermCommonModule
  .directive('knbitEventsChoosingTermNewTermForm', NewTermFormDirective);

export default ChoosingTermCommonModule;
