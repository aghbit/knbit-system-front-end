'use strict';

ChoosingTermController.$inject = ['$scope', '$rootScope', '$modal', 'ChoosingTermResource', 'ToastingService', 'Messages'];

function ChoosingTermController($scope, $rootScope, $modal, ChoosingTermResource, ToastingService, Messages) {

  listenForModalOpenRequest();

  function listenForModalOpenRequest() {
    let destroyListener = $rootScope.$on('kanban-board:choosing-term',
      (event, id, state) => handleStartChoosingTermFor(id, state)
    );

    $scope.$on('$destroy', () => destroyListener());
  }

  function handleStartChoosingTermFor(eventDomainId, state) {
    openModal()
      .waitForResult
      .then(data => tryStartChoosingTerm(eventDomainId, state, data));
  }

  function openModal() {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/start-choosing/modal/choosing-term.tpl.html',
      controller: 'ChoosingTermModalController',
      controllerAs: 'vm',
      size: 'lg'
    };

    let modalResult = $modal
      .open(modalOptions)
      .result;

    return {
      waitForResult: modalResult
    };
  }

  function tryStartChoosingTerm(id, state, data) {
    return ChoosingTermResource
      .startChoosingTerm(id, state, data)
      .then(handleChoosingTermStarted, handleError);
  }

  function handleChoosingTermStarted() {
    ToastingService.showSuccessToast(Messages.EVENT_STATE.success);
    $rootScope.$emit('kanban-board:refresh');
  }

  function handleError() {
    ToastingService.showErrorToast(Messages.EVENT_STATE.error);
  }
}

export default ChoosingTermController;
