'use strict';

class ChoosingTermResource {

  constructor($resource, choosingTermConfig) {
    let allowedMethods = {
      'start': {
        method: 'POST'
      }
    };

    let endpointUrl = choosingTermConfig.urlPrefix + '/:id' + choosingTermConfig.urlSuffix + '?fromState=:state';
    this.surveyInterestResource = $resource(endpointUrl, {id: '@id', state: '@state'}, allowedMethods);
  }

  startChoosingTerm(eventBacklogId, state, data) {
    return this.surveyInterestResource.start({
      id: eventBacklogId,
      state: state
    }, data)
      .$promise;
  }

}

ChoosingTermResource.$inject = ['$resource', 'choosingTermConfig'];
export default ChoosingTermResource;
