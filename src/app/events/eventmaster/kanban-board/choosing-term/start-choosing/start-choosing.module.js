'use strict';

import TermListController from './modal/terms/term-list.controller.js';
import TermListDirective from './modal/terms/term-list.directive.js';

import ChoosingTermCommonModule from '../common/choosing-term-common.module.js';
import ChoosingTermController from './choosing-term.controller.js';
import ChoosingTermModalController from './modal/choosing-term-modal.controller.js';
import ChoosingTermResource from './choosing-term.resource.js';

let StartChoosingModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.StartingChoosingPopup', [
  'knbitFrontend.Events.Config',
  'ui.bootstrap',
  ChoosingTermCommonModule.name
])
  .controller('ChoosingTermStartChoosingTermListController', TermListController)
  .directive('knbitEventsChoosingTermStartChoosingTermList', TermListDirective)
  .controller('ChoosingTermController', ChoosingTermController)
  .controller('ChoosingTermModalController', ChoosingTermModalController)
  .service('ChoosingTermResource', ChoosingTermResource);

export default StartChoosingModule;
