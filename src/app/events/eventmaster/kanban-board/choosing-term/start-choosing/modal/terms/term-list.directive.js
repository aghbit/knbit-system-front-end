/**
 * Created by novy on 06.09.15.
 */

'use strict';

function TermListDirective() {

  return {
    restrict: 'E',
    scope: {
      terms: '='
    },
    controller: 'ChoosingTermStartChoosingTermListController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/start-choosing/modal/terms/term-list.tpl.html'
  };
}

export default TermListDirective;
