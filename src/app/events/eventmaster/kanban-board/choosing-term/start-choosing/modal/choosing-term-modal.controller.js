'use strict';

ChoosingTermModalController.$inject = ['$modalInstance'];

function ChoosingTermModalController($modalInstance) {

  let vm = this;
  vm.terms = [];
  vm.submit = submit;
  vm.cancel = cancel;

  function submit() {
    let data = {
      terms: vm.terms.filter(term => term.roomBooking === false),
      termProposals: vm.terms.filter(term => term.roomBooking === true)
    };

    $modalInstance.close(data);
  }

  function cancel() {
    $modalInstance.dismiss('cancel');
  }
}

export default ChoosingTermModalController;
