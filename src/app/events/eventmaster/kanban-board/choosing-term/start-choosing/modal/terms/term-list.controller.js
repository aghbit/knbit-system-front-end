/**
 * Created by novy on 06.09.15.
 */

'use strict';

function ChoosingTermStartChoosingTermListController() {

  let vm = this;
  vm.addTerm = addTerm;
  vm.removeTerm = removeTerm;
  vm.addTerm();

  function addTerm() {
    vm.terms.push({
      date: null,
      capacity: null,
      duration: null,
      roomBooking: false
    });
  }

  function removeTerm(index) {
    vm.terms.splice(index, 1);
  }
}

export default ChoosingTermStartChoosingTermListController;
