/**
 * Created by novy on 01.09.15.
 */

'use strict';

ChoosingTermController.$inject =
  ['$stateParams', 'termService', 'reservationService', 'choosingTermPreviewService'];

function ChoosingTermController($stateParams, termService, reservationService, choosingTermPreviewService) {

  let vm = this;
  let refreshView = () => getEventData($stateParams.eventId);
  vm.getEventData = getEventData;
  vm.removeTerm = removeTerm;
  vm.cancelReservation = cancelReservation;
  vm.addNewTerm = addNewTerm;
  refreshView();

  function getEventData(eventId) {
    choosingTermPreviewService
      .getEventData(eventId)
      .then(eventData => vm.event = eventData);
  }

  function removeTerm(term) {
    termService
      .removeTerm($stateParams.eventId, term.termId)
      .then(refreshView);
  }

  function cancelReservation(reservation) {
    reservationService
      .cancelReservation($stateParams.eventId, reservation)
      .then(refreshView);
  }

  function addNewTerm(term) {
    let successfullyAdded;

    if (term.roomBooking) {
      successfullyAdded = reservationService.createReservation($stateParams.eventId, term);
    } else {
      successfullyAdded = termService.addNewTerm($stateParams.eventId, term);
    }

    successfullyAdded
      .then(refreshView);
  }
}

export default ChoosingTermController;
