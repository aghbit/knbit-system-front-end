/**
 * Created by novy on 01.09.15.
 */

'use strict';

TermService.$inject = ['$http', 'choosingTermPreviewConfig'];

function TermService($http, choosingTermPreviewConfig) {

  return {
    addNewTerm: addNewTerm,
    removeTerm: removeTerm
  };

  function addNewTerm(eventId, newTerm) {
    let endpoint_url = choosingTermPreviewConfig.termsUrlPrefix + `/${eventId}` + choosingTermPreviewConfig.termsUrlSuffix;

    return $http({
      method: 'POST',
      url: endpoint_url,
      data: newTerm
    });
  }

  function removeTerm(eventId, termId) {
    let endpoint_url = choosingTermPreviewConfig.termsUrlPrefix + `/${eventId}` + choosingTermPreviewConfig.termsUrlSuffix + `/${termId}`;

    return $http({
      method: 'DELETE',
      url: endpoint_url
    });
  }

}

export default TermService;
