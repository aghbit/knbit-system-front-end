/**
 * Created by novy on 07.09.15.
 */

'use strict';

ChoosingTermPreviewService.$inject = ['$resource', 'choosingTermPreviewConfig'];

function ChoosingTermPreviewService($resource, choosingTermPreviewConfig) {

  let eventUnderChoosingTermResource = newResource();

  return {
    getEventData: getEventData
  };

  function newResource() {
    let allowedMethods = {
      'get': {method: 'GET'}
    };
    let endpointUrl = choosingTermPreviewConfig.urlPrefix + '/:id' + choosingTermPreviewConfig.urlSuffix;

    return $resource(endpointUrl, {id: '@id'}, allowedMethods);
  }

  function getEventData(eventId) {
    return eventUnderChoosingTermResource
      .get({id: eventId})
      .$promise;
  }
}

export default ChoosingTermPreviewService;
