/**
 * Created by novy on 07.09.15.
 */

'use strict';

ReservationService.$inject = ['$resource', 'choosingTermPreviewConfig'];

function ReservationService($resource, choosingTermPreviewConfig) {

  let reservationsResource = createReservationsResource();
  let identifiedReservationResource = createIdentifiedReservationResource();

  return {
    createReservation: createReservation,
    cancelReservation: cancelReservation
  };

  function createReservationsResource() {
    let allowedMethods = {
      'post': {method: 'POST'}
    };
    let endpointUrl = choosingTermPreviewConfig.reservationsUrlPrefix + '/:id' + choosingTermPreviewConfig.reservationsUrlSuffix;

    return $resource(endpointUrl, {id: '@id'}, allowedMethods);
  }

  function createIdentifiedReservationResource() {
    let allowedMethods = {
      'delete': {method: 'DELETE'}
    };
    let endpointUrl = choosingTermPreviewConfig.reservationsUrlPrefix +
      '/:eventId' + choosingTermPreviewConfig.reservationsUrlSuffix + '/:reservationId';

    return $resource(
      endpointUrl,
      {eventId: '@eventId', reservationId: '@reservationId'},
      allowedMethods
    );
  }

  function createReservation(eventId, reservation) {
    return reservationsResource
      .post({id: eventId}, reservation)
      .$promise;
  }

  function cancelReservation(eventId, reservation) {
    let pathVariables = {
      eventId: eventId,
      reservationId: reservation.reservationId
    };

    return identifiedReservationResource
      .delete(pathVariables)
      .$promise;
  }
}

export default ReservationService;
