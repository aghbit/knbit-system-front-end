/**
 * Created by novy on 07.09.15.
 */

'use strict';

import TermService from './term.service.js';
import ReservationService from './reservation.service.js';
import ChoosingTermPreviewService from './choosing-term-preview.service.js';

let moduleName = 'knbitFrontend.Events.KanbanBoard.ChoosingTerm.TermPreview.Services';

let ChoosingTermPreviewServicesModule = angular.module(moduleName, []);

ChoosingTermPreviewServicesModule
  .factory('termService', TermService)
  .factory('reservationService', ReservationService)
  .factory('choosingTermPreviewService', ChoosingTermPreviewService);


export default ChoosingTermPreviewServicesModule;
