/**
 * Created by novy on 09.10.15.
 */

'use strict';

const noTermsWarningTemplate = `
  <div class="alert alert-warning" role="alert">
    <p>There are no terms and reservations assigned for this event.</p>

    <p>You should add at least one term before going further.</p>

    <br/>

    <p ng-click="vm.newTermCallback()">
      <a>
        <strong>Click here to do it!</strong>
      </a>
    </p>
  </div>`;

function NoTermsWarning() {

  return {
    restrict: 'E',
    scope: {
      newTermCallback: '&'
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    template: noTermsWarningTemplate
  };

}

export default NoTermsWarning;
