/**
 * Created by novy on 05.09.15.
 */

'use strict';

function TermDirective() {

  return {
    restrict: 'E',
    scope: {
      term: '=',
      displayCapacity: '&'
    },
    templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/choosing-term-preview/terms/term/term.tpl.html',
    controller: 'ChoosingTermPreviewTermController',
    controllerAs: 'vm',
    bindToController: true
  };
}

export default TermDirective;
