/**
 * Created by novy on 01.09.15.
 */

'use strict';

import NewTermModalController from './new-term-modal/new-term-modal.controller.js';

import TermsModalService from './terms-modal.service.js';
import TermsController from './terms.controller.js';
import TermsDirective from './terms.directive.js';

import TermListDirective from './term-list/term-list.directive.js';
import TermDirective from './term/term.directive.js';
import TermDirectiveController from './term/term.controller.js';

import NotTermWarning from './no-terms-warning.directive.js';

let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.TermPreview.Terms';

let TermsModule = angular.module(moduleName, []);

TermsModule
  .factory('termsModalService', TermsModalService)
  .controller('ChoosingTermPreviewTermsController', TermsController)
  .directive('knbitEventsChoosingTermPreviewTerms', TermsDirective)
  .directive('knbitEventsChoosingTermPreviewTermList', TermListDirective)
  .controller('NewTermModalController', NewTermModalController)
  .controller('ChoosingTermPreviewTermController', TermDirectiveController)
  .directive('knbitEventsChoosingTermPreviewTerm', TermDirective)
  .directive('knbitEventsChoosingTermPreviewNoTermsWarning', NotTermWarning);

export default TermsModule;
