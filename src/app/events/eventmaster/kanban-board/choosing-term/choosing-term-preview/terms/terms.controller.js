/**
 * Created by novy on 05.09.15.
 */

'use strict';

ChoosingTermPreviewTermsController.$inject = ['termsModalService'];

function ChoosingTermPreviewTermsController(termsModalService) {

  let vm = this;
  vm.removeTerm = removeTerm;
  vm.cancelReservation = cancelReservation;
  vm.addNewTerm = addNewTerm;
  vm.noTermOrReservationAssigned = noTermOrReservationAssigned;

  function removeTerm(termId) {
    waitForTermRemovalConfirmation(termId)
      .then(callTermRemoveCallback);
  }

  function waitForTermRemovalConfirmation(termId) {
    return termsModalService
      .confirmTermRemoval()
      .then(() => termId);
  }

  function callTermRemoveCallback(term) {
    vm.removeTermCallback({term: term});
  }

  function cancelReservation(reservation) {
    waitForReservationCancelConfirmation(reservation)
      .then(callCancelReservationCallback);
  }

  function waitForReservationCancelConfirmation(reservation) {
    return termsModalService
      .confirmReservationCancellation()
      .then(() => reservation);
  }

  function callCancelReservationCallback(reservation) {
    vm.cancelReservationCallback({reservation: reservation});
  }

  function addNewTerm() {
    waitForNewTerm()
      .then(callNewTermCallback);
  }

  function waitForNewTerm() {
    return termsModalService.openNewTermModal();
  }

  function callNewTermCallback(newTerm) {
    vm.newTermCallback({newTerm: newTerm});
  }

  function noTermOrReservationAssigned() {
    return vm.terms && vm.terms.length === 0 && vm.reservations && vm.reservations.length === 0;
  }
}

export default ChoosingTermPreviewTermsController;
