/**
 * Created by novy on 01.09.15.
 */

'use strict';

function TermsDirective() {

  return {
    restrict: 'E',
    scope: {
      terms: '=',
      reservations: '=',
      removeTermCallback: '&',
      cancelReservationCallback: '&',
      newTermCallback: '&'
    },
    controller: 'ChoosingTermPreviewTermsController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/choosing-term-preview/terms/terms.tpl.html'
  };
}

export default TermsDirective;
