/**
 * Created by novy on 06.09.15.
 */

'use strict';

NewTermModalController.$inject = ['$modalInstance'];

function NewTermModalController($modalInstance) {

  let vm = this;
  vm.term = newTerm();
  vm.submit = submit;
  vm.cancel = cancel;

  function newTerm() {
    return {
      date: null,
      capacity: null,
      duration: null,
      roomBooking: false
    };
  }

  function cancel() {
    $modalInstance.dismiss();
  }

  function submit() {
    $modalInstance.close(vm.term);
  }
}

export default NewTermModalController;
