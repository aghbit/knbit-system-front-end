/**
 * Created by novy on 05.09.15.
 */

'use strict';

TermController.$inject = ['$filter'];

function TermController($filter) {

  let dateFilter = $filter('date');
  let dateFormat = 'mediumDate';
  let timeFormat = 'hh:mm a';

  let vm = this;
  vm.formattedDate = () => dateFilter(vm.term.date, dateFormat);
  vm.startTime = () => dateFilter(vm.term.date, timeFormat);
  vm.endTime = () => dateFilter(vm.term.date + toMillis(vm.term.duration), timeFormat);
  vm.shouldDisplayLocation = shouldDisplayLocation;
  vm.shouldDisplayCapacity = shouldDisplayCapacity;

  function toMillis(durationInMinutes) {
    return durationInMinutes * 60000;
  }

  function shouldDisplayLocation() {
    return angular.isDefined(vm.term.location);
  }

  function shouldDisplayCapacity() {
    return vm.displayCapacity();
  }
}

export default TermController;
