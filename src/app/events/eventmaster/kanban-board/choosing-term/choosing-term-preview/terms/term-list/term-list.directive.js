/**
 * Created by novy on 01.09.15.
 */

'use strict';

function TermListDirective() {

  return {
    restrict: 'E',
    scope: {
      terms: '=',
      caption: '@',
      color: '@',
      removeCallback: '&'
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/choosing-term-preview/terms/term-list/term-list.tpl.html'
  };
}

export default TermListDirective;
