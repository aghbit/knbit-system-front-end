/**
 * Created by novy on 06.09.15.
 */

'use strict';

TermsModalService.$inject = ['$modal', 'modalService'];

function TermsModalService($modal, modalService) {

  return {
    confirmTermRemoval: confirmTermRemoval,
    confirmReservationCancellation: confirmReservationCancellation,
    openNewTermModal: openNewTermModal
  };

  function confirmTermRemoval() {
    return confirmActionWithMessage('Are you sure you want to remove selected term?');
  }

  function confirmReservationCancellation() {
    return confirmActionWithMessage('Are you sure you want to cancel selected reservation?');
  }

  function confirmActionWithMessage(confirmationMessage) {
    let confirmationCaptions = {
      confirmationMessage: confirmationMessage
    };

    return modalService.confirm(confirmationCaptions);
  }

  function openNewTermModal() {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/kanban-board/choosing-term/choosing-term-preview/terms/new-term-modal/new-term-modal.tpl.html',
      controller: 'NewTermModalController',
      controllerAs: 'vm',
      size: 'lg'
    };

    return $modal
      .open(modalOptions)
      .result;
  }
}

export default TermsModalService;
