/**
 * Created by novy on 01.09.15.
 */

'use strict';

import ChoosingTermCommonModule from '../common/choosing-term-common.module.js';
import KanbanBoardCommonModule from '../../common/kanban-board-common.module.js';
import TermsModule from './terms/terms.module.js';
import ServicesModule from './services/choosing-term-preview-services.module.js';

import ChoosingTermPreviewController from './choosing-term-preview.controller.js';

let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.ChoosingTerm.TermPreview';

let ChoosingTermPreviewModule = angular.module(moduleName, [
  KanbanBoardCommonModule.name,
  ChoosingTermCommonModule.name,
  TermsModule.name,
  ServicesModule.name
]);

ChoosingTermPreviewModule
  .controller('ChoosingTermPreviewController', ChoosingTermPreviewController);

export default ChoosingTermPreviewModule;
