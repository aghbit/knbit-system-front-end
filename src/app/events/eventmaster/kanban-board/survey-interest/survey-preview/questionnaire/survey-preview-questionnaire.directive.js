/**
 * Created by novy on 30.08.15.
 */

'use strict';

function SurveyPreviewQuestionnaireDirective() {

  return {
    restrict: 'E',
    scope: {
      questions: '='
    },
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/survey-preview/questionnaire/survey-preview-questionnaire.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };
}

export default SurveyPreviewQuestionnaireDirective;
