/**
 * Created by novy on 30.08.15.
 */

'use strict';

function SurveyPreviewQuestionDirective() {

  return {
    restrict: 'E',
    scope: {
      question: '='
    },
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/survey-preview/questionnaire/question/survey-preview-question.tpl.html',
    controller: 'SurveyPreviewQuestionController',
    controllerAs: 'vm',
    bindToController: true
  };
}

export default SurveyPreviewQuestionDirective;
