/**
 * Created by novy on 30.08.15.
 */

'use strict';

import QuestionDirective from './survey-preview-question.directive.js';
import QuestionController from './survey-preview-question.controller.js';
import SingleOrMultipleAnswersDirective from './answers/survey-preview-single-multiple-answers.directive.js';
import TextAnswersDirective from './answers/survey-preview-text-answers.directive.js';

let moduleName =
  'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.SurveyInterestPreview.Questionnaire.Question';

let SurveyPreviewQuestionModule = angular.module(moduleName, []);

SurveyPreviewQuestionModule
  .directive('knbitEventsSurveyPreviewQuestion', QuestionDirective)
  .controller('SurveyPreviewQuestionController', QuestionController)
  .directive('knbitEventsSurveyPreviewSingleMultipleAnswers', SingleOrMultipleAnswersDirective)
  .directive('knbitEventsSurveyPreviewTextAnswers', TextAnswersDirective);

export default SurveyPreviewQuestionModule;
