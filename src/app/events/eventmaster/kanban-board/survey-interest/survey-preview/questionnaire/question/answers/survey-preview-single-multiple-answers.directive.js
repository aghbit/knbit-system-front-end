/**
 * Created by novy on 30.08.15.
 */

'use strict';

function SurveyPreviewSingleOrMultipleAnswers() {

  return {
    restrict: 'E',
    scope: {
      answers: '='
    },
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/survey-preview/questionnaire/question/answers/survey-preview-single-multiple-answers.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };
}

export default SurveyPreviewSingleOrMultipleAnswers;
