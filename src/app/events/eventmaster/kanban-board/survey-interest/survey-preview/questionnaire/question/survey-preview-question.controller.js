/**
 * Created by novy on 30.08.15.
 */

'use strict';

function SurveyPreviewQuestionController() {

  let vm = this;
  vm.showAnswers = false;
  vm.textQuestion = textQuestion;
  vm.singleOrMultipleChoiceQuestion = singleOrMultipleChoiceQuestion;

  function textQuestion() {
    return vm.question.questionType === 'TEXT';
  }

  function singleOrMultipleChoiceQuestion() {
    return (vm.question.questionType === 'SINGLE_CHOICE') || (vm.question.questionType === 'MULTIPLE_CHOICE');
  }
}

export default SurveyPreviewQuestionController;
