/**
 * Created by novy on 30.08.15.
 */

'use strict';

import QuestionModule from './question/survey-preview-question.module.js';
import QuestionnaireDirective from './survey-preview-questionnaire.directive.js';

let moduleName =
  'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.SurveyInterestPreview.Questionnaire';

let SurveyPreviewQuestionnaireModule = angular.module(moduleName, [
  QuestionModule.name
]);

SurveyPreviewQuestionnaireModule
  .directive('knbitEventsSurveyPreviewQuestionnaire', QuestionnaireDirective);

export default SurveyPreviewQuestionnaireModule;
