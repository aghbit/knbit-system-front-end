/**
 * Created by novy on 05.06.15.
 */

'use strict';

class SurveyInterestPreviewService {

  constructor($resource, surveyInterestPreviewConfig) {
    let allowedMethods = {
      'get': {method: 'GET'}
    };
    let endpointUrl = surveyInterestPreviewConfig.urlPrefix + '/:id' + surveyInterestPreviewConfig.urlSuffix;

    this.surveyInterestResource = $resource(endpointUrl, {id: '@id'}, allowedMethods);
  }

  surveyBy(eventId) {
    return this.surveyInterestResource
      .get({id: eventId})
      .$promise;
  }

}

SurveyInterestPreviewService.$inject = ['$resource', 'surveyInterestPreviewConfig'];

export default SurveyInterestPreviewService;
