/**
 * Created by novy on 05.06.15.
 */

'use strict';

import KanbanBoardCommonModule from '../../common/kanban-board-common.module.js';
import SurveyPreviewQuestionnaireModule from './questionnaire/survey-preview-questionnaire.module.js';

import SurveyInterestPreviewController from './survey-interest-preview.controller.js';
import SurveyInterestPreviewService from './survey-interest-preview.service.js';

import SurveyStatisticsDirective from './survey-statistics/survey-statistics.directive.js';

var SurveyInterestPreviewModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.SurveyInterestPreview', [
  'knbitFrontend.Events.Config',
  SurveyPreviewQuestionnaireModule.name,
  KanbanBoardCommonModule.name
])
  .controller('SurveyInterestPreviewController', SurveyInterestPreviewController)
  .service('SurveyInterestPreviewService', SurveyInterestPreviewService)
  .directive('knbitEventSurveyPreviewStatistics', SurveyStatisticsDirective);


export default SurveyInterestPreviewModule;
