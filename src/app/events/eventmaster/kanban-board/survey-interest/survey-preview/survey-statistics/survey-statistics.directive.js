/**
 * Created by novy on 30.08.15.
 */

'use strict';

function SurveyStatisticsDirective() {

  return {
    restrict: 'E',
    scope: {
      votedUp: '@',
      votedDown: '@'
    },
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/survey-preview/survey-statistics/survey-statistics.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };
}

export default SurveyStatisticsDirective;
