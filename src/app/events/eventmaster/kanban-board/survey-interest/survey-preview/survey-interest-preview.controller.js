/**
 * Created by novy on 05.06.15.
 */

'use strict';

SurveyInterestPreviewController.$inject = ['$stateParams', 'SurveyInterestPreviewService'];

function SurveyInterestPreviewController($stateParams, SurveyInterestPreviewService) {

  let vm = this;
  vm.surveyHasQuestionnaire = surveyHasQuestionnaire;
  getSurveyData($stateParams.eventId);

  function getSurveyData(eventId) {
    SurveyInterestPreviewService
      .surveyBy(eventId)
      .then(survey => vm.survey = survey);
  }

  function surveyHasQuestionnaire() {
    return angular.isDefined(vm.survey) && angular.isDefined(vm.survey.questions);
  }
}

export default SurveyInterestPreviewController;
