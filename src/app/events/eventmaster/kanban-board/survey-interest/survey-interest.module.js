'use strict';

import StartingSurveyPreviewModule from './starting-survey/starting-survey.module.js';
import SurveyInterestPreviewModule from './survey-preview/survey-interest-preview.module.js';

var SurveyInterestModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest', [
  StartingSurveyPreviewModule.name,
  SurveyInterestPreviewModule.name
]);

export default SurveyInterestModule;
