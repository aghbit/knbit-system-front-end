'use strict';

class SurveyInterestResource {

  constructor($resource, eventConfig) {
    let allowedMethods = {
      'create': {method: 'POST'}
    };
    let endpointUrl = eventConfig.eventUrl + '/:id' + '/survey';

    this.surveyInterestResource = $resource(endpointUrl, {id: '@id'}, allowedMethods);
  }

  startInterestSurveying(eventBacklogId, survey) {
    return this.surveyInterestResource
      .create({id: eventBacklogId}, survey)
      .$promise;
  }
}

SurveyInterestResource.$inject = ['$resource', 'eventConfig'];

export default SurveyInterestResource;
