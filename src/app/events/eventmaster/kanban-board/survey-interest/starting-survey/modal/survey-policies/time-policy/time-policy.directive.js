/**
 * Created by novy on 29.08.15.
 */

'use strict';

function TimePolicy() {

  return {
    restrict: 'E',
    scope: {
      endOfSurveying: '=',
      onEndOfSurveyingChanged: '='
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/survey-policies/time-policy/time-policy.tpl.html'
  };
}

export default TimePolicy;
