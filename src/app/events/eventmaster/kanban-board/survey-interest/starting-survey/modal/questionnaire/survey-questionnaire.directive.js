/**
 * Created by novy on 27.08.15.
 */

'use strict';

function SurveyQuestionnaireDirective() {

  return {
    restrict: 'E',
    scope: {
      questions: '='
    },
    controller: 'SurveyQuestionnaireController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/questionnaire/survey-questionnaire.tpl.html'
  };
}

export default SurveyQuestionnaireDirective;
