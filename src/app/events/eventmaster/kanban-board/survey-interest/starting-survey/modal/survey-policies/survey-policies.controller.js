/**
 * Created by novy on 29.08.15.
 */

'use strict';

function SurveyPoliciesController() {

  let vm = this;
  let chosenValuePlaceholders = emptyPlaceholders();
  vm.policies = disabledPolicies();

  vm.clearEndOfSurveyingOrSetLastChosenDate = clearEndOfSurveyingOrSetLastChosenDate;
  vm.onEndOfSurveyingChanged = onEndOfSurveyingChanged;

  vm.clearMinimalInterestThresholdOrSetLastInterest = clearMinimalInterestThresholdOrSetLastInterest;
  vm.onMinimalInterestThresholdChanged = onMinimalInterestThresholdChanged;

  function emptyPlaceholders() {
    return {
      endOfSurveying: null,
      minimalInterestThreshold: null
    };
  }

  function disabledPolicies() {
    return {
      timeBasedEndingPolicy: false,
      memberVotesNotificationPolicy: false
    };
  }

  function onEndOfSurveyingChanged(newEndOfSurveying) {
    chosenValuePlaceholders.endOfSurveying = newEndOfSurveying;
    vm.endOfSurveying = newEndOfSurveyingDateOrNull(newEndOfSurveying);
  }

  function newEndOfSurveyingDateOrNull(newEndOfSurveyingDate) {
    return vm.policies.timeBasedEndingPolicy ? newEndOfSurveyingDate : null;
  }

  function clearEndOfSurveyingOrSetLastChosenDate() {
    vm.endOfSurveying = vm.policies.timeBasedEndingPolicy ?
      chosenValuePlaceholders.endOfSurveying : null;
  }

  function onMinimalInterestThresholdChanged(newInterestThreshold) {
    chosenValuePlaceholders.minimalInterestThreshold = newInterestThreshold;
    vm.minimalInterestThreshold = newMinimalInterestThresholdOrNull(newInterestThreshold);
  }

  function newMinimalInterestThresholdOrNull(newInterestThreshold) {
    return vm.policies.memberVotesNotificationPolicy ? newInterestThreshold : null;
  }

  function clearMinimalInterestThresholdOrSetLastInterest() {
    vm.minimalInterestThreshold = vm.policies.memberVotesNotificationPolicy ?
      chosenValuePlaceholders.minimalInterestThreshold : null;
  }
}

export default SurveyPoliciesController;
