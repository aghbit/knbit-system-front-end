'use strict';

function SurveyQuestionDirective() {

  return {
    restrict: 'E',
    scope: {
      questionNumber: '@',
      question: '='
    },
    controller: 'SurveyQuestionController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/questionnaire/question/survey-question.tpl.html'
  };
}

export default SurveyQuestionDirective;
