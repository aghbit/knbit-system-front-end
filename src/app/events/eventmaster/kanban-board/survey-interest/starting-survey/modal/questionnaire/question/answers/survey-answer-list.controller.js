'use strict';

function SurveyAnswerListController() {

  let vm = this;
  vm.addEmptyAnswer = addEmptyAnswer;
  vm.removeAnswer = removeAnswer;

  function addEmptyAnswer() {
    vm.answers.push('');
  }

  function removeAnswer(answerIndex) {
    vm.answers.splice(answerIndex, 1);
  }
}

export default SurveyAnswerListController;
