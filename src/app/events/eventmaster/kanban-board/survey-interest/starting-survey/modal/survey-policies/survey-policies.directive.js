/**
 * Created by novy on 29.08.15.
 */

'use strict';

function SurveyPoliciesDirective() {

  return {
    restrict: 'E',
    scope: {
      endOfSurveying: '=',
      minimalInterestThreshold: '='
    },
    controller: 'SurveyPoliciesController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/survey-policies/survey-policies.tpl.html'
  };
}

export default SurveyPoliciesDirective;
