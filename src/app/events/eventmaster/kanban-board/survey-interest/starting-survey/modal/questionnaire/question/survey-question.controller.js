'use strict';

function SurveyQuestionController() {

  let vm = this;
  vm.questionTypes = [
    {name: 'Single choice answer', value: 'SINGLE_CHOICE'},
    {name: 'Multiple choice answer', value: 'MULTIPLE_CHOICE'},
    {name: 'Text choice answer', value: 'TEXT'}
  ];
  vm.isQuestionChoosable = isQuestionChoosable;
  vm.onQuestionTypeChanged = onQuestionTypeChanged;


  function isQuestionChoosable() {
    return (vm.question.type === 'SINGLE_CHOICE') || (vm.question.type === 'MULTIPLE_CHOICE');
  }

  function onQuestionTypeChanged() {
    if (vm.question.type === 'TEXT') {
      vm.question.answers = [];
    } else {
      vm.question.answers = oldAnswersOrEmpty(vm.question.answers);
    }
  }

  function oldAnswersOrEmpty(answers) {
    return answers.length > 2 ? answers : ['', ''];
  }
}

export default SurveyQuestionController;
