/**
 * Created by novy on 27.08.15.
 */

'use strict';

function SurveyQuestionnaireController() {

  let vm = this;
  vm.addNewQuestion = addNewQuestion;
  vm.removeQuestion = removeQuestion;

  function addNewQuestion() {
    vm.questions.push({
      title: '',
      description: '',
      type: 'SINGLE_CHOICE',
      answers: [
        '', ''
      ]
    });
  }

  function removeQuestion(questionIndex) {
    vm.questions.splice(questionIndex, 1);
  }
}

export default SurveyQuestionnaireController;
