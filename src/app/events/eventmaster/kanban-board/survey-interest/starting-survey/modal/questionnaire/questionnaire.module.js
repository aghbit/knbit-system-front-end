/**
 * Created by novy on 29.08.15.
 */

'use strict';

import SurveyAnswerDirective from './question/answers/answer/survey-answer.directive.js';
import SurveyAnswerListDirective from './question/answers/survey-answer-list.directive.js';
import SurveyAnswerListController from './question/answers/survey-answer-list.controller.js';
import SurveyQuestionDirective from './question/survey-question.directive.js';
import SurveyQuestionController from './question/survey-question.controller.js';
import SurveyQuestionnaireDirective from './survey-questionnaire.directive.js';
import SurveyQuestionnaireController from './survey-questionnaire.controller.js';

let moduleName =
  'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.Questionnaire';

let QuestionnaireModule = angular.module(moduleName, []);

QuestionnaireModule
  .directive('knbitEventsSurveyAnswer', SurveyAnswerDirective)
  .directive('knbitEventsSurveyAnswerList', SurveyAnswerListDirective)
  .controller('SurveyAnswerListController', SurveyAnswerListController)
  .directive('knbitEventsSurveyQuestion', SurveyQuestionDirective)
  .controller('SurveyQuestionController', SurveyQuestionController)
  .directive('knbitEventsSurveyQuestionnaire', SurveyQuestionnaireDirective)
  .controller('SurveyQuestionnaireController', SurveyQuestionnaireController);

export default QuestionnaireModule;
