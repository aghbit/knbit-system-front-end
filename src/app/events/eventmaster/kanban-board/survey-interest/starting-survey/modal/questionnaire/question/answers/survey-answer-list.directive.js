'use strict';

function SurveyAnswerListDirective() {

  return {
    restrict: 'E',
    scope: {
      answers: '='
    },
    controller: 'SurveyAnswerListController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/questionnaire/question/answers/answer-list.tpl.html'
  };
}

export default SurveyAnswerListDirective;
