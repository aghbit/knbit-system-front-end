/**
 * Created by novy on 29.08.15.
 */

'use strict';

import SurveyPoliciesDirective from './survey-policies.directive.js';
import SurveyPoliciesController from './survey-policies.controller.js';
import MemberVotesPolicyDirective from './member-votes-policy/member-votes-policy.directive.js';
import TimePolicyDirective from './time-policy/time-policy.directive.js';

let moduleName =
  'knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup.SurveyPolicies';

let SurveyPoliciesModule = angular.module(moduleName, []);

SurveyPoliciesModule
  .directive('knbitEventsSurveyPolicies', SurveyPoliciesDirective)
  .controller('SurveyPoliciesController', SurveyPoliciesController)
  .directive('knbitEventsSurveyMemberVotesPolicy', MemberVotesPolicyDirective)
  .directive('knbitEventsSurveyTimePolicy', TimePolicyDirective);

export default SurveyPoliciesModule;

