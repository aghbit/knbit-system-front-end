'use strict';

function SurveyAnswerDirective() {

  return {
    restrict: 'E',
    scope: {
      answer: '='
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/questionnaire/question/answers/answer/survey-answer.tpl.html'
  };
}

export default SurveyAnswerDirective;
