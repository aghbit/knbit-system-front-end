'use strict';

SurveyInterestModalController.$inject = ['$modalInstance'];

function SurveyInterestModalController($modalInstance) {

  let vm = this;
  vm.survey = {
    endOfSurveying: null,
    minimalInterestThreshold: null,
    questions: []
  };
  vm.submit = submit;
  vm.cancel = cancel;

  function submit() {
    $modalInstance.close(vm.survey);
  }

  function cancel() {
    $modalInstance.dismiss('cancel');
  }
}

export default SurveyInterestModalController;
