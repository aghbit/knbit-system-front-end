/**
 * Created by novy on 29.08.15.
 */

'use strict';

function MemberVotesPolicy() {

  return {
    restrict: 'E',
    scope: {
      minimalInterestThreshold: '=',
      onMinimalInterestThresholdChanged: '='
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/survey-policies/member-votes-policy/member-votes-policy.tpl.html'
  };
}

export default MemberVotesPolicy;
