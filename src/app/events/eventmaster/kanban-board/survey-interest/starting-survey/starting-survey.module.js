'use strict';

import SurveyPoliciesModule from './modal/survey-policies/survey-policies.module.js';
import QuestionnaireModule from './modal/questionnaire/questionnaire.module.js';

import SurveyInterestController from './survey-interest.controller.js';
import SurveyInterestModalController from './modal/survey-interest-modal.controller.js';
import SurveyInterestResource from './survey-interest.resource.js';

let StartingSurveyModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.SurveyInterest.StartingInterestPopup', [
  'knbitFrontend.Events.Config',
  'ui.bootstrap',
  SurveyPoliciesModule.name,
  QuestionnaireModule.name
]);

StartingSurveyModule
  .service('SurveyInterestResource', SurveyInterestResource)
  .controller('SurveyInterestController', SurveyInterestController)
  .controller('SurveyInterestModalController', SurveyInterestModalController);

export default StartingSurveyModule;
