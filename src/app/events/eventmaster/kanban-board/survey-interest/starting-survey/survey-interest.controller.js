'use strict';

SurveyInterestController.$inject =
  ['$rootScope', '$scope', '$modal', 'SurveyInterestResource', 'ToastingService', 'Messages'];

function SurveyInterestController($rootScope, $scope, $modal, SurveyInterestResource, ToastingService, Messages) {

  listenForModalOpenRequest();

  function listenForModalOpenRequest() {
    let deregistrationHandler = $rootScope.$on(
      'kanban-board:survey-interest',
      (event, eventId) => handleStartInterestSurveyingFor(eventId)
    );

    $scope.$on('$destroy', () => deregistrationHandler());
  }

  function handleStartInterestSurveyingFor(eventId) {
    openModal()
      .waitForSurveyToBeAvailable
      .then(survey => tryToStartInterestSurveying(eventId, survey));
  }

  function openModal() {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/kanban-board/survey-interest/starting-survey/modal/survey-interest.tpl.html',
      controller: 'SurveyInterestModalController',
      controllerAs: 'vm',
      size: 'lg'
    };

    let modalResult = $modal
      .open(modalOptions)
      .result;

    return {
      waitForSurveyToBeAvailable: modalResult
    };
  }

  function tryToStartInterestSurveying(eventBacklogId, survey) {
    SurveyInterestResource
      .startInterestSurveying(eventBacklogId, survey)
      .then(handleSurveyingStarted, handleError);
  }

  function handleSurveyingStarted() {
    ToastingService.showSuccessToast(Messages.EVENT_STATE.success);
    $rootScope.$emit('kanban-board:refresh');
  }

  function handleError() {
    ToastingService.showErrorToast(Messages.EVENT_STATE.error);
  }
}

export default SurveyInterestController;
