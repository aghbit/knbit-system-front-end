'use strict';

import CommonModule from './common/kanban-board-common.module.js';
import BoardModule from './board/board.module.js';
import BacklogModule from './backlog/backlog.module';
import SurveyInterestModule from './survey-interest/survey-interest.module.js';
import ChoosingTermModule from './choosing-term/choosing-term.module.js';
import EnrollmentModule from './enrollment/enrollment.module.js';
import ReadyEventModule from './ready-event/ready-event.module.js';

let KanbanBoardModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard', [
  CommonModule.name,
  BoardModule.name,
  BacklogModule.name,
  SurveyInterestModule.name,
  ChoosingTermModule.name,
  EnrollmentModule.name,
  ReadyEventModule.name
]);

export default KanbanBoardModule;
