/**
 * Created by novy on 01.09.15.
 */

'use strict';

import EventDetailsDirective from './event-details/event-details.directive.js';


let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.Common';

let CommonModule = angular.module(moduleName, []);

CommonModule
  .directive('knbitEventsEventDetails', EventDetailsDirective);

export default CommonModule;


