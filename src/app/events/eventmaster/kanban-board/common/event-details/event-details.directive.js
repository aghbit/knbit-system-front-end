/**
 * Created by novy on 30.08.15.
 */

'use strict';

function EventDetailsDirective() {

  return {
    restrict: 'E',
    scope: {
      eventName: '@',
      eventDescription: '@',
      eventType: '@',
      imageUrl: '@',
      section: '='
    },
    templateUrl: 'app/events/eventmaster/kanban-board/common/event-details/event-details.tpl.html',
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true
  };
}

export default EventDetailsDirective;
