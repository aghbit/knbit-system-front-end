/**
 * Created by novy on 02.12.15.
 */

'use strict';

KanbanBoardItemController.$inject = ['kanbanBoardItemModalService'];

function KanbanBoardItemController(kanbanBoardItemModalService) {

  const vm = this;
  vm.showDetails = vm.onShowDetails;

  vm.cancel = cancelEvent;
  function cancelEvent() {
    kanbanBoardItemModalService
      .openCancellationModalFor(vm.event)
      .then(vm.onCancel);
  }
}


export default KanbanBoardItemController;
