/**
 * Created by novy on 02.12.15.
 */

'use strict';

KanbanBoardItemModalService.$inject = ['modalService'];

function KanbanBoardItemModalService(modalService) {

  return {openCancellationModalFor};

  function openCancellationModalFor(event) {
    const modalOptions = {
      confirmationTitle: 'Confirm cancellation',
      confirmationMessage: `Are you sure you want to cancel event "${event.name}"?`
    };

    return modalService.confirm(modalOptions);
  }
}

export default KanbanBoardItemModalService;
