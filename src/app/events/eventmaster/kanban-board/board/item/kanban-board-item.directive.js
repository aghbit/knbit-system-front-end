/**
 * Created by novy on 02.12.15.
 */

'use strict';

function KanbanBoardItemDirective() {

  return {
    restrict: 'E',
    replace: true,
    scope: {
      event: '=item',
      onShowDetails: '&',
      onCancel: '&'
    },
    templateUrl: 'app/events/eventmaster/kanban-board/board/item/kanban-board-item.tpl.html',
    controller: 'KanbanBoardItemController',
    controllerAs: 'item',
    bindToController: true
  };
}

export default KanbanBoardItemDirective;
