'use strict';

var KanbanBoardDragg = function () {
  return {
    link: function ($scope, element, attrs) {

      var dragData = '';
      $scope.$watch(attrs.kanbanBoardDragg, val => dragData = val);

      element.bind('dragstart', (event) => {
        event.originalEvent.dataTransfer.setData('event', dragData);
        $scope.$emit('kanban-board-dragg-start', dragData);
      });
    }
  };
};

export default KanbanBoardDragg;
