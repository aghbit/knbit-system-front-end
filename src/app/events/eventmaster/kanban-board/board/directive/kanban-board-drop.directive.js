'use strict';

var KanbanBoardDrop = function () {
  return {
    link: function ($scope, element, attrs) {

      var reachableClass = 'kanban-board-column-reachable';
      var unreachableClass = 'kanban-board-column-unreachable';

      var columnId = '';
      $scope.$watch(attrs.kanbanBoardDrop, val => columnId = val);

      //  Prevent the default behavior. This has to be called in order for drop to work
      function cancel(event) {
        if (event.preventDefault) {
          event.preventDefault();
        }

        if (event.stopPropigation) {
          event.stopPropigation();
        }
        return false;
      }

      function findClass(reachableStates, boardState) {
        if (reachableStates.indexOf(boardState) > -1) {
          return reachableClass;
        } else {
          return unreachableClass;
        }
      }

      function clearClassDecorations() {
        element.removeClass(reachableClass);
        element.removeClass(unreachableClass);
      }

      function extractData() {
        return $scope.event;
      }

      function createEventId(id) {
        return 'kanban-board:' + id.replace(/_/g, '-').toLowerCase();
      }

      function isReachable(data) {
        var currentState = data.eventStatus;
        var reachableStates = data.reachableStatus;
        return currentState !== columnId && reachableStates.indexOf(columnId) > -1;
      }

      function sendAppropriateEvent() {
        var data = extractData();
        if (isReachable(data)) {
          $scope.$emit(createEventId(columnId), data.eventId, data.eventStatus);
        }
      }

      $scope.$on('kanban-board-dragg-start', function (event, data) {
        $scope.$parent.event = data;
      });

      element.bind('dragover', function (event) {
        cancel(event);
        event.originalEvent.dataTransfer.dropEffect = 'move';
        var decorationClass = findClass(extractData().reachableStatus, columnId);
        element.addClass(decorationClass);
      });

      element.bind('drop', function (event) {
        cancel(event);
        clearClassDecorations(element);
        sendAppropriateEvent();
      });

      element.bind('dragleave', function () {
        clearClassDecorations(element);
      });

    }
  };
};

export default KanbanBoardDrop;
