'use strict';

import humanize from 'underscore.string/humanize';

class KanbanBoardCtrl {

  constructor($rootScope, kanbanBoardService) {
    this.kanbanBoardService = kanbanBoardService;
    this.$rootScope = $rootScope;
    this.columns = ['BACKLOG', 'SURVEY_INTEREST', 'CHOOSING_TERM', 'ENROLLMENT', 'READY'];
    this.boardData = {};

    this.initialize();
    this.refreshBoard();
  }

  refreshBoard() {
    this.kanbanBoardService.board()
      .then(response => this.boardData = response);
  }

  initialize() {
    this.$rootScope.$on('kanban-board:refresh', () => {
      this.refreshBoard();
    });
  }

  columnDescription(column) {
    return humanize(column);
  }

  moveToDetailedView(eventId, kanbanColumn) {
    this.kanbanBoardService.navigateToDetails(eventId, kanbanColumn);
  }

  cancelEvent(eventId, kanbanColumn) {
    this.kanbanBoardService
      .cancelEvent(eventId, kanbanColumn)
      .then(() => this.refreshBoard());
  }
}

KanbanBoardCtrl.$inject = ['$rootScope', 'kanbanBoardService'];

export default KanbanBoardCtrl;
