/**
 * Created by novy on 22.05.15.
 */

'use strict';

import KanbanBoardService from './service/kanban-board.service.js';
import DetailedViewNavigator from './service/detailed-view-navigator.js';
import CancellationService from './service/cancellation-service';
import KanbanBoardCtrl from './controller/kanban-board.controller.js';
import KanbanBoardDragg from './directive/kanban-board-dragg.directive.js';
import KanbanBoardDrop from './directive/kanban-board-drop.directive.js';
import KanbanBoardItem from './item/kanban-board-item.directive';
import KanbanBoardItemController from './item/kanban-board-item.controller';
import kanbanBoardItemModalService from './item/kanban-board-item-modal.service';
import kanbanBoardQuery from './service/kanban-board.query';
import eventStatesMapping from './service/event-states-mapping.constant';

var BoardModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.Board', [
  'knbitFrontend.Events.EventMaster.Common.Modal',
  'knbitFrontend.Events.Config'
])
  .service('kanbanBoardService', KanbanBoardService)
  .factory('detailedViewNavigator', DetailedViewNavigator)
  .controller('KanbanBoardCtrl', KanbanBoardCtrl)
  .directive('kanbanBoardDragg', KanbanBoardDragg)
  .directive('kanbanBoardDrop', KanbanBoardDrop)
  .directive('kanbanBoardItem', KanbanBoardItem)
  .controller('KanbanBoardItemController', KanbanBoardItemController)
  .factory('kanbanBoardItemModalService', kanbanBoardItemModalService)
  .factory('cancellationService', CancellationService)
  .factory('kanbanBoardQuery', kanbanBoardQuery)
  .constant('eventStatesMapping', eventStatesMapping);

export default BoardModule;
