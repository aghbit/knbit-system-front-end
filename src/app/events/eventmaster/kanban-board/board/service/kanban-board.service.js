'use strict';

class KanbanBoardService {

  constructor(kanbanBoardQuery, detailedViewNavigator, cancellationService) {
    this.kanbanBoardQuery = kanbanBoardQuery;
    this.detailedViewNavigator = detailedViewNavigator;
    this.cancellationService = cancellationService;
  }

  board() {
    return this.kanbanBoardQuery.boardData();
  }

  navigateToDetails(eventId, kanbanColumn) {
    this.detailedViewNavigator.navigateToDetails(eventId, kanbanColumn);
  }

  cancelEvent(eventId, kanbanColumn) {
    return this.cancellationService.cancelEvent(eventId, kanbanColumn);
  }
}

KanbanBoardService.$inject = ['kanbanBoardQuery', 'detailedViewNavigator', 'cancellationService'];

export default KanbanBoardService;
