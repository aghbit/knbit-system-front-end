/**
 * Created by novy on 17.10.15.
 */

'use strict';

DetailedViewNavigator.$inject = ['$state', 'eventStatesMapping'];

function DetailedViewNavigator($state, eventStatesMapping) {

  return {navigateToDetails};

  function navigateToDetails(eventId, kanbanColumn) {
    const newState = `events.eventmaster.${eventStatesMapping[kanbanColumn]}`;
    $state.go(newState, {eventId});
  }
}

export default DetailedViewNavigator;
