/**
 * Created by novy on 02.12.15.
 */

const eventStatesMapping = {
  BACKLOG: 'backlog',
  SURVEY_INTEREST: 'survey',
  CHOOSING_TERM: 'choosing-term',
  ENROLLMENT: 'enrollment',
  READY: 'ready'
};

export default eventStatesMapping;
