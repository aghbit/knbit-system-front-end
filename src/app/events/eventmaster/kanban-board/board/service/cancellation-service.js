/**
 * Created by novy on 02.12.15.
 */

'use strict';

CancellationService.$inject = ['$http', 'eventConfig', 'eventStatesMapping'];

function CancellationService($http, eventConfig, eventStatesMapping) {

  return {cancelEvent};

  function cancelEvent(eventId, status) {
    const endpointUrl = `${eventConfig.eventUrl}${eventId}/${eventStatesMapping[status]}`;

    return $http({method: 'DELETE', url: endpointUrl})
      .then(response => response.data);
  }
}

export default CancellationService;
