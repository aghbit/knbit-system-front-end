/**
 * Created by novy on 02.12.15.
 */

KanbanBoardQuery.$inject = ['$http', 'kanbanBoardConfig'];

function KanbanBoardQuery($http, kanbanBoardConfig) {

  return {boardData};

  function boardData() {
    return $http({method: 'GET', url: kanbanBoardConfig.kanbanBoardUrl})
      .then(response => response.data);
  }
}

export default KanbanBoardQuery;
