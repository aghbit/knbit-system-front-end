'use strict';

import StartingReadyEventModule from './start-ready-event/ready-event.module.js';
import ReadyEventPreviewModule from './ready-event-preview/ready-event-preview.module.js';

const ReadyEventModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.ReadyEvent', [
  StartingReadyEventModule.name,
  ReadyEventPreviewModule.name,
  'knbitFrontend.Events.Config'
]);

export default ReadyEventModule;

