'use strict';

ReadyEventPreviewController.$inject = ['$stateParams', 'ReadyEventPreviewResource'];

function ReadyEventPreviewController($stateParams, ReadyEventPreviewResource) {

  let vm = this;
  initialize();

  function initialize() {
    ReadyEventPreviewResource
      .getEventData($stateParams.eventId)
      .then(data => vm.event = data);
  }

}

export default ReadyEventPreviewController;
