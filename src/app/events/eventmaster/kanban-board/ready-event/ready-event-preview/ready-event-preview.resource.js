'use strict';

function ReadyEventPreviewResource($http, readyEventPreviewConfig) {

  return {
    getEventData: getEventData
  };

  function getEventData(eventId) {
    let endpointUrl = readyEventPreviewConfig.urlPrefix + `/${eventId}` + readyEventPreviewConfig.urlSuffix;

    return $http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }

}

ReadyEventPreviewResource.$inject = ['$http', 'readyEventPreviewConfig'];

export default ReadyEventPreviewResource;
