'use strict';

import ReadyEventPreviewController from './ready-event-preview.controller.js';
import ReadyEventPreviewResource from './ready-event-preview.resource.js';

let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.ReadyEvent.Preview';

let ReadyEventPreviewModule = angular.module(moduleName, []);

ReadyEventPreviewModule
  .service('ReadyEventPreviewResource', ReadyEventPreviewResource)
  .controller('ReadyEventPreviewController', ReadyEventPreviewController);

export default ReadyEventPreviewModule;
