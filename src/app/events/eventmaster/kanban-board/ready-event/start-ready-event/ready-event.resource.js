/**
 * Created by novy on 24.10.15.
 */

'use strict';

ReadyEventResource.$inject = ['$http', 'readyEventConfig'];

function ReadyEventResource($http, readyEventConfig) {

  return {markAsReady: markAsReady};

  function markAsReady(eventId) {
    const endpointUrl = `${readyEventConfig.urlPrefix}/${eventId}/${readyEventConfig.urlSuffix}`;

    return $http({
      method: 'POST',
      url: endpointUrl
    })
      .then(response => response.data);
  }
}

export default ReadyEventResource;
