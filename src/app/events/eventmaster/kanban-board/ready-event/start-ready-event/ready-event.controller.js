/**
 * Created by novy on 24.10.15.
 */

'use strict';

ReadyEventController.$inject = ['$scope', '$rootScope', 'readyEventResource'];

function ReadyEventController($scope, $rootScope, readyEventResource) {

  listenForTransitionRequest();

  function listenForTransitionRequest() {
    const destroyListener = $rootScope.$on('kanban-board:ready',
      (event, id) => markAsReady(id)
    );

    $scope.$on('$destroy', () => destroyListener());
  }

  function markAsReady(eventId) {
    readyEventResource
      .markAsReady(eventId)
      .then(requestBoardRefresh);
  }

  function requestBoardRefresh() {
    $rootScope.$emit('kanban-board:refresh');
  }
}

export default ReadyEventController;
