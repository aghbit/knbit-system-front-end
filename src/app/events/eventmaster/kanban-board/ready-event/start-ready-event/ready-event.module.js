/**
 * Created by novy on 24.10.15.
 */

'use strict';

import ReadyEventController from './ready-event.controller.js';
import readyEventResource from './ready-event.resource.js';

const StartingReadyEventModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.ReadyEvent.Starting', [
  'knbitFrontend.Events.Config'
]);

StartingReadyEventModule
  .controller('ReadyEventController', ReadyEventController)
  .factory('readyEventResource', readyEventResource);

export default StartingReadyEventModule;

