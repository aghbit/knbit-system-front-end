'use strict';

import StartingEnrollmentModule from './starting-enrollment/starting-enrollment.module.js';
import EnrollmentPreviewModule from './enrollment-preview/enrollment-preview.module.js';

var EnrollmentModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.Enrollment', [
  StartingEnrollmentModule.name,
  EnrollmentPreviewModule.name
]);

export default EnrollmentModule;
