'use strict';

class TermsResource {

  constructor($resource, $q, choosingTermConfig) {
    let allowedMethods = {
      'start': {
        method: 'GET'
      }
    };

    let endpointUrl = choosingTermConfig.urlPrefix + '/:id' + choosingTermConfig.urlSuffix;
    this.termResource = $resource(endpointUrl, {id: '@id'}, allowedMethods);
    this.$q = $q;
  }

  getTerms(eventId) {
    return this.termResource
      .start({id: eventId})
      .$promise
      .then(response => response.terms);
  }
}

TermsResource.$inject = ['$resource', '$q', 'choosingTermConfig'];

export default TermsResource;
