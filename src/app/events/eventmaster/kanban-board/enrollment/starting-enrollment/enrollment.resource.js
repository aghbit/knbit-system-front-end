'use strict';

class EnrollmentResource {

  constructor($resource, enrollmentConfig) {
    let allowedMethods = {
      'start': {
        method: 'POST'
      }
    };

    let endpointUrl = enrollmentConfig.urlPrefix + '/:id' + enrollmentConfig.urlSuffix + '?fromState=:state';
    this.enrollmentResource = $resource(endpointUrl, {id: '@id', state: '@state'}, allowedMethods);
  }

  startEnrollment(eventId, state, details) {
    return this.enrollmentResource
      .start({
        id: eventId,
        state: state
      }, details)
      .$promise;
  }
}

EnrollmentResource.$inject = ['$resource', 'enrollmentConfig'];

export default EnrollmentResource;
