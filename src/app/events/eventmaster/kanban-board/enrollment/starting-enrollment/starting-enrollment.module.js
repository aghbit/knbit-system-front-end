'use strict';

import EnrollmentController from './enrollment.controller.js';
import EnrollmentResource from './enrollment.resource.js';
import EnrollmentModalController from './modal/enrollment-modal.controller.js';
import TermsResource from './terms.resource.js';

import EnrollmentTermDetailsModule from './modal/term-details/term-details.module.js';

let StartingEnrollmentModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.Enrollment.StartingEnrollmentPopup', [
  'knbitFrontend.Events.Config',
  'ui.bootstrap',
  EnrollmentTermDetailsModule.name
]);

StartingEnrollmentModule
  .service('EnrollmentResource', EnrollmentResource)
  .service('TermsResource', TermsResource)
  .controller('EnrollmentController', EnrollmentController)
  .controller('EnrollmentModalController', EnrollmentModalController);

export default StartingEnrollmentModule;




