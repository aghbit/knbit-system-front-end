'use strict';

EnrollmentController.$inject =
  ['$rootScope', '$scope', '$modal', 'EnrollmentResource', 'ToastingService', 'Messages'];

function EnrollmentController($rootScope, $scope, $modal, EnrollmentResource, ToastingService, Messages) {

  listenForModalOpenRequest();

  function listenForModalOpenRequest() {
    let deregistrationHandler = $rootScope.$on(
      'kanban-board:enrollment',
      (event, eventId, state) => handleStartEnrollmentFor(eventId, state)
    );

    $scope.$on('$destroy', () => deregistrationHandler());
  }

  function handleStartEnrollmentFor(eventId, state) {
    openModal(eventId)
      .waitForResponse
      .then(details => tryToStartEnrollment(eventId, state, details));
  }

  function openModal(eventId) {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/kanban-board/enrollment/starting-enrollment/modal/enrollment.tpl.html',
      controller: 'EnrollmentModalController',
      controllerAs: 'vm',
      size: 'md',
      resolve: {
        eventId: () => eventId
      }
    };

    let modalResult = $modal
      .open(modalOptions)
      .result;

    return {
      waitForResponse: modalResult
    };
  }

  function tryToStartEnrollment(eventId, state, details) {
    EnrollmentResource
      .startEnrollment(eventId, state, details)
      .then(handleSuccess, handleError);
  }

  function handleSuccess() {
    ToastingService.showSuccessToast(Messages.EVENT_STATE.success);
    $rootScope.$emit('kanban-board:refresh');
  }

  function handleError() {
    ToastingService.showErrorToast(Messages.EVENT_STATE.error);
  }
}

export default EnrollmentController;
