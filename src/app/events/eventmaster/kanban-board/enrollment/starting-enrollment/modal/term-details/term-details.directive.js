'use strict';

function TermEnrollmentDetailsDirective() {

  return {
    restrict: 'E',
    scope: {
      details: '='
    },
    controller: 'EnrollmentTermDetailsController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/enrollment/starting-enrollment/modal/term-details/term-details.tpl.html'
  };

}

export default TermEnrollmentDetailsDirective;
