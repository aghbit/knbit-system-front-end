'use strict';

EnrollmentModalController.$inject = ['$modalInstance', 'TermsResource', 'eventId'];

function EnrollmentModalController($modalInstance, TermsResource, eventId) {

  let vm = this;
  vm.details = [];
  vm.submit = submit;
  vm.cancel = cancel;
  initialize();

  function prepareForSave(terms) {
    return terms.map(term => {
      return {
        termId: term.termId,
        lecturers: term.lecturers,
        participantsLimit: term.participantsLimit
      }
    });
  }

  function submit() {
    $modalInstance.close(prepareForSave(vm.details));
  }

  function cancel() {
    $modalInstance.dismiss('cancel');
  }

  function initialize() {
    TermsResource.getTerms(eventId)
      .then(terms => vm.details = terms);
  }

}

export default EnrollmentModalController;
