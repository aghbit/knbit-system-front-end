'use strict';

EnrollmentTermDetailsController.$inject = ['LecturerResource'];

function EnrollmentTermDetailsController(LecturerResource) {

  let vm = this;
  vm.queryForLecturer = queryForLecturer;
  vm.lecturers = [];

  initialize();

  function lecturerMatches(lecturer, query) {
    return lecturer.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
  }

  function queryForLecturer(query) {
    return vm.lecturers.filter(lecturer => lecturerMatches(lecturer, query));
  }

  function initialize() {
    LecturerResource
      .getAll()
      .then(lecturers => vm.lecturers = lecturers);
  }

}

export default EnrollmentTermDetailsController;
