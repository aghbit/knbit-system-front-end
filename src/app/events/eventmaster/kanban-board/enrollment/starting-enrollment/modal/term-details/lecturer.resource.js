'use strict';

class LecturerResource {

  constructor($http, usersListUrl) {
    this.$http = $http;
    this.usersListUrl = usersListUrl;
  }

  static lecturerOf(user) {
    return {
      'name': `${user.firstName} ${user.lastName}`,
      'id'  : user.userId
    }
  }

  static lecturersOf(response) {
    return _.map(response.data.values, LecturerResource.lecturerOf);
  }

  getAll() {
    return this.$http({
      'method' : 'GET',
      'url'    : this.usersListUrl
    })
      .then(LecturerResource.lecturersOf);
  }
}

LecturerResource.$inject = ['$http', 'usersListUrl'];

export default LecturerResource;
