'use strict';

import TermEnrollmentDetailsDirective from './term-details.directive.js';
import EnrollmentTermDetailsController from './term-details.controller.js';
import LecturerResource from './lecturer.resource.js';

const moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.Enrollment.StartingEnrollmentPopup.EnrollmentTermDetailsModule';

let EnrollmentTermDetailsModule = angular.module(moduleName, [
  'knbitFrontend.Events.Config',
  'ui.bootstrap',
  'ngTagsInput'
]);

EnrollmentTermDetailsModule
  .service('LecturerResource', LecturerResource)
  .controller('EnrollmentTermDetailsController', EnrollmentTermDetailsController)
  .directive('knbitEventsTermEnrollmentDetails', TermEnrollmentDetailsDirective);

export default EnrollmentTermDetailsModule;




