'use strict';

function EnrollmentPreviewResource($http, enrollmentPreviewConfig) {

  return {
    getEventData: getEventData
  };

  function getEventData(eventId) {
    let endpointUrl = enrollmentPreviewConfig.urlPrefix + `/${eventId}` + enrollmentPreviewConfig.urlSuffix;

    return $http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }

}

EnrollmentPreviewResource.$inject = ['$http', 'enrollmentPreviewConfig'];

export default EnrollmentPreviewResource;
