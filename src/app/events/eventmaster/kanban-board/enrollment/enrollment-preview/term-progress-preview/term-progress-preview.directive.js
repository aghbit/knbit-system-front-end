'use strict';

function TermProgressPreviewDirective() {

  return {
    restrict: 'E',
    scope: {
      term: '='
    },
    controller: 'TermProgressPreviewController',
    controllerAs: 'tpp',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/enrollment/enrollment-preview/term-progress-preview/term-progress-preview.tpl.html'
  };

}

export default TermProgressPreviewDirective;
