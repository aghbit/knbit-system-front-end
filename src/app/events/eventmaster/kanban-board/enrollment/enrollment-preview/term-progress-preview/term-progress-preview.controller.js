'use strict';

function TermProgressPreviewController() {

  let tpp = this;
  tpp.progress = progress;
  tpp.progressBarStyle = progressBarStyle;
  tpp.getParticipantsAmount = getParticipantsAmount;
  tpp.getLecturers = getLecturers;

  function getParticipantsAmount() {
    return tpp.term.participants.length;
  }

  function progress() {
    return getParticipantsAmount() / tpp.term.limit * 100;
  }

  function progressBarStyle() {
    return {
      'width': progress() + '%'
    }
  }

  function getLecturers() {
    return tpp.term.lecturers
      .map(lecturer => lecturer.name)
      .join(', ');
  }

}

export default TermProgressPreviewController;
