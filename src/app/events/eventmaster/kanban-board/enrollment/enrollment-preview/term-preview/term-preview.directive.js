'use strict';

function TermPreviewDirective() {

  return {
    restrict: 'E',
    scope: {
      termDetails: '='
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/enrollment/enrollment-preview/term-preview/term-preview.tpl.html'
  };

}

export default TermPreviewDirective;
