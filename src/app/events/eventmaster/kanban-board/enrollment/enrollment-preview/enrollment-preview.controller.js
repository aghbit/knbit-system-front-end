'use strict';

EnrollmentPreviewController.$inject = ['$stateParams', 'EnrollmentPreviewResource'];

function EnrollmentPreviewController($stateParams, EnrollmentPreviewResource) {

  let vm = this;
  initialize();

  function initialize() {
    EnrollmentPreviewResource
      .getEventData($stateParams.eventId)
      .then(data => vm.event = data);
  }

}

export default EnrollmentPreviewController;
