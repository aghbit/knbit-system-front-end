'use strict';

function AttendeePreviewDirective() {

  return {
    restrict: 'E',
    scope: {
      attendee: '='
    },
    controller: angular.noop,
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/kanban-board/enrollment/enrollment-preview/attendee-preview/attendee-preview.tpl.html'
  };

}

export default AttendeePreviewDirective;
