'use strict';

import EnrollmentPreviewController from './enrollment-preview.controller.js';
import EnrollmentPreviewResource from './enrollment-preview.resource.js';
import TermPreviewDirective from './term-preview/term-preview.directive.js';
import AttendeePreviewDirective from './attendee-preview/attendee-preview.directive.js';
import TermProgressPreviewDirective from './term-progress-preview/term-progress-preview.directive.js';
import TermProgressPreviewController from './term-progress-preview/term-progress-preview.controller.js';

let moduleName = 'knbitFrontend.Events.EventMaster.KanbanBoard.Enrollment.EnrollmentPreview';

let EnrollmentPreviewModule = angular.module(moduleName, ['ng-slide-down']);

EnrollmentPreviewModule
  .service('EnrollmentPreviewResource', EnrollmentPreviewResource)
  .directive('knbitEventsTermPreview', TermPreviewDirective)
  .directive('knbitEventsAttendeePreview', AttendeePreviewDirective)
  .directive('knbitEventsTermProgressPreview', TermProgressPreviewDirective)
  .controller('EnrollmentPreviewController', EnrollmentPreviewController)
  .controller('TermProgressPreviewController', TermProgressPreviewController);

export default EnrollmentPreviewModule;
