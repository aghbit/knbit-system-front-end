/**
 * Created by novy on 28.11.15.
 */

'use strict';

import BacklogPreviewController from './backlog-preview.controller';
import BacklogPreviewResource from './backog-preview.resource';

const BacklogModule = angular.module('knbitFrontend.Events.EventMaster.KanbanBoard.Backlog', [
  'knbitFrontend.Events.Config'
]);

BacklogModule
  .controller('BacklogPreviewController', BacklogPreviewController)
  .factory('backlogPreviewResource', BacklogPreviewResource);

export default BacklogModule;
