/**
 * Created by novy on 28.11.15.
 */

function BacklogPreviewResource($http, backlogPreviewConfig) {

  return {
    backlogPreviewFor: backlogPreviewFor
  };

  function backlogPreviewFor(eventId) {
    const endpointUrl = backlogPreviewConfig.urlPrefix + `/${eventId}` + backlogPreviewConfig.urlSuffix;

    return $http({
      method: 'GET',
      url: endpointUrl
    })
      .then(response => response.data);
  }
}

BacklogPreviewResource.$inject = ['$http', 'backlogPreviewConfig'];

export default BacklogPreviewResource;
