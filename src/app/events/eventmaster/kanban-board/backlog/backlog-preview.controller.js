/**
 * Created by novy on 28.11.15.
 */

'use strict';

function BacklogPreviewController($stateParams, backlogPreviewResource) {

  const vm = this;
  retrievePreviewForBacklogEvent($stateParams.eventId);

  function retrievePreviewForBacklogEvent(eventId) {
    backlogPreviewResource
      .backlogPreviewFor(eventId)
      .then(preview => vm.event = preview);
  }
}

BacklogPreviewController.$inject = ['$stateParams', 'backlogPreviewResource'];

export default BacklogPreviewController;
