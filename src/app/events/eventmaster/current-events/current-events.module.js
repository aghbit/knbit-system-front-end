/**
 * Created by novy on 22.05.15.
 */

'use strict';

import EventsCommonModule from '../common/common.module.js';

import NewEventController from './adding-new/new-event.controller.js';
import NewEventModalController from './adding-new/new-event-modal.controller.js';
import EventCreationResource from './adding-new/new-event.resource.js';

var CurrentEventsModule = angular.module('knbitFrontend.Events.EventMaster.CurrentEvents', [
  EventsCommonModule.name,
  'knbitFrontend.Events.Config',
  'restangular',
  'ui.bootstrap',
  'mgcrea.ngStrap.tooltip',
  'mgcrea.ngStrap.helpers.parseOptions',
  'mgcrea.ngStrap.select'
])
  .controller('NewEventModalController', NewEventModalController)
  .controller('NewEventController', NewEventController)
  .service('EventCreationResource', EventCreationResource);


export default CurrentEventsModule;
