/**
 * Created by novy on 13.05.15.
 */

'use strict';

NewEventModalController.$inject = ['$modalInstance', 'EventCreationResource'];

function NewEventModalController($modalInstance, EventCreationResource) {

  var vm = this;

  vm.eventTypes = [
    {name: 'Lecture', value: 'LECTURE'},
    {name: 'Workshop', value: 'WORKSHOP'},
    {name: 'Study Group', value: 'STUDY_GROUP'}
  ];

  vm.event = {
    name: null,
    imageUrl: null,
    description: null,
    eventType: 'LECTURE',
    section: null
  };

  vm.submit = submit;
  vm.cancel = cancel;

  pullSections();

  function submit() {
    return $modalInstance
      .close(vm.event);
  }

  function cancel() {
    $modalInstance.dismiss('cancel');
  }

  function flatSections(sections) {
    return sections
      .map(section => {
        return {
          id: section.id,
          name: section.name
        }
      });
  }

  function valueLabelSectionTuples(sections) {
    return sections
      .map(section => {
        return {
          value: section,
          label: section.name
        }
      });
  }

  // TODO: in better world, use immutable data structures instead
  function addEmptySection(sections) {
    sections.unshift({
      value: null,
      label: 'None'
    });
    return sections;
  }

  function pullSections() {
    EventCreationResource
      .pullSections()
      .then(flatSections)
      .then(valueLabelSectionTuples)
      .then(addEmptySection)
      .then(sections => vm.sections = sections);
  }

}

export default NewEventModalController;
