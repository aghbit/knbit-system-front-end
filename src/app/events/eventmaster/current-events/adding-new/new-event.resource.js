'use strict';

class EventCreationResource {

  constructor($http, eventConfig, sectionsUrl) {
    this.$http = $http;
    this.eventConfig = eventConfig;
    this.sectionsUrl = sectionsUrl;
  }

  pullSections() {
    return this.$http({
      method: 'GET',
      url: this.sectionsUrl
    })
      .then(response => response.data);
  }

  createBacklogEventOf(payload) {
    return this.$http({
      method: 'POST',
      url: this.eventConfig.eventUrl,
      data: payload
    })
      .then(response => response.data);
  }

}

EventCreationResource.$inject = ['$http', 'eventConfig', 'sectionsUrl'];

export default EventCreationResource;
