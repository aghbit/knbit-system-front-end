/**
 * Created by novy on 13.05.15.
 */

'use strict';

NewEventController.$inject = ['$rootScope', '$modal', 'EventCreationResource', 'ToastingService', 'Messages'];

function NewEventController($rootScope, $modal, EventCreationResource, ToastingService, Messages) {

  let vm = this;
  vm.addEvent = addEvent;

  function addEvent() {
    newEventFromModalWindow()
      .then(handleNewEvent);
  }

  function newEventFromModalWindow() {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/current-events/adding-new/new-event.html',
      controller: 'NewEventModalController',
      controllerAs: "vm",
      size: 'lg'
    };

    return $modal
      .open(modalOptions)
      .result;
  }

  function handleNewEvent(newEvent) {
    EventCreationResource
      .createBacklogEventOf(newEvent)
      .then(newEventSuccessfullyAdded, failedToAddNewEvent);
  }

  function newEventSuccessfullyAdded() {
    ToastingService.showSuccessToast(Messages.NEW_EVENT.success);
    $rootScope.$emit('kanban-board:refresh');
  }

  function failedToAddNewEvent() {
    ToastingService.showErrorToast(Messages.NEW_EVENT.error);
  }

}

export default NewEventController;
