'use strict';

import Routes from './eventmaster.routes.js';

import AnnouncementModule from './announcement/announcement.module.js';
import EventMasterCommonModule from './common/common.module.js';
import CurrentEventsModule from './current-events/current-events.module.js';
import KanbanBoardModule from './kanban-board/kanban-board.module.js';
import ProposalsModule from './proposals/proposals.module.js';
import AnswerQuestionModule from './answering-questions/answering-questions.module.js';

var EventMasterModule = angular.module('knbitFrontend.Events.EventMaster', [
  AnnouncementModule.name,
  EventMasterCommonModule.name,
  CurrentEventsModule.name,
  KanbanBoardModule.name,
  ProposalsModule.name,
  AnswerQuestionModule.name
])
  .config(Routes);

export default EventMasterModule;
