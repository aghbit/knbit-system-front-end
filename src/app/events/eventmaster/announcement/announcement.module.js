/**
 * Created by novy on 22.05.15.
 */

'use strict';

import AnnouncementCommonModule from './common/announcement-common.module.js';
import AnnouncementConfigurationModule from './configuration/configuration.module.js';
import AnnouncementPublisherModule from './publisher/publisher.module.js';

var AnnouncementModule = angular.module('knbitFrontend.Events.EventMaster.Announcement', [
  AnnouncementCommonModule.name,
  AnnouncementConfigurationModule.name,
  AnnouncementPublisherModule.name
]);


export default AnnouncementModule;
