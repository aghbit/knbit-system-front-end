'use strict';

class AnnouncementPublishingService {

  constructor($http, announcementPublishingConfig) {
    this.publishingEndpoint = announcementPublishingConfig.publishingEndpoint;
    this.$http = $http;
  }

  publishAnnouncement(announcement) {
    return this.$http.post(this.publishingEndpoint, announcement);
  }
}

AnnouncementPublishingService.$inject = ['$http', 'announcementPublishingConfig'];


export default AnnouncementPublishingService;
