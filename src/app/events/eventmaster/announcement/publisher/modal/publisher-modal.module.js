/**
 * Created by novy on 24.09.15.
 */

'use strict';

import EventsCommonModule from '../../../common/common.module.js';
import AnnouncementCommonModule from '../../common/announcement-common.module.js';

import publisherModalService from './publisher-modal.service.js';
import PublisherModalController from './publisher-modal.controller.js';

let moduleName = 'knbitFrontend.Events.EventMaster.Announcement.Publisher.Modal';

let PublisherModalModule = angular.module(moduleName, [
  EventsCommonModule.name,
  AnnouncementCommonModule.name,
  'knbitFrontend.Events.Config',
  'ui.bootstrap',
  'ui.select',
  'ngSanitize'
]);

PublisherModalModule
  .factory('publisherModalService', publisherModalService)
  .controller('PublisherModalController', PublisherModalController);

export default PublisherModalModule;
