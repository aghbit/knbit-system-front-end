'use strict';

AnnouncementPublishingModalController.$inject = ['$modalInstance', 'availablePublishers', 'announcement'];

function AnnouncementPublishingModalController($modalInstance, availablePublishers, announcement) {

  let vm = this;

  vm.availablePublishers = availablePublishers;
  vm.announcement = announcement;
  vm.publishAnnouncement = publishAnnouncement;
  vm.cancel = cancel;

  function publishAnnouncement() {
    $modalInstance.close(vm.announcement);
  }

  function cancel() {
    $modalInstance.dismiss('cancel');
  }
}

export default AnnouncementPublishingModalController;
