/**
 * Created by novy on 24.09.15.
 */

'use strict';

publisherModalService.$inject = ['$modal', 'basicConfigurationDataResource'];

function publisherModalService($modal, basicConfigurationDataResource) {

  return {
    openPublishingAnnouncementModal: openPublishingAnnouncementModal
  };

  function openPublishingAnnouncementModal(prepopulatedAnnouncement) {
    let announcement = prepopulatedAnnouncement || emptyAnnouncement();

    return retrievePublisherConfigurations()
      .then(publisherConfigurations => openModalWith(publisherConfigurations, announcement));
  }

  function emptyAnnouncement() {
    return {
      title: '',
      content: '',
      imageUrl: '',
      publishers: []
    };
  }

  function retrievePublisherConfigurations() {
    return basicConfigurationDataResource.basicConfigurationData();
  }

  function openModalWith(publishers, prepopulatedAnnouncement) {
    return $modal
      .open(prepareModalOptionsWith(publishers, prepopulatedAnnouncement))
      .result;
  }

  function prepareModalOptionsWith(publishers, prepopulatedAnnouncement) {
    let defaultModalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/announcement/publisher/modal/publisher-form.html',
      controller: 'PublisherModalController',
      controllerAs: 'vm',
      size: 'lg'
    };

    return angular.extend({}, defaultModalOptions, {
      resolve: {
        availablePublishers: () => publishers,
        announcement: () => prepopulatedAnnouncement
      }
    });
  }
}

export default publisherModalService;
