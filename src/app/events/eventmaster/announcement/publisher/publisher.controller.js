/**
 * Created by novy on 12.04.15.
 */

'use strict';

AnnouncementPublishingController.$inject = [
  'AnnouncementPublishingService',
  'publisherModalService',
  'ToastingService',
  'Messages'
];

function AnnouncementPublishingController(AnnouncementPublishingService,
                                          publisherModalService,
                                          ToastingService,
                                          Messages) {
  let ctrl = this;
  ctrl.publishAnnouncement = publishAnnouncement;

  function publishAnnouncement() {
    publisherModalService
      .openPublishingAnnouncementModal()
      .then(consumeAnnouncement);
  }

  function consumeAnnouncement(announcement) {
    AnnouncementPublishingService
      .publishAnnouncement(announcement)
      .then(showSuccessToast, showFailureToast);
  }

  function showSuccessToast() {
    ToastingService.showSuccessToast(Messages.ANNOUNCEMENT_PUBLISHER.success);
  }

  function showFailureToast() {
    ToastingService.showErrorToast(Messages.ANNOUNCEMENT_PUBLISHER.error);
  }
}

export default AnnouncementPublishingController;
