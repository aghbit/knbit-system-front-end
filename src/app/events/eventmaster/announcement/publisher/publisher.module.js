/**
 * Created by novy on 22.05.15.
 */

'use strict';

import EventsCommonModule from '../../common/common.module.js';
import AnnouncementPublishingModalModule from './modal/publisher-modal.module.js';

import AnnouncementPublishingService from './publisher.service.js';
import AnnouncementPublishingController from './publisher.controller.js';

let AnnouncementPublisherModule = angular.module('knbitFrontend.Events.EventMaster.Announcement.Publisher', [
  EventsCommonModule.name,
  AnnouncementPublishingModalModule.name,
  'knbitFrontend.Events.Config'
])
  .service('AnnouncementPublishingService', AnnouncementPublishingService)
  .controller('AnnouncementPublishingController', AnnouncementPublishingController);


export default AnnouncementPublisherModule;
