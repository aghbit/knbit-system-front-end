/**
 * Created by novy on 21.09.15.
 */

'use strict';

import basicConfigurationDataResource from './basic-configuration-data.resource.js';

let moduleName = 'knbitFrontend.Events.EventMaster.Announcement.Common';

let CommonModule = angular.module(moduleName, [
  'knbitFrontend.Events.Config',
  'ngResource'
]);

CommonModule
  .factory('basicConfigurationDataResource', basicConfigurationDataResource);

export default CommonModule;
