/**
 * Created by novy on 20.09.15.
 */

'use strict';

basicConfigurationDataResource.$inject = ['$resource', 'announcementConfigurationProperties'];

function basicConfigurationDataResource($resource, announcementConfigurationProperties) {

  let resource = createBasicConfigurationDataResource();

  return {
    basicConfigurationData: basicConfigurationData
  };

  function createBasicConfigurationDataResource() {
    let allowedMethods = {
      get: {method: 'GET', isArray: true}
    };
    let endpointUrl = announcementConfigurationProperties.basicDataUrl;
    return $resource(endpointUrl, {}, allowedMethods);
  }

  function basicConfigurationData() {
    return resource
      .get()
      .$promise;
  }
}

export default basicConfigurationDataResource;
