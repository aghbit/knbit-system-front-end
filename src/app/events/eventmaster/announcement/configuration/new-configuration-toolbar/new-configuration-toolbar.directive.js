/**
 * Created by novy on 20.09.15.
 */

'use strict';

function NewConfigurationToolbarDirective() {

  return {
    restrict: 'E',
    scope: {
      publishersWithActions: '='
    },
    controller: angular.noop,
    controllerAs: 'ctrl',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/announcement/configuration/new-configuration-toolbar/new-configuration-toolbar.tpl.html'
  };
}

export default NewConfigurationToolbarDirective;
