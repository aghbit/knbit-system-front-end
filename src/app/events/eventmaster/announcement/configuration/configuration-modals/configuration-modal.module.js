/**
 * Created by novy on 20.09.15.
 */

'use strict';

import configurationModalService from './configuration-modal.service.js';
import ConfigurationModalController from './configuration-modal.controller.js';

let moduleName = 'knbitFrontend.Events.EventMaster.Announcement.Configuration.Modals';

let ModalsModule = angular.module(moduleName, []);

ModalsModule
  .controller('ConfigurationModalController', ConfigurationModalController)
  .factory('configurationModalService', configurationModalService);

export default ModalsModule;
