/**
 * Created by novy on 20.09.15.
 */

'use strict';

ConfigurationModalController.$inject = ['$modalInstance', 'configuration'];

function ConfigurationModalController($modalInstance, configuration) {

  let vm = this;
  vm.configuration = configuration;
  vm.submit = submit;
  vm.cancel = cancel;

  function submit() {
    $modalInstance.close(vm.configuration);
  }

  function cancel() {
    $modalInstance.dismiss('cancel');
  }
}

export default ConfigurationModalController;
