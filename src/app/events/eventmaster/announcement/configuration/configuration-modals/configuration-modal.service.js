/**
 * Created by novy on 20.09.15.
 */

'use strict';

ConfigurationModalService.$inject = ['$modal', 'modalService'];

function ConfigurationModalService($modal, modalService) {

  let templatesByVendor = {
    FACEBOOK: 'app/events/eventmaster/announcement/configuration/configuration-modals/forms/facebook-form.html',
    TWITTER: 'app/events/eventmaster/announcement/configuration/configuration-modals/forms/twitter-form.html',
    GOOGLE_GROUP: 'app/events/eventmaster/announcement/configuration/configuration-modals/forms/google-group-form.html',
    IIET_BOARD: 'app/events/eventmaster/announcement/configuration/configuration-modals/forms/board-form.html'
  };

  return {
    confirmConfigurationDeletion: confirmConfigurationDeletion,
    openConfigurationModal: openConfigurationModal
  };

  function confirmConfigurationDeletion() {
    let confirmationCaptions = {
      confirmationMessage: 'Are you sure you want to delete this configuration?'
    };

    return modalService.confirm(confirmationCaptions);
  }

  function openConfigurationModal(configurtion, vendor) {
    let modalOptions = {
      animation: true,
      templateUrl: templatesByVendor[vendor],
      controller: 'ConfigurationModalController',
      controllerAs: 'vm',
      size: 'lg',
      resolve: {
        configuration: () => configurtion
      }
    };

    return $modal
      .open(modalOptions)
      .result;
  }
}

export default ConfigurationModalService;
