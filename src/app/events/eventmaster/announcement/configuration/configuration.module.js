/**
 * Created by novy on 22.05.15.
 */

'use strict';

import EventsCommonModule from '../../common/common.module.js';
import ServicesModule from './data-services/publisher-configuration-services.module.js';
import ModalsModule from './configuration-modals/configuration-modal.module.js';
import AnnouncementConfigurationController from './announcement-configuration.controller.js';
import ConfigurationListDirective from './configuration-list/configuration-list.directive.js';
import ConfigurationOverviewDirective from './configuration-list/configuration-overview/configuration-overview.directive.js';
import ConfigurationOverviewController from './configuration-list/configuration-overview/configuration-overview.controller.js';
import NewConfigurationToolbarDirective from './new-configuration-toolbar/new-configuration-toolbar.directive.js';

let AnnouncementConfigurationModule = angular.module('knbitFrontend.Events.EventMaster.Announcement.Configuration', [
  EventsCommonModule.name,
  ServicesModule.name,
  ModalsModule.name,
  'knbitFrontend.Events.Config'
]);

AnnouncementConfigurationModule
  .directive('knbitEventsAnnouncementConfigurationList', ConfigurationListDirective)
  .directive('knbitEventsAnnouncementConfigurationOverview', ConfigurationOverviewDirective)
  .controller('ConfigurationOverviewController', ConfigurationOverviewController)
  .controller('AnnouncementConfigurationController', AnnouncementConfigurationController)
  .directive('knbitEventsAnnouncementNewConfigurationToolbar', NewConfigurationToolbarDirective);


export default AnnouncementConfigurationModule;
