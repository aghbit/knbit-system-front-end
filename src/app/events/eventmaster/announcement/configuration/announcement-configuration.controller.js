/**
 * Created by novy on 20.09.15.
 */

'use strict';

AnnouncementConfigurationController.$inject = [
  'publisherConfigurationService',
  'configurationModalService',
  'ToastingService',
  'Messages'
];

function AnnouncementConfigurationController(publisherConfigurationService,
                                             configurationModalService,
                                             ToastingService,
                                             Messages) {

  let vm = this;
  prepareActionsForToolbar();
  populateView();

  function prepareActionsForToolbar() {
    let publishersWithoutAction = [
      {vendor: 'FACEBOOK', name: 'Facebook'},
      {vendor: 'TWITTER', name: 'Twitter'},
      {vendor: 'GOOGLE_GROUP', name: 'Google Group'},
      {vendor: 'IIET_BOARD', name: 'IIET Board'}
    ];

    let assignAction = publisher => {
      return {
        name: publisher.name,
        newConfiguration: () => createConfigurationHandler(publisher.vendor)
      };
    };

    vm.publishersWithActions = publishersWithoutAction.map(assignAction);
  }

  function populateView() {
    publisherConfigurationService
      .basicConfigurationData()
      .then(assignEditAndDeleteCallbacks)
      .then(configurationsWithCallbacks => vm.configurationsWithCallbacks = configurationsWithCallbacks);
  }

  function assignEditAndDeleteCallbacks(configurations) {
    let configurationWithCallbacks = configuration => {
      return {
        configuration: configuration,
        editCallback: () => editConfigurationHandler(configuration.id, configuration, configuration.vendor),
        removeCallback: () => deleteConfiguration(configuration.id, configuration.vendor)
      };
    };

    return configurations.map(configurationWithCallbacks);
  }

  function createConfigurationHandler(vendor) {
    configurationModalService
      .openConfigurationModal({}, vendor)
      .then(newConfiguration => handleNewConfiguration(newConfiguration, vendor));
  }

  function handleNewConfiguration(newConfiguration, vendor) {
    publisherConfigurationService
      .createConfiguration(newConfiguration, vendor)
      .then(handleSuccess, handleFailure)
      .then(populateView);
  }

  function editConfigurationHandler(configurationId, configuration, vendor) {
    configurationModalService
      .openConfigurationModal(angular.copy(configuration), vendor)
      .then(editedConfiguration => handleConfigurationToUpdate(configurationId, editedConfiguration, vendor));
  }

  function handleConfigurationToUpdate(configurationId, configuration, vendor) {
    publisherConfigurationService
      .editConfiguration(configurationId, configuration, vendor)
      .then(handleSuccess, handleFailure)
      .then(populateView);
  }

  function deleteConfiguration(configurationId, vendor) {
    configurationModalService
      .confirmConfigurationDeletion()
      .then(() => publisherConfigurationService.deleteConfiguration(configurationId, vendor))
      .then(populateView);
  }

  function handleSuccess() {
    ToastingService.showSuccessToast(Messages.ANNOUNCEMENT_CONFIGURATION.success);
  }

  function handleFailure() {
    ToastingService.showErrorToast(Messages.ANNOUNCEMENT_CONFIGURATION.error);
  }
}

export default AnnouncementConfigurationController;
