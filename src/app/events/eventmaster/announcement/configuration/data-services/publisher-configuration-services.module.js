/**
 * Created by novy on 20.09.15.
 */

'use strict';

import {facebookConfigurationResource, twitterConfigurationResource, googleGroupConfigurationResource, iietBoardConfigurationResource}
  from './publisher-configuration.resources.js';
import publisherConfigurationService from './publisher-configuration.service.js';

let moduleName = 'knbitFrontend.Events.EventMaster.Announcement.Configuration.Services';

let ServicesModule = angular.module(moduleName, [
  'knbitFrontend.Events.Config',
  'ngResource'
]);

ServicesModule
  .factory('facebookConfigurationResource', facebookConfigurationResource)
  .factory('twitterConfigurationResource', twitterConfigurationResource)
  .factory('googleGroupConfigurationResource', googleGroupConfigurationResource)
  .factory('iietBoardConfigurationResource', iietBoardConfigurationResource)
  .factory('publisherConfigurationService', publisherConfigurationService);

export default ServicesModule;

