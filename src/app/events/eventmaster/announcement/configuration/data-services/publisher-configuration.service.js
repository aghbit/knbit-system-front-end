/**
 * Created by novy on 20.09.15.
 */

'use strict';

publisherConfigurationService.$inject = [
  'basicConfigurationDataResource',
  'facebookConfigurationResource',
  'twitterConfigurationResource',
  'googleGroupConfigurationResource',
  'iietBoardConfigurationResource'
];

function publisherConfigurationService(basicConfigurationDataResource,
                                       facebookConfigurationResource,
                                       twitterConfigurationResource,
                                       googleGroupConfigurationResource,
                                       iietBoardConfigurationResource) {

  let resourcesByVendor = {
    FACEBOOK: facebookConfigurationResource,
    TWITTER: twitterConfigurationResource,
    GOOGLE_GROUP: googleGroupConfigurationResource,
    IIET_BOARD: iietBoardConfigurationResource
  };

  return {
    basicConfigurationData: basicConfigurationData,
    getConfigurationDetails: getConfigurationDetails,
    createConfiguration: createConfiguration,
    editConfiguration: editConfiguration,
    deleteConfiguration: deleteConfiguration
  };

  function basicConfigurationData() {
    return basicConfigurationDataResource
      .basicConfigurationData();
  }

  function getConfigurationDetails(configurationId, vendor) {
    return findProperResourceBy(vendor)
      .getConfigurationDetails(configurationId);
  }

  function createConfiguration(configuration, vendor) {
    return findProperResourceBy(vendor)
      .createConfiguration(configuration);
  }

  function editConfiguration(configurationId, configuration, vendor) {
    return findProperResourceBy(vendor)
      .updateConfiguration(configurationId, configuration);
  }

  function deleteConfiguration(configurationId, vendor) {
    return findProperResourceBy(vendor)
      .deleteConfiguration(configurationId);
  }

  function findProperResourceBy(vendor) {
    let resource = resourcesByVendor[vendor];
    if (!angular.isDefined(resource)) {
      throw new Error('no such vendor');
    }
    return resource;
  }
}

export default publisherConfigurationService;
