/**
 * Created by novy on 07.04.15.
 */

'use strict';

var configurationResource = function ($resource, endpointUrl) {

  var allConfigurationsResource = createAllConfigurationResource();
  var identifiedConfigurationResource = createIdentifiedConfigurationResource();

  return {
    getConfigurationDetails: getConfigurationDetails,
    createConfiguration: createConfiguration,
    updateConfiguration: updateConfiguration,
    deleteConfiguration: deleteConfiguration
  };

  function createAllConfigurationResource() {
    var allowedMethods = {
      create: {method: 'POST'}
    };
    return $resource(endpointUrl, {}, allowedMethods);
  }

  function createIdentifiedConfigurationResource() {
    var allowedMethods = {
      get: {method: 'GET'},
      update: {method: 'PUT'},
      'delete': {method: 'DELETE'}
    };
    return $resource(endpointUrl + '/:id', {id: '@id'}, allowedMethods);
  }

  function getConfigurationDetails(configurationId) {
    return identifiedConfigurationResource
      .get({id: configurationId})
      .$promise;
  }

  function createConfiguration(configuration) {
    return allConfigurationsResource
      .create(configuration)
      .$promise;
  }

  function updateConfiguration(configurationId, configuration) {
    return identifiedConfigurationResource
      .update({id: configurationId}, configuration)
      .$promise;
  }

  function deleteConfiguration(configurationId) {
    return identifiedConfigurationResource
      .delete({id: configurationId})
      .$promise;
  }
};

var facebookConfigurationResource = function ($resource, announcementConfigurationProperties) {
  return configurationResource($resource, announcementConfigurationProperties.facebookUrl);
};
facebookConfigurationResource.$inject = ['$resource', 'announcementConfigurationProperties'];

var twitterConfigurationResource = function ($resource, announcementConfigurationProperties) {
  return configurationResource($resource, announcementConfigurationProperties.twitterUrl);
};
twitterConfigurationResource.$inject = ['$resource', 'announcementConfigurationProperties'];

var googleGroupConfigurationResource = function ($resource, announcementConfigurationProperties) {
  return configurationResource($resource, announcementConfigurationProperties.googleGroupUrl);
};
googleGroupConfigurationResource.$inject = ['$resource', 'announcementConfigurationProperties'];

var iietBoardConfigurationResource = function ($resource, announcementConfigurationProperties) {
  return configurationResource($resource, announcementConfigurationProperties.iietBoardUrl);
};
iietBoardConfigurationResource.$inject = ['$resource', 'announcementConfigurationProperties'];


export {facebookConfigurationResource, twitterConfigurationResource, googleGroupConfigurationResource, iietBoardConfigurationResource};
