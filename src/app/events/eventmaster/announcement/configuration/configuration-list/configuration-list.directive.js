/**
 * Created by novy on 20.09.15.
 */

'use strict';

function ConfigurationListDirective() {

  return {
    restrict: 'E',
    scope: {
      configurationsWithCallbacks: '='
    },
    controller: angular.noop,
    controllerAs: 'ctrl',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/announcement/configuration/configuration-list/configuration-list.tpl.html'
  };
}

export default ConfigurationListDirective;
