/**
 * Created by novy on 20.09.15.
 */

'use strict';

function ConfigurationOverviewDirective() {

  return {
    restrict: 'E',
    scope: {
      configuration: '=',
      onDetailsRequested: '&'
    },
    controller: 'ConfigurationOverviewController',
    controllerAs: 'vm',
    bindToController: true,
    templateUrl: 'app/events/eventmaster/announcement/configuration/configuration-list/configuration-overview/configuration-overview.tpl.html'
  };
}

export default ConfigurationOverviewDirective;
