/**
 * Created by novy on 20.09.15.
 */

'use strict';

function ConfigurationOverviewController() {
  let cssByVendor = {
    FACEBOOK: {
      btnClass: 'btn-facebook',
      icon: 'fa-facebook'
    },
    TWITTER: {
      btnClass: 'btn-twitter',
      icon: 'fa-twitter'
    },
    GOOGLE_GROUP: {
      btnClass: 'btn-facebook',
      icon: 'fa-google'
    },
    IIET_BOARD: {
      btnClass: 'btn-facebook',
      icon: 'fa-vk'
    }
  };

  var vm = this;
  vm.cssClass = buttonClassFor(vm.configuration);
  vm.iconClass = iconClassFor(vm.configuration);


  function buttonClassFor(configuration) {
    return cssByVendor[configuration.vendor].btnClass;
  }

  function iconClassFor(configuration) {
    return cssByVendor[configuration.vendor].icon;
  }
}

export default ConfigurationOverviewController;
