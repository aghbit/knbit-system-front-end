'use strict';

class AnsweringQuestionService {

  constructor($resource, membersConfig) {
    let allowedMethods = {
      'post': {method: 'POST'}
    };

    this.answerResource = $resource(membersConfig.answerUrl, {}, allowedMethods);
  }

  answer(answer) {
    return this.answerResource
      .post(answer)
      .$promise;
  }

}

AnsweringQuestionService.$inject = ['$resource', 'membersConfig'];
export default AnsweringQuestionService;
