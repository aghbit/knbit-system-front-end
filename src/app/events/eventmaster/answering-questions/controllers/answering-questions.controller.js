'use strict';

AnswerQuestionController.$inject =
  ['$rootScope', '$modal', 'AnsweringQuestionService', 'ToastingService', 'Messages'];

function AnswerQuestionController($rootScope, $modal, AnsweringQuestionService, ToastingService, Messages) {

  listenForQuestionToAnswer();

  function listenForQuestionToAnswer() {
    $rootScope.$on('notification-bar:answer-question-selected', (event, memberQuestion) => {
      onQuestionToAnswerSelected(memberQuestion);
    });
  }

  function onQuestionToAnswerSelected(memberQuestion) {
    questionAnswerFromModal(memberQuestion)
      .then(answer => handleEventMasterAnswer(answer, memberQuestion));
  }

  function questionAnswerFromModal(memberQuestion) {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/answering-questions/partials/answering-question.html',
      controller: 'AnswerQuestionModalController as ctrl',
      size: 'md',
      resolve: {
        payload: () => memberQuestion
      }
    };

    return $modal
      .open(modalOptions)
      .result;
  }

  function handleEventMasterAnswer(answer, memberQuestion) {
    submitAnswer(answer, memberQuestion)
      .then(successfullyAnsweredMemberQuestion, failedToAnswerMemberQuestion);
  }

  function submitAnswer(answer, question) {
    return AnsweringQuestionService
      .answer({
        email: question.email,
        subject: 'Re: ' + question.topic,
        content: answer
      });
  }

  function successfullyAnsweredMemberQuestion() {
    ToastingService.showSuccessToast(Messages.ANSWER_QUESTION.success);
  }

  function failedToAnswerMemberQuestion() {
    ToastingService.showErrorToast(Messages.ANSWER_QUESTION.error);
  }
}

export default AnswerQuestionController;
