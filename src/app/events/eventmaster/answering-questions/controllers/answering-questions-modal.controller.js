'use strict';

AnswerQuestionModalController.$inject = ['$modalInstance', 'payload'];

function AnswerQuestionModalController($modalInstance, payload) {

  let vm = this;
  vm.question = payload;
  vm.answerQuestion = () => $modalInstance.close(vm.answer);
  vm.cancel = () => $modalInstance.dismiss('cancel');

}


export default AnswerQuestionModalController;
