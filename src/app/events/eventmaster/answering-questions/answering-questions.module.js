'use strict';

import EventsCommonModule from '../common/common.module.js';

import AnswerQuestionController from './controllers/answering-questions.controller.js';
import AnswerQuestionModalController from './controllers/answering-questions-modal.controller.js';
import AnsweringQuestionService from './services/answering-questions.resource.js';

var AnswerQuestionModule = angular.module('knbitFrontend.Events.EventMaster.AnswerQuestion', [
  EventsCommonModule.name,
  'knbitFrontend.Events.Config',
  'ui.bootstrap'
])
  .controller('AnswerQuestionController', AnswerQuestionController)
  .controller('AnswerQuestionModalController', AnswerQuestionModalController)
  .service('AnsweringQuestionService', AnsweringQuestionService);

export default AnswerQuestionModule;
