/**
 * Created by novy on 22.05.15.
 */

'use strict';

import EventsCommonModule from '../common/common.module.js';

import EventProposalService from './accept-reject/services/event-proposal.resource.js';
import EventProposalController from './accept-reject/controllers/event-proposal.controller.js';
import EventProposalModalController from './accept-reject/controllers/event-proposal-modal.controller.js';

var ProposalsModule = angular.module('knbitFrontend.Events.EventMaster.Proposals', [
  EventsCommonModule.name,
  'knbitFrontend.Events.Config',
  'ngResource',
  'ui.bootstrap'
])
  .service('EventProposalResource', EventProposalService)
  .controller('EventProposalController', EventProposalController)
  .controller('EventProposalModalController', EventProposalModalController);


export default ProposalsModule;
