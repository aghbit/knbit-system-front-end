/**
 * Created by novy on 12.05.15.
 */

'use strict';

class EventProposalService {

  constructor($resource, proposalConfig) {
    let allowedMethods = {
      'get': {method: 'GET'},
      'update': {method: 'PATCH'}
    };
    let endpointUrl = proposalConfig.proposalUrl + '/:id';

    this.proposalResource = $resource(endpointUrl, {id: '@id'}, allowedMethods);
  }

  proposalBy(proposalId) {
    return this.proposalResource
      .get({id: proposalId})
      .$promise;
  }

  changeState(domainId, state) {
    return this.proposalResource
      .update({id: domainId}, {state: state})
      .$promise;
  }
}

EventProposalService.$inject = ['$resource', 'proposalConfig'];


export default EventProposalService;
