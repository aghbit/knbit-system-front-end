/**
 * Created by novy on 14.05.15.
 */

'use strict';

EventProposalModalController.$inject = ['$modalInstance', 'eventProposal'];

function EventProposalModalController($modalInstance, eventProposal) {

  var vm = this;
  vm.eventProposal = eventProposal;
  vm.accept = () => exitWithState('ACCEPTED');
  vm.reject = () => exitWithState('REJECTED');
  vm.isProposalPending = () => vm.eventProposal.state === 'PENDING';

  function exitWithState(newState) {
    return $modalInstance.close(newState);
  }

}


export default EventProposalModalController;
