/**
 * Created by novy on 12.05.15.
 */

'use strict';

EventProposalController.$inject =
  ['$rootScope', '$q', '$modal', 'EventProposalResource', 'ToastingService', 'Messages'];

function EventProposalController($rootScope, $q, $modal, EventProposalResource, ToastingService, Messages) {

  let toastsByAction = {
    ACCEPTED: Messages.ACCEPTED_PROPOSAL,
    REJECTED: Messages.REJECTED_PROPOSAL
  };
  listenForProposalSelection();

  function listenForProposalSelection() {
    $rootScope.$on('notification-bar:event-proposal-selected', (event, eventProposalId) => {
      onEventProposalSelected(eventProposalId);
    });
  }

  function onEventProposalSelected(eventProposalId) {
    EventProposalResource
      .proposalBy(eventProposalId)
      .then(openModalForGivenProposal)
      .then(acceptOrRejectAction => handleAcceptOrRejectActionFor(eventProposalId, acceptOrRejectAction));
  }

  function openModalForGivenProposal(eventProposal) {
    let modalOptions = {
      animation: true,
      templateUrl: 'app/events/eventmaster/proposals/accept-reject/partials/event-proposal-view.html',
      controller: 'EventProposalModalController as ctrl',
      size: 'lg',
      resolve: {
        eventProposal: () => eventProposal
      }
    };

    return $modal
      .open(modalOptions)
      .result;
  }

  function handleAcceptOrRejectActionFor(eventProposalId, acceptedOrRejectedAction) {
    acceptOrReject(eventProposalId, acceptedOrRejectedAction)
      .then(successfullyAcceptedOrRejectedProposal, failedToAcceptOrRejectProposal);
  }

  function acceptOrReject(eventProposalId, acceptOrRejectAction) {
    let deferredAcceptedOrRejectedState = $q.defer();
    let resolveWithGivenAction = () => deferredAcceptedOrRejectedState.resolve(acceptOrRejectAction);
    let rejectWithGivenAction = () => deferredAcceptedOrRejectedState.reject(acceptOrRejectAction);

    EventProposalResource
      .changeState(eventProposalId, acceptOrRejectAction)
      .then(resolveWithGivenAction, rejectWithGivenAction);

    return deferredAcceptedOrRejectedState.promise;
  }

  function successfullyAcceptedOrRejectedProposal(acceptedOrRejectedAction) {
    ToastingService.showSuccessToast(toastsByAction[acceptedOrRejectedAction].success);
    $rootScope.$emit('kanban-board:refresh');
  }

  function failedToAcceptOrRejectProposal(acceptedOrRejectedAction) {
    ToastingService.showErrorToast(toastsByAction[acceptedOrRejectedAction].error);
  }
}


export default EventProposalController;
