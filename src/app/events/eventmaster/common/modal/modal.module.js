/**
 * Created by novy on 06.09.15.
 */

'use strict';

import modalService from './modal.service.js';
import ConfirmationModalController from './confirm/confirmation-modal.controller.js';

let moduleName = 'knbitFrontend.Events.EventMaster.Common.Modal';

let ModalModule = angular.module(moduleName, [
  'ui.bootstrap'
]);

ModalModule
  .factory('modalService', modalService)
  .controller('ConfirmationModalController', ConfirmationModalController);

export default ModalModule;
