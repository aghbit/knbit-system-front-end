/**
 * Created by novy on 06.09.15.
 */

'use strict';

ModalService.$inject = ['$modal'];

function ModalService($modal) {

  return {
    confirm: confirm
  };

  function confirm(customCaptions) {
    let captions = customCaptions || {};

    let defaultModalOptions = {
      animation: true,
      size: 'md',
      templateUrl: 'app/events/eventmaster/common/modal/confirm/confirmation-modal.tpl.html',
      controller: 'ConfirmationModalController',
      controllerAs: 'vm',
      resolve: {
        captions: () => captions
      }
    };

    return $modal
      .open(defaultModalOptions)
      .result;
  }
}

export default ModalService;
