/**
 * Created by novy on 06.09.15.
 */

'use strict';

ConfirmationModalController.$inject = ['$modalInstance', 'captions'];

function ConfirmationModalController($modalInstance, captions) {
  let vm = this;
  vm.confirmationTitle = captions.confirmationTitle || 'Confirmation';
  vm.confirmationMessage = captions.confirmationMessage || 'Are you sure?';
  vm.confirmCaption = captions.confirmCaption || 'Yes';
  vm.cancelCaption = captions.cancelCaption || 'No';

  vm.confirm = confirm;
  vm.cancel = cancel;

  function confirm() {
    $modalInstance.close();
  }

  function cancel() {
    $modalInstance.dismiss();
  }
}

export default ConfirmationModalController;
