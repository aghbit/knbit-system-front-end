/**
 * Created by novy on 22.05.15.
 */

'use strict';

import EventsCommonModule from '../../common/common.module.js';
import ModalModule from './modal/modal.module.js';
import EventsHeaderCtrl from './header/header.controller.js';

var EventMasterCommonModule = angular.module('knbitFrontend.Events.EventMaster.Common', [
  EventsCommonModule.name,
  'toaster',
  'knbitFrontend.Events.Config',
  ModalModule.name
])
  .controller('EventsHeaderCtrl', EventsHeaderCtrl);


export default EventMasterCommonModule;
