'use strict';

import SectionBarModule from './section-bar/section-bar.module.js';
import ToastingService from './toaster/toaster.service.js';
import Messages from './messages/messages.constant.js';

var EventsCommonModule = angular.module('knbitFrontend.Events.Common', [
  SectionBarModule.name
])
  .service('ToastingService', ToastingService)
  .value('Messages', Messages);

export default EventsCommonModule;
