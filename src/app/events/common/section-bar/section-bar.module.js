'use strict';

import SectionBarDirective from './section-bar.directive.js';
import SectionDecorationClassMapping from './section-bar-mappings.js';

var SectionBarModule = angular.module('knbitFrontend.Events.Common.SectionBar', [])
  .directive('knbitEventsSectionBar', SectionBarDirective)
  .value('SectionDecorationClassMapping', SectionDecorationClassMapping);


export default SectionBarModule;
