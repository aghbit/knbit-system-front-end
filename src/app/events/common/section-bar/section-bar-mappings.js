'use strict';

var SectionDecorationClassMapping = {
  'None': 'section-bar__none',
  'Grafika': 'section-bar__grafika',
  'Infra': 'section-bar__infra',
  '.NET': 'section-bar__dotnet',
  'AGK': 'section-bar__agk',
  'Events': 'section-bar__events',
  'AI': 'section-bar__ai',
  'Idea Factory': 'section-bar__if',
  'Algo': 'section-bar__algo'
};

export default SectionDecorationClassMapping;
