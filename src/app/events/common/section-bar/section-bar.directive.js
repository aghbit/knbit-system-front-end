'use strict';

function SectionBarDirective(SectionDecorationClassMapping) {

  return {
    restrict: 'A',
    scope: {
      'knbitEventsSectionBar': '='
    },
    replace: false,
    link: function (scope, elem) {
      const section = scope.knbitEventsSectionBar ? scope.knbitEventsSectionBar.name : 'None';
      elem.addClass(SectionDecorationClassMapping[section]);
    }
  };

}

SectionBarDirective.$inject = ['SectionDecorationClassMapping'];

export default SectionBarDirective;
