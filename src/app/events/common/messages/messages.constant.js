'use strict';

var Messages = {
  'EVENT_STATE': {
    success: {
      title: 'Success',
      message: 'Successfully changed event status'
    },
    error: {
      title: 'Error',
      message: 'Could not change event status'
    }
  },
  'NEW_EVENT': {
    success: {
      title: 'Success',
      message: 'Successfully created an event!'
    },
    error: {
      title: 'Error',
      message: 'Could not create event!'
    }
  },
  'ANSWER_QUESTION': {
    success: {
      title: 'Success',
      message: 'Successfully submitted answer!'
    },
    error: {
      title: 'Error',
      message: 'Couldn\'t submit your answer!'
    }
  },
  'ANNOUNCEMENT_CONFIGURATION': {
    success: {
      title: 'Success',
      message: 'Successfully updated configuration!'
    },
    error: {
      title: 'Error', message: 'Couldn\'t update configuration!'
    }
  },
  'ANNOUNCEMENT_PUBLISHER': {
    success: {
      title: 'Success',
      message: 'Announcement published!'
    },
    error: {
      title: 'Error', message: 'Couldn\'t publish announcement!'
    }
  },
  'MEMBERS_ANSWER_QUESTION': {
    success: {
      title: 'Success',
      message: 'Successfully submitted question!'
    },
    error: {
      title: 'Error',
      message: 'Couldn\'t submit your question!'
    }
  },
  'MEMBERS_VOTE': {
    success: {
      title: 'Success',
      message: 'Your vote has been successfully marked'
    },
    error: {
      title: 'Error',
      message: 'Could not mark your vote'
    }
  },
  'ACCEPTED_PROPOSAL': {
    success: {
      title: 'Success',
      message: 'Successfully accepted proposal!'
    },
    error: {
      title: 'Error',
      message: 'Couldn\'t accept!'
    }
  },
  'REJECTED_PROPOSAL': {
    success: {
      title: 'Success',
      message: 'Successfully rejected proposal!'
    },
    error: {
      title: 'Error',
      message: 'Couldn\'t reject!'
    }
  },
  'EVENT_PROPOSAL': {
    success: {
      title: 'Success',
      message: 'Your proposal has been successfully submitted'
    },
    error: {
      title: 'Error',
      message: 'Couldn\'t submit your proposal'
    }
  }
};

export default Messages;
