/**
 * Created by novy on 13.04.15.
 */

'use strict';

class ToastingService {

  constructor(Toaster) {
    this.Toaster = Toaster;
  }

  showToast(toastType, toastData) {
    this.Toaster.pop({
      type: toastType,
      title: toastData.title,
      body: toastData.message
    });
  }

  showSuccessToast(toastData) {
    this.showToast('success', toastData);
  }

  showErrorToast(toastData) {
    this.showToast('error', toastData);
  }
}

ToastingService.$inject = ['toaster'];


export default ToastingService;
