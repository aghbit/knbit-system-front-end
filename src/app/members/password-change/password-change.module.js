'use strict';

import PasswordChangeCtrl from './controller/password-change.controller.js';
import PasswordChangeFormFactory from './factory/password-change-form.factory.js';

var PasswordChangeModule = angular.module('PasswordChangeModule', [
  'formly',
  'formlyBootstrap',
  'knbitFrontend.Members.Config'
])
  .factory('PasswordChangeFormFactory', PasswordChangeFormFactory)
  .controller('PasswordChangeCtrl', PasswordChangeCtrl);

export default PasswordChangeModule;
