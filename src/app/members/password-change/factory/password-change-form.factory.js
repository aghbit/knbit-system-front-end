'use strict';

var PasswordChangeFormFactory = function () {
  return {
    getFields: function () {
      return [
        {
          key: 'currentPassword',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'Current password',
            placeholder: 'Enter current password',
            required: true
          }
        },
        {
          key: 'newPassword',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'New password',
            placeholder: 'Enter new password',
            required: true
          }
        },
        {
          key: 'newPasswordRepeated',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'New password',
            placeholder: 'Repeat new password',
            required: true
          }
        }
      ];
    }
  };
};

export default PasswordChangeFormFactory;
