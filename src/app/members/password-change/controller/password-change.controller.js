'use strict';

class PasswordChangeCtrl {
  constructor(PasswordChangeFormFactory, $http, aaUsersListUrl, AuthService) {
    this.form = {};
    this.fields = PasswordChangeFormFactory.getFields();
    this.authHeader = {
      'knbit-aa-auth': AuthService.getToken()
    };
    this.passwordChangeUrl = aaUsersListUrl + '/' + AuthService.getUserId() + '/password';
    this.http = $http;
    this.requestFailed = false;
    this.requestSuccessful = false;

  }

  changePassword() {
    // todo validate passes
    if (this.form.newPassword !== this.form.newPasswordRepeated) {
      this.requestHasFailed('NEW_PASSWORDS_DO_NOT_MATCH');
      return;
    }
    var requestJson = {
      'oldPassword': this.form.currentPassword,
      'newPassword': this.form.newPassword
    };
    this.http({
      method: 'PATCH',
      url: this.passwordChangeUrl,
      headers: this.authHeader,
      data: requestJson
    }).then(() => {
      this.requestFailed = false;
      this.requestSuccessful = true;
      this.requestSuccessfulMessage = 'Password successfully changed.';
    }, (error) => {
      if (error.status === 401) {
        this.requestHasFailed('USER_NOT_LOGGED_IN');
      } else {
        this.requestHasFailed(error.data.reason);
      }
    });
    //todo commit master + rebuild
  }

  requestHasFailed(message) {
    this.requestFailedMessage = 'Request failed: ' + message;
    this.requestSuccessful = false;
    this.requestFailed = true;
  }

}

PasswordChangeCtrl.$inject = ['PasswordChangeFormFactory', '$http', 'aaUsersListUrl', 'AuthService'];

export default PasswordChangeCtrl;
