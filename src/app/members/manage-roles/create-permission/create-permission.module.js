'use strict';

import CreatePermissionFormFactory from './create-permission-form.factory.js';
import CreatePermissionCtrl from './create-permission.controller.js';

var CreatePermissionModule = angular.module('CreatePermissionModule', [])
.factory('CreatePermissionFormFactory', CreatePermissionFormFactory)
.controller('CreatePermissionCtrl', CreatePermissionCtrl);

export default CreatePermissionModule;