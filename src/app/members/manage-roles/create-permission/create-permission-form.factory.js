'use strict';

var CreatePermissionFormFactory = function () {
  return {
    getFields: function () {
      return [
        {
          key: 'name',
          type: 'input',
          templateOptions: {
            label: 'Permission name',
            placeholder: 'Enter UPPERCASE permission name',
            required: true
          }
        }
      ];
    }
  };
};

export default CreatePermissionFormFactory;
