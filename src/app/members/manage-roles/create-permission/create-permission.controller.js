'use strict';

class CreatePermissionCtrl {
  constructor($scope, CreatePermissionFormFactory, ManageRolesService) {
    this.scope = $scope;
    this.fields = CreatePermissionFormFactory.getFields();
    this.form = {};
    this.manageRolesService = ManageRolesService;
  }

  cancel() {
    this.scope.$close(false);
  }

  createPermission() {
    this.manageRolesService.createPermission(this.form.name).$promise.then(() => {
      this.scope.$close(true);
    });
  }

}

CreatePermissionCtrl.$inject = ['$scope', 'CreatePermissionFormFactory', 'ManageRolesService'];
export default CreatePermissionCtrl;