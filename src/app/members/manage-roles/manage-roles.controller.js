'use strict';

class ManageRolesCtrl {
  constructor(ManageRolesService, $modal) {
    this.manageRolesService = ManageRolesService;
    this.modal = $modal;
    ManageRolesService.getUsers().$promise.then((usersList) => {
      this.members = usersList.values.filter((user) => {
        return user.state === 'accepted'
      });
      this.fetchData();
    });
  }

  fetchData() {
    this.manageRolesService.getAAUsers().$promise.then((aaUsersList) => {
      aaUsersList.users.forEach((aaUser) => {
        this.members.forEach((member) => {
          if (aaUser.id === member.userId) {
            member.roles = aaUser.roles.join(', ')
          }
        });
      });
    });
    this.manageRolesService.getPermissions().$promise.then((permissions) => {
      this.permissions = permissions;
    });
    this.manageRolesService.getRoles().$promise.then((roles) => {
      this.bottomRoles = roles;
      this.upperRoles = roles;
      this.upperRoles.forEach((role) => {
        role.formattedPermissions = role.permissions.join(', ');
      });
    });
  }

  createPermission() {
    this.modal.open({
      templateUrl: 'app/members/manage-roles/create-permission/create-permission.view.html',
      controller: 'CreatePermissionCtrl',
      controllerAs: 'createPermission',
      size: 'sm'
    }).result.finally(() => {
      this.fetchData();
    });
  }

  createRole() {
    this.modal.open({
      templateUrl: 'app/members/manage-roles/create-role/create-role.view.html',
      controller: 'CreateRoleCtrl',
      controllerAs: 'createRole',
      size: 'lg',
      resolve: {
        permissions: () => this.getSelected(this.permissions)
      }
    }).result.finally(() => {
      this.fetchData();
    });
  }

  updateRole() {
    this.modal.open({
      templateUrl: 'app/members/manage-roles/update-role/update-role.view.html',
      controller: 'UpdateRoleCtrl',
      controllerAs: 'updateRole',
      size: 'lg',
      resolve: {
        permissions: () => this.getSelected(this.permissions),
        previousRole: () => this.getSelected(this.upperRoles)
      }
    }).result.finally(() => {
      this.fetchData();
    });
  }

  assignRole() {
    var selectedRoles = this.getSelected(this.bottomRoles);
    if (selectedRoles && selectedRoles.length === 1) {
      var roleName = selectedRoles[0].name;
      this.getSelected(this.members).forEach((member) => {
        this.manageRolesService.assignRole(member.userId, roleName).$promise.then(() => {
          this.fetchData();
        }, (error) => {
          console.log(error)
        });
      });
    }
  }

  revokeRole() {
    var selectedRoles = this.getSelected(this.bottomRoles);
    if (selectedRoles && selectedRoles.length === 1) {
      var roleName = selectedRoles[0].name;
      this.getSelected(this.members).forEach((member) => {
        this.manageRolesService.revokeRole(member.userId, roleName).$promise.then(() => {
          this.fetchData();
        }, (error) => {
          console.log(error)
        });
      });
    }
  }

  deleteRole() {
    var selectedRoles = this.getSelected(this.bottomRoles);
    if (selectedRoles && selectedRoles.length === 1) {
      var roleName = selectedRoles[0].name;
      this.manageRolesService.deleteRole(roleName).$promise.then(() => {
        this.fetchData();
      }, (error) => {
        console.log(error)
      });
    }
  }

  getSelected(collection) {
    return collection.filter((element) => {
      return element.isSelected;
    });
  }

}


ManageRolesCtrl.$inject = ['ManageRolesService', '$modal'];
export default ManageRolesCtrl