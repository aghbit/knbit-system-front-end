'use strict';

import AuthModule from '../../auth/auth.module.js';
import CreateRoleModule from './create-role/create-role.module.js';
import CreatePermissionModule from './create-permission/create-permission.module.js';
import UpdateRoleModule from './update-role/update-role.module';
import ManageRolesCtrl from './manage-roles.controller.js';
import ManageRolesService from './manage-roles.service.js';

var ManageRolesModule = angular.module('ManageRolesModule', [
  'ui.bootstrap',
  'smart-table',
  'knbitFrontend.Members.Config',
  AuthModule.name,
  CreatePermissionModule.name,
  CreateRoleModule.name,
  UpdateRoleModule.name
])
  .service('ManageRolesService', ManageRolesService)
  .controller('ManageRolesCtrl', ManageRolesCtrl);

export default ManageRolesModule;