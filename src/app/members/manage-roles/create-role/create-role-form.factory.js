'use strict';

var CreateRoleFormFactory = function () {
  return {
    getFields: function () {
      return [
        {
          key: 'name',
          type: 'input',
          templateOptions: {
            label: 'Role name',
            placeholder: 'Enter UPPERCASE role name',
            required: true
          }
        }
      ];
    }
  };
};

export default CreateRoleFormFactory;
