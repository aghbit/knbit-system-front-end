'use strict';

class CreateRoleCtrl {
  constructor($scope, CreateRoleFormFactory, ManageRolesService, permissions) {
    this.scope = $scope;
    this.fields = CreateRoleFormFactory.getFields();
    this.form = {};
    if (angular.isArray(permissions)) {
      this.permissions = permissions.map(function (permission) {
        return permission.name;
      });
      this.permissionsFormatted = this.permissions.join(', ');
    }

    this.manageRolesService = ManageRolesService;
  }

  cancel() {
    this.scope.$close(false);
  }

  createRole() {
    this.manageRolesService.createRole(this.form.name, this.permissions).$promise.then(() => {
      this.scope.$close(true);
    });
  }

}

CreateRoleCtrl.$inject = ['$scope', 'CreateRoleFormFactory', 'ManageRolesService', 'permissions'];
export default CreateRoleCtrl;