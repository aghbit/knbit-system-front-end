'use strict';

import CreateRoleFormFactory from './create-role-form.factory.js';
import CreateRoleCtrl from './create-role.controller.js';

var CreateRoleModule = angular.module('CreateRoleModule', [])
.factory('CreateRoleFormFactory', CreateRoleFormFactory)
.controller('CreateRoleCtrl', CreateRoleCtrl);

export default CreateRoleModule;