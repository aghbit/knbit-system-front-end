'use strict';

class ManageRolesService {
  constructor($resource, AuthService, roleManagementUrls, aaUsersListUrl, usersListUrl) {
    this.authHeader = {
      'knbit-aa-auth': AuthService.getToken()
    };
    this.aaUsersListResource = $resource(aaUsersListUrl, {}, {
      fetch: {
        method: 'GET',
        headers: this.authHeader
      }
    });
    this.usersListResource = $resource(usersListUrl, {}, {
      fetch: {
        method: 'GET',
        headers: this.authHeader
      }
    });
    this.permissionsResource = $resource(roleManagementUrls.permissionsUrl, {}, {
      fetch: {
        method: 'GET',
        isArray: true,
        headers: this.authHeader
      },
      create: {
        method: 'POST',
        headers: this.authHeader
      }
    });
    this.allRolesResource = $resource(roleManagementUrls.rolesUrl, {}, {
      fetch: {
        method: 'GET',
        isArray: true,
        headers: this.authHeader
      },
      create: {
        method: 'POST',
        headers: this.authHeader
      }
    });
    this.singleRoleResource = $resource(roleManagementUrls.rolesUrl + "/:roleName", {
      roleName: '@name'
    }, {
      update: {
        method: 'PUT',
        headers: this.authHeader
      },
      delete: {
        method: 'DELETE',
        headers: this.authHeader
      }
    });
    this.assignRoleResource = $resource(aaUsersListUrl + "/:userId/roles", {
      userId: '@id'
    }, {
      assign: {
        method: 'PUT',
        headers: this.authHeader
      }
    });
    this.revokeRoleResource = $resource(aaUsersListUrl + "/:userId/roles/:roleName", {
      userId: '@id',
      roleName: '@name'
    }, {
      revoke: {
        method: 'DELETE',
        headers: this.authHeader
      }
    });
  }

  getPermissions() {
    return this.permissionsResource.fetch();
  }

  getRoles() {
    return this.allRolesResource.fetch();
  }

  getUsers() {
    return this.usersListResource.fetch();
  }

  getAAUsers() {
    return this.aaUsersListResource.fetch();
  }

  createPermission(permissionName) {
    var requestJson = {
      'name': permissionName
    };
    return this.permissionsResource.create(requestJson);
  }

  createRole(roleName, permissionsList) {
    var requestJson = {
      'name': roleName,
      'permissions': permissionsList
    };
    return this.allRolesResource.create(requestJson);
  }

  updateRole(previousRoleName, newRoleName, permissionsList) {
    var requestJson = {
      'name': newRoleName,
      'permissions': permissionsList
    };
    return this.singleRoleResource.update({roleName: previousRoleName}, requestJson);
  }

  deleteRole(roleName) {
    return this.singleRoleResource.delete({roleName: roleName});
  }

  assignRole(userId, roleName) {
    var requestJson = {
      'roleName': roleName
    };
    return this.assignRoleResource.assign({userId: userId}, requestJson);
  }

  revokeRole(userId, roleName) {
    return this.revokeRoleResource.revoke({userId: userId, roleName: roleName});
  }

}

ManageRolesService.$inject = ['$resource', 'AuthService', 'roleManagementUrls', 'aaUsersListUrl', 'usersListUrl'];
export default ManageRolesService