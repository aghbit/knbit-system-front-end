'use strict';

var UpdateRoleFormFactory = function () {
  return {
    getFields: function () {
      return [
        {
          key: 'name',
          type: 'input',
          templateOptions: {
            label: 'New ole name',
            placeholder: 'Enter UPPERCASE role name',
            required: true
          }
        }
      ];
    }
  };
};

export default UpdateRoleFormFactory;
