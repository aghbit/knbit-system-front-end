'use strict';

class UpdateRoleCtrl {
  constructor($scope, UpdateRoleFormFactory, ManageRolesService, permissions, previousRole) {
    this.scope = $scope;
    this.fields = UpdateRoleFormFactory.getFields();
    this.form = {};
    this.previousRoleName = (previousRole[0]) ? previousRole[0].name : "";
    this.form.name = this.previousRoleName;

    this.previousPermissionsFormatted = (previousRole[0]) ? previousRole[0].permissions.join(', ') : "";
    if (angular.isArray(permissions)) {
      this.permissions = permissions.map(function (permission) {
        return permission.name;
      });
      this.permissionsFormatted = this.permissions.join(', ');
    }

    this.manageRolesService = ManageRolesService;
  }

  cancel() {
    this.scope.$close(false);
  }

  updateRole() {
    this.manageRolesService.updateRole(this.previousRoleName, this.form.name, this.permissions).$promise.then(() => {
      this.scope.$close(true);
    });
  }

}

UpdateRoleCtrl.$inject = ['$scope', 'CreateRoleFormFactory', 'ManageRolesService', 'permissions', 'previousRole'];
export default UpdateRoleCtrl;