'use strict';

import UpdateRoleFormFactory from './update-role-form.factory.js';
import UpdateRoleCtrl from './update-role.controller.js';

var UpdateRoleModule = angular.module('UpdateRoleModule', [])
.factory('UpdateRoleFormFactory', UpdateRoleFormFactory)
.controller('UpdateRoleCtrl', UpdateRoleCtrl);

export default UpdateRoleModule;