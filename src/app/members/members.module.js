'use strict';

import Routes from './members.routes.js';
import LoginModule from './login/login.module.js';
import RegisterModule from './register/register.module.js';
import ManageUsersModule from './manage-users/manage-users.module.js';
import PasswordChangeModule from './password-change/password-change.module.js';
import ManageRolesModule from './manage-roles/manage-roles.module';

var knbitFrontendMembersModule = angular.module('knbitFrontend.Members', [
  'ui.router',
  LoginModule.name,
  RegisterModule.name,
  ManageUsersModule.name,
  PasswordChangeModule.name,
  ManageRolesModule.name
])
  .config(Routes);

export default knbitFrontendMembersModule;
