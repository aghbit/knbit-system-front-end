'use strict';

var FacultyListFactory = function () {
  return {
    getFaculties: function () {
      return [
        {
          name: 'Computer Science, Electronics and Telecommunications',
          value: 'Computer Science, Electronics and Telecommunications'
        },
        {
          name: 'Mining and Geoengineering',
          value: 'Mining and Geoengineering'
        },
        {
          name: 'Metals Engineering and Industrial Computer Science',
          value: 'Metals Engineering and Industrial Computer Science'
        },
        {
          name: 'Electrical Engineering, Automatics, Computer Science and Biomedical Engineering',
          value: 'Electrical Engineering, Automatics, Computer Science and Biomedical Engineering'
        },
        {
          name: 'Mechanical Engineering and Robotics',
          value: 'Mechanical Engineering and Robotics'
        },
        {
          name: 'Geology, Geophysics and Environmental Protection',
          value: 'Geology, Geophysics and Environmental Protection'
        },
        {
          name: 'Mining Surveying and Environmental Engineering',
          value: 'Mining Surveying and Environmental Engineering'
        },
        {
          name: 'Materials Science and Ceramics',
          value: 'Materials Science and Ceramics'
        },
        {
          name: 'Foundry Engineering',
          value: 'Foundry Engineering'
        },
        {
          name: 'Non-Ferrous Metals',
          value: 'Non-Ferrous Metals'
        },
        {
          name: 'Drilling, Oil and Gas',
          value: 'Drilling, Oil and Gas'
        },
        {
          name: 'Management',
          value: 'Management'
        },
        {
          name: 'Energy and Fuels',
          value: 'Energy and Fuels'
        },
        {
          name: 'Physics and Applied Computer Science',
          value: 'Physics and Applied Computer Science'
        },
        {
          name: 'Applied Mathematics',
          value: 'Applied Mathematics'
        },
        {
          name: 'Humanities',
          value: 'Humanities'
        },
        {
          name: 'Geotechnology in Jastrzebie-Zdroj',
          value: 'Geotechnology in Jastrzebie-Zdroj'
        },
        {
          name: 'Manufacturing Engineering in Mielec',
          value: 'Manufacturing Engineering in Mielec'
        },
        {
          name: 'Academic Centre for Materials and Nanotechnology',
          value: 'Academic Centre for Materials and Nanotechnology'
        },
        {
          name: 'Other',
          value: 'Other'
        }
      ];
    }
  }
};

export default FacultyListFactory;