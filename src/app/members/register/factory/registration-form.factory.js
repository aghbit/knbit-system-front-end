'use strict';

var RegistrationFormFactory = function (FacultyListFactory) {
  return {
    getFields: function () {
      return [
        {
          className: 'row',
          fieldGroup: [
            {
              key: 'firstName',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: 'First Name',
                placeholder: 'Enter your first name',
                required: true
              }
            },
            {
              key: 'lastName',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: 'Last Name',
                placeholder: 'Enter your last name',
                required: true
              }
            }
          ]
        },
        {
          className: 'row',
          fieldGroup: [
            {
              key: 'email',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'email',
                label: 'Email address',
                placeholder: 'Enter email',
                required: true
              }
            },
            {
              key: 'indexNumber',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'number',
                label: 'Index number',
                placeholder: 'Enter index number',
                required: true
              }
            }
          ]
        },
        {
          className: 'row',
          fieldGroup: [
            {
              key: 'password',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'password',
                label: 'Password',
                placeholder: 'Enter password',
                required: true
              }
            },
            {
              key: 'repeatedPassword',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'password',
                label: 'Repeat password',
                placeholder: 'Repeat password',
                required: true
              }
            }
          ]
        },
        {
          className: 'row',
          fieldGroup: [
            {
              key: 'departmentName',
              className: 'col-sm-6',
              type: 'select',
              defaultValue: 'Computer Science, Electronics and Telecommunications',
              templateOptions: {
                label: 'Department name',
                placeholder: 'Enter department name',
                required: true,
                options: FacultyListFactory.getFaculties()
              }
            },
            {
              key: 'startOfStudiesYear',
              className: 'col-sm-6',
              type: 'input',
              templateOptions: {
                type: 'number',
                label: 'Start of studies (year)',
                placeholder: 'Enter year in which you started studies',
                required: true
              },
              validators: {
                minYear: function ($viewValue, $modelValue) {
                  var value = $modelValue || $viewValue;
                  if (value) {
                    return value >= 2000 && value < 2100; // to prevent from inserting current year of studies
                  }
                }
              }
            }
          ]
        },

        {
          key: 'acceptsBitStatute',
          className: '<hr />',
          type: 'checkbox',
          templateOptions: {
            label: 'I have read BIT Scientific Group statute (available on BIT website) ' +
            'and I accept it\'s terms and conditions. I am willing to diligently fulfil my ' +
            'obligations and responsibilities as a member of BIT Scientific Group.',

            //label: 'Oświadczam, że zapoznałem/am się ze Statutem Koła Naukowego BIT' +
            //'(dostępny na stronie internetowej Koła Naukowego) i akceptuję jego treść. ' +
            //'Jednocześnie zobowiązuję się sumiennie wypełniać obowiązki wynikające z' +
            //'mojego członkostwa w Kole Naukowym BIT.',
            required: true
          }
        },
        {
          key: 'acceptsProcessingPersonalInformation',
          type: 'checkbox',
          templateOptions: {
            label: 'I hereby agree for processing my personal data, included in Membership Declaration ' +
            '(as defined in Act of August 29, 1997 on Protection of Personal Data (Journal of Laws ' +
            'No. 133, item 883)) solely for the purpose of BIT Scientific Group statutory activities.',

            //label: 'Oświadczam, że wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w ' +
            //'Deklaracji członkowskiej z zachowaniem przepisów ustawy z dnia 29.08.1997 r. o ochronie ' +
            //'danych osobowych (Dz. U. Nr 133, poz. 883) jedynie do celu realizacji statutowych działań ' +
            //'Koła Naukowego BIT.',
            required: true
          }
        }
      ];
    }
  };
};

RegistrationFormFactory.$inject = ['FacultyListFactory'];
export default RegistrationFormFactory;