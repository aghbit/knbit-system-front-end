'use strict';

import RegisterCtrl from './controller/register.controller.js';
import RegistrationFormFactory from './factory/registration-form.factory.js';
import FacultyListFactory from './factory/faculty-list.factory.js';

var RegisterModule = angular.module('RegisterModule', [
  'formly',
  'formlyBootstrap'
])
  .factory('RegistrationFormFactory', RegistrationFormFactory)
  .factory('FacultyListFactory', FacultyListFactory)
  .controller('RegisterCtrl', RegisterCtrl);

export default RegisterModule;
