'use strict';

class RegisterCtrl {
  constructor(RegistrationFormFactory, $resource, registerUrls) {
    this.accountCreationResource = $resource(registerUrls.createAccountUrl);
    this.declarationSubmitResource = $resource(registerUrls.submitDeclarationUrl);
    this.form = {};
    this.fields = RegistrationFormFactory.getFields();

    this.registrationFailed = false;

    this.registrationFinishedWithSuccess = false;
  }

  submitRegistrationForm() {
    if (this.form.password !== this.form.repeatedPassword) {
      this.registrationFailed = true;
      this.registrationFailedMessage = 'Registration failed: PASSWORDS_NOT_MATCHING';
      return;
    }

    var accountCreationRequestJson = {
      email: this.form.email,
      password: this.form.password
    };
    var declarationSubmitRequestJson = {
      userId: null,
      email: this.form.email,
      firstName: this.form.firstName,
      lastName: this.form.lastName,
      departmentName: this.form.departmentName,
      indexNumber: this.form.indexNumber,
      startOfStudiesYear: this.form.startOfStudiesYear
    };

    this.accountCreationResource.save(accountCreationRequestJson).$promise.then((success) => {
      declarationSubmitRequestJson.userId = success.userId;
      this.declarationSubmitResource.save(declarationSubmitRequestJson).$promise.then(() => {
        this.registrationFailed = false;
        this.registrationFinishedWithSuccess = true;
      }, (error) => {
        this.registrationHasFailed(error);
      });
    }, (error) => {
      this.registrationHasFailed(error);
    });
  }

  registrationHasFailed(error) {
    this.registrationFailed = true;
    this.registrationFailedMessage = 'Registration failed: ';
    if (error.status === 0) {
      this.registrationFailedMessage += 'unable to access membership module.';
    } else {
      this.registrationFailedMessage += error.data.reason;
    }
  }

}

RegisterCtrl.$inject = ['RegistrationFormFactory', '$resource', 'registerUrls'];

export default RegisterCtrl;
