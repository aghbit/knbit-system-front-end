'use strict';

import AuthModule from '../../auth/auth.module.js';
import NotificationsModule from '../../notifications/notifications.module.js';
import ManageUsersCtrl from './controller/manage-users.controller.js';

var ManageUsersModule = angular.module('AdminPanelModule', [
  'ui.bootstrap',
  'smart-table',
  'knbitFrontend.Members.Config',
  AuthModule.name,
  NotificationsModule.name
])
  .controller('ManageUsersCtrl', ManageUsersCtrl);

export default ManageUsersModule;
