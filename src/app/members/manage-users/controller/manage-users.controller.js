'use strict';

class ManageUsersCtrl {
  constructor($resource, $state, AuthService, usersListUrl, declarationActionUrls) {
    this.authHeader = {
      'knbit-aa-auth': AuthService.getToken()
    };
    this.usersResource = $resource(usersListUrl, {}, {
      fetch: {
        method: 'GET',
        headers: this.authHeader
      }
    });

    this.userActivationResource = $resource(declarationActionUrls.declarationAcceptUrl, {}, {
      accept: {
        method: 'POST',
        headers: this.authHeader
      }
    });
    this.userRejectionResource = $resource(declarationActionUrls.declarationRejectUrl, {}, {
      reject: {
        method: 'POST',
        headers: this.authHeader
      }
    });
    this.state = $state;
    this.daysToMilliseconds = 86400000;
    this.commandFailed = false;
    this.fetchUsers();
  }

  fetchUsers() {
    this.usersResource.fetch().$promise.then((usersList) => {
      this.users = usersList.values;
      this.users.forEach((user) => {
        user.declarationSubmitDate = new Date(user.submitDateEpochDay * this.daysToMilliseconds);
      });
      this.candidates = this.users.filter((user) => {
        return user.state === 'created'
      });
      this.members = this.users.filter((user) => {
        return user.state === 'accepted'
      });
    });
  }

  performActionForSelected(collection, actionFun) {
    this.getSelected(collection).map(user => {
      actionFun({'userId': user.userId})
    });
  }

  getSelected(collection) {
    return collection.filter((user) => {
      return user.isSelected;
    });
  }

  activateSelectedCandidates() {
    this.performActionForSelected(this.candidates, (id) => this.activate(id));
  }

  removeSelectedCandidates() {
    this.performActionForSelected(this.candidates, (id) => this.reject(id));
  }

  composeEmailToSelected(collection) {
    var userIdsList = this.getSelected(collection).map((user) => {return {'userId': user.userId}});
    this.state.go('members.compose-email', {userIdsList: userIdsList});
  }

  activate(userIdJson) {
    this.userActivationResource.accept(userIdJson).$promise.then(() => {
      this.commandHasSucceeded();
      this.fetchUsers();
    }, (error) => {
      this.commandHasFailed(error.data.reason);
    });
  }

  reject(userIdJson) {
    this.userRejectionResource.reject(userIdJson).$promise.then(() => {
      this.commandHasSucceeded();
      this.fetchUsers();
    }, (error) => {
      this.commandHasFailed(error.data.reason);
    });
  }

  commandHasFailed(message) {
    this.commandFailed = true;
    this.commandFailedMessage = 'Command failed: ' + message;
  }

  commandHasSucceeded() {
    this.commandFailed = false;
  }
}

ManageUsersCtrl.$inject = ['$resource', '$state', 'AuthService', 'usersListUrl', 'declarationActionUrls'];

export default ManageUsersCtrl;
