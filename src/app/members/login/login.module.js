'use strict';

import LoginCtrl from './controller/login.controller.js';

var LoginModule = angular.module('LoginModule', [
  'ui.bootstrap'
])
  .controller('LoginCtrl', LoginCtrl);

export default LoginModule;
