'use strict';

class LoginCtrl {
  constructor(AuthService, $state, $stateParams) {
    this.authService = AuthService;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.email = null;
    this.password = null;
    this.error = false;
  }

  logIn() {
    var credentials = {
      email: this.email,
      password: this.password
    };

    this.authService.logIn(credentials).then(() => {
      this.$state.go(this.$stateParams.next);
    }, (error) => {
      if (error.status === 401 || error.status === 400) {
        if (error.data.reason === 'USER_INACTIVE') {
          this.errorMessage = 'Your candidate declaration must be accepted by BIT authorities before you can login.' +
          ' Please try again later.';
        } else {
          this.errorMessage = 'Invalid combination of email and password. Please try again.';
        }
      } else {
        this.errorMessage = 'Unable to contact with authorization system.';
      }
      this.error = true;
    });
  }

}

LoginCtrl.$inject = ['AuthService', '$state', '$stateParams'];

export default LoginCtrl;
