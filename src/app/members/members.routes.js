'use strict';

var Routes = function ($stateProvider) {
  $stateProvider
  .state('members.login', {
    url: '/login',
    views: {
      'content-page': {
        templateUrl: 'app/members/login/view/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      }
    },
    params: {
      next: 'sections.overview'
    }
  }).state('members.register', {
    url: '/register',
    views: {
      'content-page': {
        templateUrl: 'app/members/register/view/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
      }
    }
  })
  .state('members.password-change', {
    url: '/password-change',
    views: {
      'content-page': {
        templateUrl: 'app/members/password-change/view/password-change.html',
        controller: 'PasswordChangeCtrl',
        controllerAs: 'passwordChange'
      }
    },
    data: {
      requireSignedIn: true
    }
  })
  .state('members.manage-users', {
    url: '/manage-users',
    views: {
      'content-page': {
        templateUrl: 'app/members/manage-users/view/manage-users.html',
        controller: 'ManageUsersCtrl',
        controllerAs: 'userManager'
      }
    },
    data: {
      permission: 'MANAGE_USERS'
    }
  })
  .state('members.manage-roles', {
    url: '/manage-roles',
    views: {
      'content-page': {
        templateUrl: 'app/members/manage-roles/manage-roles.view.html',
        controller: 'ManageRolesCtrl',
        controllerAs: 'roleManager'
      }
    },
    data: {
      permission: 'MANAGE_ROLES'
    }
  })
  .state('members.compose-email', {
    url: '/compose-email',
    parent: 'members.manage-users',
    params: {userIdsList: {}},
    onEnter: ['$modal', '$state', function ($modal, $state) {
      $modal.open({
        templateUrl: 'app/notifications/compose-email.view.html',
        controller: 'ComposeEmailCtrl',
        controllerAs: 'composeEmail',
        size: 'lg'
      }).result.finally(function () {
        $state.go('^');
      });
    }],
    data: {
      permission: 'SEND_MAIL'
    }
  });
};

Routes.$inject = ['$stateProvider'];

export default Routes;
