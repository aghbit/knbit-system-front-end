## Stack

- AngularJS (~1.3.4)
- [Babel](https://babeljs.io/) (transforms ES6+ code into ES5)

## Setup

### Requirements

- NodeJS (version at least 0.11) or IO.js.

### Installation

- Clone repo.

````
git clone git@bitbucket.org:aghbit/knbit-system-front-end.git
cd knbit-system-front-end
````

- Install node dependencies.

````
npm install
````

- Install bower dependencies.

````
bower install
````

## Running

### Development

````
gulp serve
````

Translates babel code, launches browser, watches for changes and automatically reloads css/html/js files, detect errors and potential
problems in code.

### Production

Builds an optimized version of your application in `/dist`.

````
gulp build
````