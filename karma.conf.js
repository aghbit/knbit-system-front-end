'use strict';

module.exports = function (config) {

  var configuration = {
    autoWatch: false,

    frameworks: ['jasmine', 'es6-shim'],

    ngHtml2JsPreprocessor: {
      stripPrefix: 'src/',
      moduleName: 'templates'
    },

    browsers: ['PhantomJS'],

    plugins: [
      'karma-es6-shim',
      'karma-phantomjs-launcher',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-babel-preprocessor',
      'karma-jasmine',
      'karma-ng-html2js-preprocessor'
    ],

    preprocessors: {
      'src/**/*.html': ['ng-html2js'],
      'src/**/*.js': ['babel']
    }
  };

  config.set(configuration);
};
