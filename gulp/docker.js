var gulp = require('gulp');
var shell = require('gulp-shell');
var gutil = require('gulp-util');

module.exports = function () {
  var user = gutil.env.user;
  var pass = gutil.env.pass;
  var email = gutil.env.email;

  var registry = gutil.env.registry;
  var repo = gutil.env.repo;
  var tag = gutil.env.tag;

  var login = 'docker login -u ' + user + ' -p ' + pass + ' -e ' + email;
  var build = 'docker build --no-cache -t ' + registry + '/' + repo + ':' + tag + ' .';
  var push = 'docker push ' + registry + '/' + repo + ':' + tag;

  gulp.task('docker', shell.task([login, build, push]));
};
