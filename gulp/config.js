/**
 * Created by novy on 15.05.15.
 */

var gulp = require('gulp');
var ngConstant = require('gulp-ng-constant');
var gulpUtil = require('gulp-util');

var determineEnvironment = function () {
  var specifiedEnvironment = gulpUtil.env.env;
  return specifiedEnvironment === undefined ? 'development' : specifiedEnvironment;
};

module.exports = function (options) {

  var config = options.config;

  gulp.task('config', function () {
    config.modules.forEach(function (moduleName) {

      var constantFileContent = require(
        options.root + '/' + config.configFileInputPrefix + '/' + moduleName + '/' + config.configFileInputSuffix
      );

      var withChosenEnvironment = constantFileContent[determineEnvironment()];

      ngConstant({
        name: withChosenEnvironment.name,
        deps: withChosenEnvironment.deps,
        constants: withChosenEnvironment.constants,
        stream: true
      })
        .pipe(gulp.dest(config.configFileOutputPrefix + '/' + moduleName + '/'));

    });
  });

};
