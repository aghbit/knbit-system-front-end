'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep');
var karma = require('karma');
var concat = require('concat-stream');
var _ = require('lodash');
var gulpUtil = require('gulp-util');

module.exports = function (options) {
  function listFiles(callback) {
    var wiredepOptions = _.extend({}, options.wiredep, {
      dependencies: true,
      devDependencies: true
    });
    var bowerDeps = wiredep(wiredepOptions);

    var specFiles = [
      options.src + '/**/*.spec.js',
      options.src + '/**/*.mock.js'
    ];

    var htmlFiles = [
      options.src + '/**/*.html'
    ];

    var srcFiles = [
      options.tmp + '/serve/app/index.js'
    ].concat(specFiles.map(function (file) {
        return '!' + file;
      }));


    gulp.src(srcFiles)
      .pipe(concat(function (files) {
        callback(bowerDeps.js
          .concat(_.pluck(files, 'path'))
          .concat(htmlFiles)
          .concat(specFiles));
      }));
  }

  function runTests(singleRun, done) {
    listFiles(function (files) {
      var params = {
        configFile: __dirname + '/../karma.conf.js',
        files: files,
        singleRun: singleRun,
        autoWatch: !singleRun
      };

      var browsers = gulpUtil.env.browsers;
      if (browsers !== undefined) {
        params.browsers = browsers.split(',');
      }

      karma.server.start(params, done);
    });
  }

  gulp.task('test', ['scripts'], function (done) {
    runTests(true, done);
  });
  gulp.task('test:auto', ['watch'], function (done) {
    runTests(false, done);
  });
};
