#!/usr/bin/env sh

# pre-commit.sh
#.git/hooks/pre-commit

# Installation:
# >> cd <repo>
# >> ln -s ../../hooks/pre-commit.sh .git/hooks/pre-commit
# Note: you may need to run the following command to add execute permissions.
# >> chmod +x .git/hooks/pre-commit

if git diff-index --quiet HEAD --; then
    # no changes between index and working copy; just run tests
    ./node_modules/eslint/bin/eslint.js src/**/*.js
    RESULT=$?
else
    # Test the version that's about to be committed, stashing all unindexed changes
    git stash -q --keep-index
    ./node_modules/eslint/bin/eslint.js src/**/*.js
    RESULT=$?
    git stash pop -q
fi

if [ $RESULT -ne 0 ]; then
  echo "Pre-commit check failed, commit rejected."
  exit 1
fi

echo "All tests passed!"
exit 0
